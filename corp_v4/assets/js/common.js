// GA
function GA() {
	var ua = {};
	ua.name = window.navigator.userAgent.toLowerCase();
	ua.isIE = (ua.name.indexOf('msie') >= 0 || ua.name.indexOf('trident') >= 0);
	ua.isFirefox = ua.name.indexOf('firefox') >= 0;
	ua.isChrome = ua.name.indexOf('chrome') >= 0;
	ua.isSafari = ua.name.indexOf('safari') >= 0;
	ua.isiPhone = ua.name.indexOf('iphone') >= 0;
	ua.isiPod = ua.name.indexOf('ipod') >= 0;
	ua.isiPad = ua.name.indexOf('ipad') >= 0;
	ua.isiOS = (ua.isiPhone || ua.isiPod || ua.isiPad);
	ua.isAndroid = ua.name.indexOf('android') >= 0;
	ua.isTablet = (ua.isiPad || (ua.isAndroid && ua.name.indexOf('mobile') < 0));
	if (ua.isIE) {		$('body').addClass('ie ie_' + ua.ver);	}
	if (ua.isFirefox) {	$('body').addClass('firefox');	}
	if (ua.isChrome) {	$('body').addClass('chrome');	}
	if (ua.isSafari) {	$('body').addClass('safari');	}
	if (ua.isiPhone) {	$('body').addClass('iPhone');	}
	if (ua.isiPod) {	$('body').addClass('iPod');		}
	if (ua.isiPad) {	$('body').addClass('iPad');		}
	if (ua.isiOS) {		$('body').addClass('iOS iOS_' + ua.ver);	}
	if (ua.isAndroid) {	$('body').addClass('android android_' + ua.ver);	}
	if (ua.isTablet) {	$('body').addClass('tablet');	}
}


// smoothScroll
function jsSmooth() {
	window.onload = function () {
		smoothScroll.init({
			selector: '[data-scroll]',
			selectorHeader: '[data-scroll-header]',
			speed: 1000,
			easing: 'easeOutQuart',
			offset: 0,
			updateURL: true,
			callback: function () {}
		});
	}
}


// Full Window
function jsFullWindow () {
  var wh = $('.js-full-window');
  var h = $(window).height();
  var w = $(window).width();
  wh.css('height', h + 'px');
  wh.css('width', w + 'px');
  $(window).resize(function() {
    var h = $(window).height();
    var w = $(window).width();
    wh.css('height', h + 'px');
    wh.css('width', w + 'px');
  });
}


// recruit bnr hover
function recruitBnrHover() {
  var hoverBtn = $('.js-hover-btn');
  var hoverBg = $('.js-hover-bg');
  hoverBtn.each(function () {
    $(this).on({
      'touchstart mouseenter': function() {
        var index = hoverBtn.index(this);
        hoverBtn.removeClass('is-active');
        $(this).addClass('is-active');
        hoverBg.eq(index).addClass('is-active');
      },
      'touchend mouseleave': function() {
        var index = hoverBtn.index(this);
        hoverBg.eq(index).removeClass('is-active');
      }
    });
  });
}


// header Fiexd
function headerFiexd() {
	$('.r_header__area').each(function () {
		var $window = $(window),
			$header = $(this),
			headerOffsetTop = $header.offset().top;

		$window.on('scroll', function () {
			if ($window.scrollTop() > headerOffsetTop) {
				$header.addClass('sticky');
			} else {
				$header.removeClass('sticky');
			}
		});
		$window.trigger('scroll');
	});
}

// canvas
function jsParticle() {
  var canvasDiv = document.getElementById('particle-canvas');
  var options = {
    particleColor: '#999',
    background: '#ccc',
    interactive: true,
    speed: 'medium',
    density: 'high'
  };
  var particleCanvas = new ParticleNetwork(canvasDiv, options);
}

// modal
function jsModais() {
	$('.js-modais').iziModal();
	$('.js-trigger-view').on('click', function() {
		$(this).hide();
		$('.js-image-gallery').find('li').show();
	});
}

// top
function JsTopVisual() {
  function JsMotion1(){
   $('.js-loading_1').addClass('hide');
 }
 function JsMotion2(){
   $('.js-loading_2').addClass('active');
 }
 setTimeout(JsMotion1, 2000);
 setTimeout(JsMotion2, 3000);
}

// top
function jsInview() {
  $('.js-view').on('inview', function(event, isInView) {
    if (isInView) {
      $(this).addClass('-view');
    } else {
		}
  });
}


function jsSliderRecruit() {
	$('.js-slider').slick({
		cssEase: 'ease-in-out',
		dots: false,
		arrows: true,
		infinite: true,
		slidesToScroll: 1,
  });
}



function jsSliderInterview() {
	$('.js-interview_slider').slick({
		cssEase: 'ease-in-out',
		dots: false,
		arrows: true,
		variableWidth: true,
		// initialSlide: 0,
		mobileFirst: true,
		slidesToShow: 3,
		infinite: false
	});
}

;(function($) {
	GA();
	jsSmooth();
	jsFullWindow();
	recruitBnrHover();
	headerFiexd();
	jsParticle();
	JsTopVisual();
	jsInview();
	jsSliderInterview();
})(jQuery);
