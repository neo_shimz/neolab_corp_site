<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-recruit'); ?>
<div id="particle-canvas" style="display: none;"></div>

<main class="g_main" role="main">
  <?php RecruitHeader(); ?>
  <div class="r_interview__archive-header">
    <?php RecruitVisualHeader(); ?>
    <?php wpBreadcrumbs(); ?>
  </div>
  <div class="r_interview__archive-main">
    <?php RecruitNav(); ?>
    <div class="r_interview__archive-main__inner">
      <?php
        $loop = new WP_Query(array(
          'post_type'			=> 'interview',
          'order'				=> 'DESC',
        ));
        while ($loop->have_posts()) : $loop->the_post();
      ?>
        <?php interviewPost(); ?>
      <?php endwhile; ?>
      <?php wpPagenavi(); ?>
    </div>
  </div>
</main>

<?php RecruitFooterBnner(); ?>
<?php get_template_part('_include/footer'); ?>
