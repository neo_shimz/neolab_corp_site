<?php get_template_part('./_blog/header'); ?>

<?php if(have_posts()) : while(have_posts()) :the_post(); ?>
	<div class="g_page__area cf">
		<div class="g_main cf">
			<div class="g_main__inner -single">
				<div class="s_post__head">
					<ul class="post-categories">
						<li>
							<?php echo get_the_term_list($post->ID, 'blog_category'); ?>
						</li>
					</ul>
					<time class="update wfont1">
						<?php echo get_the_date( 'Y.m.d' ); ?>
						<?php upData(); ?>
					</time>
				</div>
				<h1 class="s_single__title">
					<span><?php wp_title(''); ?></span>
				</h1>
				<?php singleSnsBtn(); ?>
				<?php if (has_post_thumbnail()) : ?>
					<div class="s_post__thumbs">
						<?php the_post_thumbnail('post-thumbs', array('class' => 'thumbs__size -large')); ?>
					</div>
				<?php endif; ?>
				<div class="s_the_content cf">
					<?php the_content(); ?>
				</div>
				<div class="s_post__food">
					<ul class="post-categories">
						<li>
							<?php echo get_the_term_list($post->ID,'blog_category'); ?>
						</li>
						<?php foreach(wp_get_object_terms(get_the_ID(),'blog_keywords') as $blog_keywords) : ?>
							<li>
								<a href="<?php echo home_url(); ?>/blog_keywords/<?php echo $blog_keywords->slug; ?>">
									<?php echo $blog_keywords->name?>
								</a>
							</li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="mt1">
					<?php singleSnsBtn(); ?>
				</div>
				<div class="s_facebook_area">
					<div class="s_img">
						<?php if (has_post_thumbnail()) : ?>
							<?php the_post_thumbnail('post-thumbs', array('class' => 'thumbs__size -large')); ?>
						<?php else : ?>
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/thumbs-not_img-L.png" alt="<?php the_title(); ?>｜イメージ" />
						<?php endif; ?>
					</div>
					<div class="s_btn">
						<p>facebookでも<br />随時配信しています</p>
						<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fneolabjpve%2F&width=114&layout=button_count&action=like&size=large&show_faces=false&share=false&height=21&appId=242333472476268" width="114" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
					</div>
				</div>
				<?php pagerSingle(); ?>
				<section class="s_recommend__area cf">
					<h2 class="s_sub_title">
						<span>関連記事</span>
					</h2>
					<div class="s_recommend__area-inner">
						<?php
							global $post;
							$args = array(
								'numberposts' => 3,
								'post_type' => 'blog',
								'taxonomy' => 'blog_category',
								'post__not_in' => array($post->ID)
						 );
						?>
						<?php $myPosts = get_posts($args); if($myPosts) : ?>
						<?php foreach($myPosts as $post) : setup_postdata($post); ?>
							<?php postBlogArticle(); ?>
						<?php endforeach; ?>
						<?php else : ?>
						<?php endif; wp_reset_postdata(); ?>
					</div>
				</section>
			</div>
		</div>
		<?php get_template_part('./_blog/sidebar'); ?>
		<?php wp_reset_query(); ?>
	</div>
	<?php wpBreadcrumbs(); ?>
	<?php get_template_part('./_include/inc-sns'); ?>
<?php endwhile; endif; ?>

<?php get_template_part('./_blog/footer'); ?>
