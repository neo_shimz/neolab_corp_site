<?php
  $share_url   = get_permalink();
  $share_title = get_the_title();
?>

<div class="g_navigation__fixed">
  <section class="g_navigation__sns">
    <a class="g_navigation__sns-btn">
      <i class="icon-share"></i>
    </a>
    <ul class="g_navigation__sns-btn__inner">
      <li class="facebook">
        <?php if ( is_single() ) : ?>
          <a href="//www.facebook.com/sharer.php?src=bm&u=<?=$share_url?>&t=<?=$share_title?>" onclick="return sns_window(this, 800, 600);">
            <i class="icon-facebook"></i>
          </a>
        <?php elseif ( is_page() ) : ?>
          <?php // 固定ページ ?>
        <?php else : ?>
          <?php // それ以外 ?>
          <a href="//www.facebook.com/sharer.php?src=bm&u=<?=home_url();?>/&t=<?=siteTitle();?>" onclick="return sns_window(this, 800, 600);">
            <i class="icon-facebook"></i>
          </a>
        <?php endif;; ?>
      </li>
      <li class="twitter">
        <?php if ( is_single() ) : ?>
          <a href="//twitter.com/share?text=<?=$share_title?>&url=<?=$share_url?>" onclick="return sns_window(this, 400, 600);">
            <i class="icon-twitter"></i>
          </a>
        <?php elseif ( is_page() ) : ?>
          <?php // 固定ページ ?>
        <?php else : ?>
          <?php // それ以外 ?>
          <a href="//twitter.com/share?text=<?=siteTitle();?>&url=<?=home_url();?>/" onclick="return sns_window(this, 400, 600);">
            <i class="icon-twitter"></i>
          </a>
        <?php endif;; ?>
      </li>
      <li class="pocket">
        <?php if ( is_single() ) : ?>
          <a href="//getpocket.com/edit?url=<?=$share_url?>&title=<?=$share_title?>" onclick="return sns_window(this, 500, 800);">
            <i class="icon-pocket"></i>
          </a>
        <?php elseif ( is_page() ) : ?>
          <?php // 固定ページ ?>
        <?php else : ?>
          <?php // それ以外 ?>
          <a href="//getpocket.com/edit?url=<?=home_url();?>/&title=<?=siteTitle();?>" onclick="return sns_window(this, 500, 800);">
            <i class="icon-pocket"></i>
          </a>
        <?php endif;; ?>
      </li>
      <li class="hatena">
        <?php if ( is_single() ) : ?>
          <a href="//b.hatena.ne.jp/add?mode=confirm&url=<?=$share_url?>" onclick="return sns_window(this, 600, 1000);">
            <i class="icon-hatenabookmark"></i>
          </a>
        <?php elseif ( is_page() ) : ?>
          <?php // 固定ページ ?>
        <?php else : ?>
          <?php // それ以外 ?>
          <a href="//b.hatena.ne.jp/add?mode=confirm&url=<?=home_url();?>/" onclick="return sns_window(this, 600, 1000);">
            <i class="icon-hatenabookmark"></i>
          </a>
        <?php endif;; ?>
      </li>
    </ul>
  </section>
  <a href="#" data-scroll class="g_page__scroll-btn">
    <i class="icon-keyboard_arrow_up"></i>
  </a>
</div>
