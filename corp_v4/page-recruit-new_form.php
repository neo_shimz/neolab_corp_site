<?php
/*
	template Name: 採用フォーム - 新卒採用
*/
?>
<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-recruit'); ?>
<div id="particle-canvas" style="display: none;"></div>

<main class="g_main header_fixed__margin" role="main" id="main">
  <?php wpBreadcrumbs(); ?>
  <div class="m_main__area-inner -contact r_formrun__input">


    <!-- class, action, methodを変更しないでください -->
    <!-- ↓自由に要素を追加・編集することができます -->
    <script src="https://sdk.form.run/js/v2/formrun.js"></script>
    <form class="formrun" action="https://form.run/api/v1/r/90kux2ljncjysx74tz4zksao" method="post">
      <h2>新卒採用フォーム</h2>
      <h3>プロフィール情報</h3>
      <section class="f_form__area f_layout__row">
        <dl>
          <dt class="required">氏名</dt>
          <dd>
            <input class="f_input__text" data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="氏名" placeholder="例）山田　太郎" type="text">
            <span class="error" data-formrun-show-if-error="氏名">氏名を正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">よみがな</dt>
          <dd>
            <input class="f_input__text" data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="よみがな" placeholder="例）ヤマダ　たろう" type="text">
            <span class="error" data-formrun-show-if-error="よみがな">よみがなを正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">性別</dt>
          <dd>
            <select class="f_input__select" data-formrun-class-if-error="error" data-formrun-required="" name="性別">
              <option value>--選択してください--</option>
              <option value="男">男</option>
              <option value="女">女</option>
              <option value="その他">その他</option>
            </select>
            <i class="f_select__icon icon-arrow_drop_down"></i>
            <span class="error" data-formrun-show-if-error="性別">性別を選択してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">生年月日</dt>
          <dd>
            <input class="f_input__text" data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="生年月日" placeholder="例）1990年1月10日" type="text">
            <span class="error" data-formrun-show-if-error="生年月日">生年月日を正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">メールアドレス</dt>
          <dd>
            <input class="f_input__text" data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" data-formrun-type="email" name="メールアドレス" type="text">
            <span class="error" data-formrun-show-if-error="メールアドレス">メールアドレスを正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">電話番号</dt>
          <dd>
            <input class="f_input__text" data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" data-formrun-type="tel" name="電話番号" type="text">
            <span class="error" data-formrun-show-if-error="電話番号">電話番号を正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">顔写真 アップロード</dt>
          <dd>
            <input class="f_input__file" data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" data-formrun-type="file" name="画像" type="file">
            <span class="error" data-formrun-show-if-error="画像">画像を正しくアップしてください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">郵便番号</dt>
          <dd>
            <input class="f_input__text" data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="郵便番号" placeholder="例）100-0001" type="text">
            <span class="error" data-formrun-show-if-error="郵便番号">郵便番号を正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">住所</dt>
          <dd>
            <textarea data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="住所" placeholder=""></textarea>
            <span class="error" data-formrun-show-if-error="住所">住所を正しく入力してください</span>
          </dd>
        </dl>
      </section>
      <h3>ご経歴</h3>
      <section class="f_form__area f_layout__row">
        <dl>
          <dt class="required">アルバイト・<br>インターンの経歴</dt>
          <dd>
            <textarea data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="アルバイト・インターンの職歴" placeholder=""></textarea>
            <span class="error" data-formrun-show-if-error="アルバイト・インターンの職歴">アルバイト・インターンの職歴を正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">最後に在籍した<br>教育機関名</dt>
          <dd>
            <textarea data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="最後に在籍した教育機関名" placeholder="例）○○大学 2030年4月卒業予定" style="min-height: 80px;"></textarea>
            <span class="error" data-formrun-show-if-error="最後に在籍した教育機関名">最後に在籍した教育機関名を正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">中学校以降の学歴</dt>
          <dd>
            <textarea data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="中学校以降の学歴" placeholder=""></textarea>
            <span class="error" data-formrun-show-if-error="中学校以降の学歴">中学校以降の学歴を正しく入力してください</span>
          </dd>
        </dl>
      </section>
      <h3>できること</h3>
      <section class="f_form__area f_layout__row">
        <dl>
          <dt class="required">ソフトウェア</dt>
          <dd>
            <textarea data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="ソフトウェア" placeholder="例）Sketch / Photoshop / Illustrator" style="min-height: 120px;"></textarea>
            <span class="error" data-formrun-show-if-error="ソフトウェア">ソフトウェアを正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">プログラミング言語</dt>
          <dd>
            <textarea data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="プログラミング言語" placeholder="例） JavaScript / PHP / C++ / C# / Objective-C / Swift / Java / Kotlin / Python / Ruby / Go/ HTML / CSS/ vue / React" style="min-height: 120px;"></textarea>
            <span class="error" data-formrun-show-if-error="プログラミング言語">プログラミング言語を正しく入力してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">英語</dt>
          <dd>
            <select class="f_input__select" data-formrun-class-if-error="error" data-formrun-required="" name="英語">
              <option value>--レベルを選択してください--</option>
              <option value="ネイティブ">ネイティブ</option>
              <option value="ビジネスレベル">ビジネスレベル</option>
              <option value="日常会話レベル">日常会話レベル</option>
              <option value="日常会話レベル未満">日常会話レベル未満</option>
            </select>
            <i class="f_select__icon icon-arrow_drop_down"></i>
            <span class="error" data-formrun-show-if-error="英語">英語を選択してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">他に話せる言語</dt>
          <dd>
            <textarea data-formrun-class-if-error="error" data-formrun-class-if-success="success" data-formrun-required="" name="他に話せる言語" placeholder="例）中国語：ビジネスレベル／日本語：日常会話レベル" style="min-height: 120px;"></textarea>
            <span class="error" data-formrun-show-if-error="他に話せる言語">他に話せる言語を正しく入力してください</span>
          </dd>
        </dl>
      </section>
      <h3>エントリーに際して</h3>
      <section class="f_form__area f_layout__row">
        <dl>
          <dt class="required">エントリーの<br>きっかけ</dt>
          <dd>
            <div class="f_input__checkbox-group" data-formrun-hide-if-confirm>
              <div class="f_input__checkbox-item">
                <label class="f_input__checkbox-label"><input class="f_input__checkbox" type="checkbox" name="経由[]" value="WEB上の記事を読んで" data-formrun-required> WEB上の記事を読んで</label>
              </div>
              <div class="f_input__checkbox-item">
                <label class="f_input__checkbox-label"><input class="f_input__checkbox" type="checkbox" name="経由[]" value="ネオラボブログを読んで" data-formrun-required> ネオラボブログを読んで</label>
              </div>
              <div class="f_input__checkbox-item">
                <label class="f_input__checkbox-label"><input class="f_input__checkbox" type="checkbox" name="経由[]" value="知人・友人に紹介された" data-formrun-required> 知人・友人に紹介された</label>
              </div>
              <div class="f_input__checkbox-item">
                <label class="f_input__checkbox-label"><input class="f_input__checkbox" type="checkbox" name="経由[]" value="Wantedlyを見て" data-formrun-required> Wantedlyを見て</label>
              </div>
              <div class="f_input__checkbox-item">
                <label class="f_input__checkbox-label"><input class="f_input__checkbox" type="checkbox" name="経由[]" value="ネオラボ社員が登壇したイベントを見て" data-formrun-required> ネオラボ社員が登壇したイベントを見て</label>
              </div>
            </div>
            <span class="error" data-formrun-show-if-error="経由[]">エントリーのきっかけを入力してください</span>
            <div data-formrun-show-if-confirm>
              <span data-formrun-confirm-value="経由[]"></span>
            </div>
          </dd>
        </dl>
      </section>
      <h3>職種・勤務地</h3>
      <section class="f_form__area f_layout__row">
        <dl>
          <dt class="required">希望する職種</dt>
          <dd>
            <select class="f_input__select" data-formrun-class-if-error="error" data-formrun-required="" name="職種">
              <option value="">--希望する職種を選択してください--</option>
              <option value="開発エンジニア">開発エンジニア</option>
              <option value="アプリ開発エンジニア">アプリ開発エンジニア</option>
              <option value="PM/PL">PM/PL</option>
              <option value="QA/QC">QA/QC</option>
              <option value="インフラエンジニア">インフラエンジニア</option>
              <option value="デザイナー">デザイナー</option>
            </select>
            <i class="f_select__icon icon-arrow_drop_down"></i>
            <span class="error" data-formrun-show-if-error="職種">職種を選択してください</span>
          </dd>
        </dl>
        <dl>
          <dt class="required">希望する勤務地</dt>
          <dd>
            <select class="f_input__select" data-formrun-class-if-error="error" data-formrun-required="" name="希望する勤務地">
              <option value="">--希望する勤務地を選択してください--</option>
              <option value="東京 新宿オフィス">東京 新宿オフィス</option>
              <option value="沖縄 那覇オフィス">沖縄 那覇オフィス</option>
            </select>
            <i class="f_select__icon icon-arrow_drop_down"></i>
            <span class="error" data-formrun-show-if-error="希望する勤務地">希望する勤務地を選択してください</span>
          </dd>
        </dl>
      </section>
      <h3>アピールしたいこと</h3>
      <section class="f_form__area f_layout__row">
        <dl>
          <dd style="width: 100%;">
            <textarea data-formrun-class-if-success="success" name="アピールしたいこと" placeholder="上記のフォームでアピールしきれていないことがあれば、自由に記述してください。"></textarea>
          </dd>
        </dl>
        <dl>
          <dd style="width: 100%;">
            <div class="f_input__checkbox-group" data-formrun-hide-if-confirm>
              <div class="f_input__checkbox-item" style="width: 100%;">
                <label class="f_input__checkbox-label">
                  <input class="f_input__checkbox" type="checkbox" name="プライバシーポリシー" value="プライバシーポリシーに同意します" data-formrun-required>
                  <a href="<?php echo home_url(); ?>/policy/" target="_blank">プライバシーポリシー</a>&nbsp;に同意します
                </label>
              </div>
              <span class="error" data-formrun-show-if-error="プライバシーポリシー">プライバシーポリシーへの同意が必要です</span>
            </div>
          </dd>
        </dl>
      </section>
      <div class="f_btn__submit">
        <button data-formrun-error-text="未入力の項目があります" data-formrun-submitting-text="送信中..." type="submit">エントリーする</button>
      </div>
    </form>


  </div>
</main>

<?php get_template_part('_include/footer'); ?>