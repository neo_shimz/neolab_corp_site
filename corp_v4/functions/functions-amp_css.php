<?php

add_action('amp_post_template_css', 'xyz_amp_my_additional_css_styles');

function xyz_amp_my_additional_css_styles($amp_template)
{ ?>

  // ------------------------
  // custom css
  // ------------------------

  .amp-wp-header {
  background-color: #fff !important;
  }
  .amp-wp-header div {
  display: flex;
  align-items: center;
  }
  .amp-wp-header a {
  width: 100px;
  height: 26px;
  display: block;
  background-size: 98px;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url('data:image/svg+xml;charset=utf8,%3Csvg%20fill%3D%22none%22%20height%3D%2225%22%20viewBox%3D%220%200%20120%2025%22%20width%3D%22120%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpath%20clip-rule%3D%22evenodd%22%20d%3D%22m2.89738%207.04436v15.89164h-2.89738v-22.936l17.8332%2017.9547v-15.89122h2.8973v22.93652zm52.38692%2017.63704c-2.947.0114-5.7961-1.0731-8.0092-3.0485-2.213-1.9755-3.6369-4.7052-4.0027-7.6736h-17.4656v6.0596h17.2336v2.9179h-20.1315v-20.87259h20.1315v2.91667h-17.2336v6.05982h17.4656c.3658-2.96842%201.7897-5.69813%204.0027-7.67358%202.2131-1.97544%205.0622-3.059881%208.0092-3.048492%206.6712%200%2012.0985%205.463972%2012.0985%2012.181372s-5.4271%2012.1814-12.0985%2012.1814zm-9.085-10.7221c.3613%202.3011%201.5638%204.3794%203.3675%205.8206s4.0763%202.1394%206.3646%201.9554c2.2882-.184%204.4242-1.2367%205.9819-2.9481%201.5577-1.7115%202.4228-3.9561%202.4228-6.2862%200-2.3302-.8651-4.57472-2.4228-6.28619-1.5577-1.71148-3.6937-2.76415-5.9819-2.94814-2.2883-.184-4.5609.5142-6.3646%201.95539s-3.0062%203.5195-3.3675%205.82054h5.5984c.3323-.8118.9315-1.48174%201.6956-1.89558s1.6459-.54604%202.4952-.37409c.8494.17196%201.6138.63743%202.163%201.31717.5493.6797.8495%201.5317.8495%202.4109%200%20.8791-.3002%201.7311-.8495%202.4109-.5492.6797-1.3136%201.1452-2.163%201.3171-.8493.172-1.7311.0398-2.4952-.3741-.7641-.4138-1.3633-1.0837-1.6956-1.8956zm55.4507%208.9767-8.021-16.15193-8.021%2016.15193h-15.9815v-20.87252h2.8974v17.95462h11.2926l9.8122-19.75805%209.8123%2019.75805h9.482c.548.006%201.092-.0984%201.6-.3071.508-.2088.97-.5178%201.36-.9092.39-.3913.699-.8573.91-1.3708s.32-1.0645.32-1.6209c0-.5565-.109-1.1075-.32-1.621s-.52-.9795-.91-1.3708c-.39-.3914-.852-.7004-1.36-.9092-.508-.2087-1.052-.3131-1.6-.3071h-.85v-2.9167h.85c.239%200%20.476-.0479.698-.14096.221-.09306.422-.22945.592-.4014.169-.17195.303-.37608.395-.60074s.139-.46545.139-.70862-.047-.48396-.139-.70862-.226-.4288-.395-.60074c-.17-.17195-.371-.30835-.592-.4014-.222-.09306-.459-.14096-.698-.14096h-6.154v-2.91667h6.153c.883-.00963%201.751.23433%202.504.70375.752.46943%201.359%201.14521%201.75%201.94944.39.80423.549%201.70416.458%202.59605-.092.89189-.429%201.73942-.975%202.44488%201.022.63269%201.866%201.52179%202.451%202.58149.585%201.0598.892%202.2546.89%203.4695-.01%201.897-.761%203.7125-2.087%205.0483-1.327%201.3359-3.122%202.083-4.991%202.0774z%22%20fill%3D%22%23ff6728%22%20fill-rule%3D%22evenodd%22%2F%3E%3C%2Fsvg%3E');
  }
  .amp-site-title {
  text-indent: -9999px;
  display: block;
  }
  .amp-wp-article{
  border-top: 1px solid #eee;
  margin-top: 0;
  overflow-y: hidden;
  }
  .amp-wp-article-header {
  margin-top: 1.2rem;
  }
  .amp-wp-title {
  font-size: 1.4rem;
  line-height: 1.45;
  }
  .amp-wp-meta.amp-wp-byline {
  display: none;
  }
  .amp-wp-meta.amp-wp-posted-on {
  display: none;
  }
  .amp-wp-article-featured-image.wp-caption{}
  .amp-wp-article-content {
  font-family: -apple-system, BlinkMacSystemFont, "Noto Sans Japanese", "Helvetica Neue", "Segoe UI", "Original Yu Gothic", "Yu Gothic", YuGothic, Verdana, "ヒラギノ角ゴ ProN W3", Meiryo, "M+ 1p", sans-serif;
  text-rendering: optimizeLegibility;
  -webkit-text-size-adjust: 100%;
  }
  .amp-wp-article-content pre {
  white-space: normal;
  }
  .amp-wp-article-content a {
  color: #03A9F4;
  text-decoration: underline;
  }
  .amp-wp-article-content div {
  margin-top: .6rem;
  }
  .amp-wp-article-footer {
  padding-top: 1rem;
  }
  .amp-wp-meta.amp-wp-tax-category,
  .amp-wp-meta.amp-wp-tax-tag {
  margin-top: .6rem;
  margin-bottom: .6rem;
  }
  .amp-wp-meta.amp-wp-tax-tag a,
  .amp-wp-meta.amp-wp-tax-category a {
  color: #03A9F4;
  text-decoration: underline;
  }
  .amp-wp-meta.amp-wp-comments-link{
  display: none;
  }
  .amp-wp-footer {
  background: #eee;
  border: none;
  height: 60px;
  }
  .amp-wp-footer h2 {
  display: none;
  }
  .amp-wp-footer p {
  display: none;
  }
  .amp-wp-footer div {
  padding: .4em 16px;
  }
  .back-to-top {
  position: absolute;
  top: 0;
  width: 40px;
  height: 40px;
  display: block;
  bottom: 0;
  margin: 0 auto;
  left: 0;
  right: 0;
  text-indent: -9999px;
  }
  .back-to-top:after {
  content: '';
  display: block;
  width: 50%;
  height: 50%;
  border-top: 2px solid #aaa;
  border-right: 2px solid #aaa;
  margin: 0 auto;
  transform: rotate(-45deg);
  position: relative;
  top: 0;
  }

<?php }
