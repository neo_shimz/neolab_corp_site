

  <article class="solution__items js-view e_fadein -calling">
    <a href="https://www.calling.fun/" class="solution__items-inner" target="_blank">
      <div class="solution__logo-area">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-calling.svg" alt="calling" />
      </div>
      <div class="solution__info-area">
        <h1 class="-title">Calling</h1>
        <p class="-description">
          "気になる"から"ファンになる"、全てのコミュニケーションをCalling一つで
        </p>
        <ul class="-tag">
          <li>
            <span>NEOLAB Produce</span>
          </li>
        </ul>
      </div>
    </a>
  </article>
  <article class="solution__items js-view e_fadein -jinjer">
    <a href="https://hcm-jinjer.com/" class="solution__items-inner" target="_blank">
      <div class="solution__logo-area">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-jinjer.svg" alt="Jinjer" />
      </div>
      <div class="solution__info-area">
        <h1 class="-title">jinjer</h1>
        <p class="-description">
          誰でも簡単に利用できるマルチデバイス対応のクラウド型勤怠管理システム
        </p>
        <ul class="-tag">
          <li>
            <span>NEOLAB Produce</span>
          </li>
          <li>
            <span>人材</span>
          </li>
          <li>
            <span>Web</span>
          </li>
          <li>
            <span>アプリ</span>
          </li>
        </ul>
      </div>
    </a>
  </article>
  <article class="solution__items js-view e_fadein -enigma">
    <a href="https://www.enigma.co.jp/" class="solution__items-inner" target="_blank">
      <div class="solution__logo-area">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-enigma.svg" alt="enigma" />
      </div>
      <div class="solution__info-area">
        <h1 class="-title">enigma</h1>
        <p class="-description">
          従業員は働いた分だけの給与をいつでも受け取り可能に
        </p>
        <ul class="-tag">
          <li>
            <span>NEOLAB Produce</span>
          </li>
          <li>
            <span>Webアプリ</span>
          </li>
          <li>
            <span>システム</span>
          </li>
          <li>
            <span>決済・給与</span>
          </li>
        </ul>
      </div>
    </a>
  </article>
  <article class="solution__items js-view e_fadein -develop">
    <a href="#" class="solution__items-inner -not_link">
      <div class="solution__logo-area">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-develop.svg" alt="" />
      </div>
      <div class="solution__info-area">
        <h1 class="-title">共同事業開発サービス</h1>
        <p class="-description">
        </p>
      </div>
    </a>
  </article>
