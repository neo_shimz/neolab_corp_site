
<?php function companyNav () { ?>
  <nav class="company__nav-area">
    <ul>
      <li <?php if( is_page('company') ) echo ' class="-active" '; ?>>
        <a href="<?php echo home_url(); ?>/company/">
          <span>本社(新宿)</span>
        </a>
      </li>
      <li <?php if( is_page('okinawa') ) echo ' class="-active" '; ?>>
        <a href="<?php echo home_url(); ?>/company/okinawa/">
          <span>沖縄オフィス</span>
        </a>
      </li>
      <li <?php if( is_page('vietnam-danang') ) echo ' class="-active" '; ?>>
        <a href="<?php echo home_url(); ?>/company/vietnam-danang/">
          <span>ダナンオフィス</span>
        </a>
      </li>
    </ul>
  </nav>
<?php } ?>

<?php /*

<?php function companyTokyo() { ?>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEXRsb7QV_GMbeuDudzycItMn-IphGkO8&callback=initMap"></script>
  <script>
  function initMap() {
  	var latlng = new google.maps.LatLng( 35.688891, 139.696035 );
  	var map = new google.maps.Map(document.getElementById('map'), {
  		zoom: 17,
  		center: latlng
  	});

  	//マーカーの設定
  	var marker = new google.maps.Marker({
  		position: latlng,
  		map: map,
  		icon: new google.maps.MarkerImage(
  			'<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/icon-maps-pin.png',//マーカー画像URL
  			new google.maps.Size(80, 80),//マーカー画像のサイズ
  			new google.maps.Point(0, 0),//マーカー画像表示の起点（変更しない）
  			new google.maps.Point(0, 0)//マーカー位置の調整
  		),
  	});

  	var mapStyle = [ {
  		"stylers": [ {
  			"saturation": -100
  		} ]
  	} ];
  	var mapType = new google.maps.StyledMapType(mapStyle);
  	map.mapTypes.set( 'GrayScaleMap', mapType);
  	map.setMapTypeId( 'GrayScaleMap' );

  }
  $(function() {
  	initMap()
  })
  </script>

<?php } ?>
