{
    let mouse = { x: 0, y: 0 }; //Cursor position
    let pos = { x: 0, y: 0 }; //Cursor position
    let ratio = 0.05; //delay follow cursor
    let active = false;
    let ball = document.getElementById("ball");

    TweenLite.set(ball, { xPercent: -50, yPercent: -50 }); //scale from middle ball

    document.addEventListener("mousemove", mouseMove);

    function mouseMove(e) {
        mouse.x = e.clientX;
        mouse.y = e.clientY;
    }

    TweenLite.ticker.addEventListener("tick", updatePosition);

    function updatePosition() {
        if (!active) {
            pos.x += (mouse.x - pos.x) * ratio;
            pos.y += (mouse.y - pos.y) * ratio;

            TweenLite.set(ball, { x: pos.x, y: pos.y });
        }
    }


    let JsEffectnBtn = $("a");

    JsEffectnBtn.mouseenter(function (e) {
        TweenMax.to(this, 0.3, { scale: 1 }); //scale trigger element
        TweenMax.to(ball, 0.3, { scale: 1 }); //scale ball
        document.getElementById("ball").classList.add("-hover");
        active = true;
    });

    JsEffectnBtn.mouseleave(function (e) {
        TweenMax.to(this, 0.8, { scale: 1 });
        TweenMax.to(ball, 0.8, { scale: 1 });
        document.getElementById("ball").classList.remove("-hover");
        active = false;
    });

    JsEffectnBtn.mousemove(function (e) {
        parallaxCursor(e, this, 1); //magnetic ball = low number is more attractive
        //callParallax(e, this);
    });

    // function callParallax(e, parent) {
    //     parallaxIt(e, parent, parent.querySelector(".text-hover"), 85); //magnetic area = higher number is more attractive
    // }

    function parallaxIt(e, parent, target, movement) {
        let boundingRect = parent.getBoundingClientRect();
        let relX = e.clientX - boundingRect.left;
        let relY = e.clientY - boundingRect.top;

        TweenMax.to(target, 0.3, {
            x: (relX - boundingRect.width / 2) / boundingRect.width * movement,
            y: (relY - boundingRect.height / 2) / boundingRect.height * movement,
            ease: Power2.easeOut
        });
    }

    function parallaxCursor(e, parent, movement) {
        let rect = parent.getBoundingClientRect();
        let relX = e.clientX - rect.left;
        let relY = e.clientY - rect.top;
        pos.x = rect.left + rect.width / 2 + (relX - rect.width / 2) / movement;
        pos.y = rect.top + rect.height / 2 + (relY - rect.height / 2) / movement;
        TweenMax.to(ball, 0.3, { x: pos.x, y: pos.y });
    }

}




const blobCursor = (() => {
  const CURSOR = document.querySelector('#cursorBlob');
  const LINKS = document.querySelectorAll('.global-menu__item');
  const setCursorPos = (e) => {
    const { pageX: posX, pageY: posY } = e;
    CURSOR.style.top = `${posY - (CURSOR.offsetHeight / 2)}px`;
    CURSOR.style.left = `${posX - (CURSOR.offsetWidth / 2)}px`;
  };
  document.addEventListener('mousemove', setCursorPos);

  const setCursorHover = () => CURSOR.style.transform = 'scale(2.5)';
  const removeCursorHover = () => CURSOR.style.transform = '';
  LINKS.forEach(link => link.addEventListener('mouseover', setCursorHover));
  LINKS.forEach(link => link.addEventListener('mouseleave', removeCursorHover));

})();
