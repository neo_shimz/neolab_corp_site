
<?php get_template_part('_include/header'); ?>


<main class="m_main__area" role="main">
	<div class="h_sub__header">
		<div id="particle-canvas" class="js_header__canvas"></div>
		<div class="h_sub__header-inner -short">
			<h1 class="h_title">
				<span class="-jp">ソリューション</span>
				<span class="-en">SOLUTION</span>
			</h1>
		</div>
	</div>
	<?php wpBreadcrumbs(); ?>
	<section class="solution__area -column4">
		<div class="solution__area-inner">
			<?php get_template_part('_include/inc-solution'); ?>
			<article class="solution__items -fine_hoiku">
				<a href="https://hoiku.fine.me/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-fine_hoiku.svg" alt="保育Fine!" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">保育Fine!</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -hoiku_hiroba">
				<a href="https://hoiku-hiroba.com/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-hoiku_hiroba.png" alt="保育ひろば" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">保育ひろば</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
							<li>
								<span>人材</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -hr_note">
				<a href="https://hcm-jinjer.com/media/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-hr_note.png" alt="HR NOTE" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">HR NOTE</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
							<li>
								<span>人材</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -abroaders">
				<a href="https://www.abroaders.jp/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-abroaders.png" alt="ABROADERS" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">ABROADERS</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -moby">
				<a href="https://car-moby.jp/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-moby.png" alt="MOBY" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">MOBY</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -coregano">
				<a href="https://coregano.me/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-coregano.png" alt="コレガノ" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">コレガノ</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -cocoriku">
				<a href="https://cocoriku.jp/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-cocoriku.png" alt="cocoriku（ココリク）" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">cocoriku（ココリク）</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -stepbaito">
				<a href="https://www.stepbaito.com/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-stepbaito.png" alt="ステップバイト" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">ステップバイト</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -torteo">
				<a href="https://torteo.jp/media/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-torteo.svg" alt="トルテオマガジン" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">トルテオマガジン</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -kigyosapri">
				<a href="https://kigyosapri.com/kigyo/sapri/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-kigyosapri.png" alt="起業サプリ" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">起業サプリ</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -kigyosapri">
				<a href="https://kigyosapri.com/kigyo/note/" class="solution__items-inner" target="_blank">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-kigyosapri_j.png" alt="起業サプリジャーナル" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">起業サプリジャーナル</h1>
						<ul class="-tag">
							<li>
								<span>Web</span>
							</li>
						</ul>
					</div>
				</a>
			</article>
			<article class="solution__items -neolab">
				<a href="" class="solution__items-inner">
					<div class="solution__logo-area">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-neolab.svg" alt="" />
					</div>
					<div class="solution__info-area">
						<h1 class="-title">AIを利用した次世代型新サービス</h1>
					</div>
				</a>
			</article>
		</div>
	</section>
</main>


<?php get_template_part('_include/footer'); ?>
