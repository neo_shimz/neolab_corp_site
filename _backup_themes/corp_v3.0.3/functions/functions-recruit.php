<?php

/**
 * 特集投稿タイプ定義
 */

add_action( 'init', 'create_recruit' , 0);

function create_recruit() {
	$recruit_type = array(
		'label' => '求人',
		'labels' => array(
				'name' => '求人',
				'singular_name' => '求人',
				'add_new_item' => '求人を追加',
				'new_item' => '求人を追加',
				'view_item' => '求人を表示',
				'not_found' => '求人は見つかりませんでした',
				'not_found_in_trash' => 'ゴミ箱に求人はありません。',
				'search_items' => '求人を検索'
		),
		'public' => true,
		'rewrite' => array(
			'slug' => 'recruit',
			'with_front' => false
		),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type( 'recruit', $recruit_type);



	// カテゴリータクソノミー
	$recruit_category_taxonomy = array(
		'hierarchical' => true,
		'label' => '求人カテゴリー',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'recruit_category'
		),
		'singular_label' => 'recruit_category'
	);
	register_taxonomy(
		'recruit_category',
		'recruit',
		$recruit_category_taxonomy
	);



	// キーワードタクソノミー
	$recruit_keyword_taxonomy = array(
		'hierarchical' => false,
		'label' => '求人キーワード',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'recruit_keywords'
		),
		'singular_label' => 'recruit_keywords'
	);
	register_taxonomy(
		'recruit_keywords',
		'recruit',
		$recruit_keyword_taxonomy
	);


}
