<?php

/**
 * 特集投稿タイプ定義
 */

add_action( 'init', 'create_client' , 0);

function create_client() {
	$client_type = array(
		'label' => 'クライアント',
		'labels' => array(
				'name' => 'クライアント',
				'singular_name' => 'クライアント',
				'add_new_item' => 'クライアントを追加',
				'new_item' => 'クライアントを追加',
				'view_item' => 'クライアントを表示',
				'not_found' => 'クライアントは見つかりませんでした',
				'not_found_in_trash' => 'ゴミ箱にクライアントはありません。',
				'search_items' => 'クライアントを検索'
		),
		'public' => true,
		'rewrite' => array(
			'slug' => 'client',
			'with_front' => false
		),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title', 'thumbnail')
	);
	register_post_type( 'client', $client_type);



	// カテゴリータクソノミー
	// $client_category_taxonomy = array(
	// 	'hierarchical' => true,
	// 	'label' => 'クライアントカテゴリー',
	// 	'show_ui' => true,
	// 	'query_var' => true,
	// 	'rewrite' => array(
	// 		'slug' => 'client_category'
	// 	),
	// 	'singular_label' => 'client_category'
	// );
	// register_taxonomy(
	// 	'client_category',
	// 	'client',
	// 	$client_category_taxonomy
	// );



	// キーワードタクソノミー
	// $client_keyword_taxonomy = array(
	// 	'hierarchical' => false,
	// 	'label' => 'クライアントキーワード',
	// 	'show_ui' => true,
	// 	'query_var' => true,
	// 	'rewrite' => array(
	// 		'slug' => 'client_keywords'
	// 	),
	// 	'singular_label' => 'client_keywords'
	// );
	// register_taxonomy(
	// 	'client_keywords',
	// 	'client',
	// 	$client_keyword_taxonomy
	// );


}
