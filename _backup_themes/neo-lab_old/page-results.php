<?php
/*
	template Name: results
*/
?>

<?php get_header(); ?>

<main id="results" class="g_main" role="main">
	<div class="top_visual second_visual bg cf">
		<h1 class="title wow fadeInUp cf" data-wow-delay=".4s">
			導入事例
		</h1>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="g_title cf">
		<h2 class="title wow fadeInUp cf" data-wow-delay=".4s">
			ネオラボのオフショア開発に入居されている企業様
		</h2>
	</div>
	<div class="cf mt4 mb8 wow fadeInUp cf" data-wow-delay=".8s">
		<ul class="logo_list results max-width on cf">
			<?php
				$loop = new WP_Query(array(
					'post_type'			=> 'logo',
					'order'				=> 'DESC',
					'showposts'			=> -1,
				));
				while ($loop->have_posts()) : $loop->the_post();
			?>
			<li>
				<?php if(get_field('logo_img_link')) : ?>
				<a href="<?php echo get_field('logo_img_link'); ?>" <?php if(get_field('logo_img_link_tab') == '2') : ?> target="_blank"<?php endif; ?> >
					<img src="<?php echo get_field('logo_img'); ?>" alt="<?php the_title(); ?>">
				</a>
				<?php else : ?>
					<img src="<?php echo get_field('logo_img'); ?>" alt="<?php the_title(); ?>">
				<?php endif; ?>
			</li>
			<?php endwhile; ?>
		</ul>
	</div>
	<?php include_once "inc/c_parts_development_service.php"; ?>
	<?php include_once "inc/c_parts_development.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>


<?php get_footer(); ?>