<?php
/*
	template Name: service
*/
?>

<?php get_header(); ?>

<main id="service" class="g_main" role="main">
	<div class="second_visual service_top">
		<h1 class="title wow fadeInUp cf" data-wow-delay=".4s">
			ベトナムでオフショア開発ならネオラボ
		</h1>
		<div class="logo wow fadeInUp cf" data-wow-delay=".8s">
			<svg id="path_logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
				<path data-name="NEOLAB LOGO" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)" alt="NEOLAB"/>
			</svg>
		</div>
		<div class="visual_btn btn_center tc cf wow fadeInUp cf" data-wow-delay="1.2s">
			<a href="<?php echo home_url(); ?>/contact/" class="btn op size_m orange">
				<i class="icon_mail_alt"></i>
				<span>お問い合わせはこちらから</span>
				<i class="arrow_carrot-right"></i>
			</a>
		</div>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>

	<section class="neolab_contents service_top_1 cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".4s">
				日本の突破口をひらくグローバル人材調達
			</h2>
			<div class="txt_1_wrap m_width_s tj wow fadeInUp cf" data-wow-delay=".8s" style="max-width: 720px;">
				<p class="txt_1">
					日本国内のIT人材不足は厳しさを増しています。優秀エンジニアは常に引く手あまたです。また、エンジニアネットワーク内の人材流動がほとんどで、転職市場（人材紹介等）ではあまり出会うことができません。Webニーズが増加する一方、IT人材不足が深刻になっていくものと予測されています。こうした行き詰まりを突破するために、ネオラボは「IT人材調達の多様化」を支援しています。
				</p>
			</div>
		</div>
	</section>
	<section class="neolab_contents service_top_2 cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".4s">
				競争戦略としてのベトナムオフショア
			</h2>
			<div class="txt_1_wrap m_width_s tj wow fadeInUp cf" data-wow-delay=".8s" style="max-width: 720px;">
				<p class="txt_1">
					新興国の台頭によって先進国はコスト競争の時代に突入しました。開発予算が減少傾向にある一方、エンジニア単価は高止まりし、開発プロジェクトでの利益創出の難易度も高まっています。このような潮流の中で新興国と対峙するのではなく、グローバルな役割分担を進めていく動きが求められます。ネオラボは成長著しいベトナムとのパートナーシップ戦略から「中長期的なコスト構造の変革」をご提案しています。
				</p>
			</div>
		</div>
	</section>
	<section class="neolab_contents service_top_3 cf">
		<div class="inner tc cf wow fadeInUp cf" data-wow-duration="1s" data-wow-delay=".6s">
			<div class="txt_1_wrap cf">
				<p class="txt_1">
					<span class="txt1">ネオラボはベトナムのダナンを中心にオフショア開発を行っています</span>
					<span class="txt2">なぜベトナムでオフショア開発なのか。</span>
				</p>
			</div>
		</div>
	</section>
	<section class="neolab_sevice sevice_1 cf">
		<div class="inner cf">
			<div class="image cf f_right wow fadeInRight cf" data-wow-delay=".8s">
				<div class="img"></div>
			</div>
			<div class="contents f_left cf wow fadeInLeft cf" data-wow-delay=".4s">
				<div class="title_wrap">
					<i class="number wf2">1</i>
					<h1 class="title">
						<span class="ttl_first">
							<span>ベトナムエンジニアの</span>
							<span>魅力</span>
						</span>
						<span class="ttl_secont">勤勉なベトナム人</span>
					</h1>
				</div>
				<div class="text cf">
					<p>
						オフショア開発の歴史上、過去の進出先に最も選ばれた国は中国でしたが、たび重なる日中関係の変化などから、長期でのパートナーシップを築くことが容易ではありませんでした。これに対し、日本とベトナムのあいだで政治関係が良好なことは特筆すべき点でしょう。またベトナム人がずば抜けて親日であることは、現地に行けば肌で感じられるほどです。またベトナム人の特徴として、日本と文化的な親和性を持ち、向上心が高く非常に勤勉であり、個人主義よりも集団主義的で、数学・物理の素養も高い、というようなことが挙げられます。
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="neolab_sevice sevice_2 cf">
		<div class="inner cf">
			<div class="image cf f_left wow fadeInLeft cf" data-wow-delay=".8s">
				<div class="img"></div>
			</div>
			<div class="contents f_right cf wow fadeInRight cf" data-wow-delay=".4s">
				<div class="title_wrap">
					<i class="number wf2">2</i>
					<h1 class="title">
						<span class="ttl_first">
							<span>ベトナムエンジニアの</span>
							<span>魅力</span>
						</span>
						<span class="ttl_secont">豊富なIT人材</span>
					</h1>
				</div>
				<div class="text cf">
					<p>
						ベトナムで約40万人（現状）と言われるIT人材数を「2020年までに100万人」に引き上げる国家計画が進められています。IT人材比率の上昇に取り組んでいることに加えて、ベトナムの総人口も9,250万人(2014年末)から年間100万人ペースでの増加が続いています。ベトナム政府からIT産業は最高の優遇措置を受けており、IT産業全体にさまざまな好影響をもたらしています。またベトナム国内におけるソフトウェア開発の約50-60%は日本向けです。国民の若年優秀層の多くはITエンジニアを志す国でもあり、学術機関による教育レベルも年々向上してきています。
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="neolab_sevice sevice_3 cf">
		<div class="inner cf">
			<div class="image cf f_right wow fadeInRight cf" data-wow-delay=".8s">
				<div class="img"></div>
			</div>
			<div class="contents f_left cf wow fadeInLeft cf" data-wow-delay=".4s">
				<div class="title_wrap">
					<i class="number wf2">3</i>
					<h1 class="title">
						<span class="ttl_first">
							<span>ベトナムエンジニアの</span>
							<span>魅力</span>
						</span>
						<span class="ttl_secont">若いエンジニア</span>
					</h1>
				</div>
				<div class="text cf">
					<p>
						日本国民の平均年齢が「約45歳」であることに対し、ベトナムは「約28歳」です。またベトナムは全世代の中で20代人口が最も多く、若くて成長ポテンシャルのある人材を豊富に確保しやすい社会構造が見られます。東南アジアでは平均年齢の低い国が多くそろっていますが、ベトナムもそうした人口統計を持つ国のひとつです。人口減少と少子高齢化が同時に進行する日本国内とは対照的に、ベトナムは2040年頃まで人口ボーナス期(生産年齢人口がその他人口より多い時期)が続いていくステータスとなっていることは見逃せません。
					</p>
				</div>
			</div>
		</div>
	</section>

	<?php include_once "inc/c_parts_development.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>


<?php get_footer(); ?>