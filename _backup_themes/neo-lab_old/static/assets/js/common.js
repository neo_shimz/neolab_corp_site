
;(function($) {

	/* ---------------------------------------
		pankuzu
	---------------------------------------*/
	$(function() {
		$('.breadcrumbs_inner a.home span').text("ネオラボトップページ");
	});


	/* ---------------------------------------
		sticky
	---------------------------------------*/
	$(function () {
		$('body').each(function () {
			var $window = $(window),
				$header = $('body'),
				headerOffsetTop = $header.offset().top;
			$window.on('scroll', function () {
				if ($window.scrollTop() > headerOffsetTop) {
					$header.addClass('sticky');
				} else {
					$header.removeClass('sticky');
				}
			});
			$window.trigger('scroll');
		});
	});


	/* ---------------------------------------
		pulldown
	---------------------------------------*/
	$(function(){
		var menuItem = $('.pulldown');
		menuItem.hover(function() {
			$('.sub',this).stop().addClass('open');
		},
		function () {
			$('.sub',this).stop().removeClass('open');
		});
		menuItem.hover(function(){
			$(this).addClass('current');
		},
		function(){
			$(this).removeClass('current');
		});
	});


	/* ---------------------------------------
		scroll option
	---------------------------------------*/
	$(window).load(function(){
		smoothScroll.init({
			selector: '[data-scroll]',                  // スムーススクロールが有効なリンクに付ける属性
			selectorHeader: '[data-scroll-header]',   // 固定ナビに付ける属性
			speed: 1000,                                 // 到達するまでの総時間(ミリ秒)
			easing: 'easeOutQuart',                   // スピードの種類
			offset: 0,                                  // 到達場所からズラすピクセル数
			updateURL: true,                            // URLを[#〜]に変更するか？
			callback: function () {}                    // コールバック関数 (到達時に実行される関数)
		});
	});


	/* ---------------------------------------
		drawer option
	---------------------------------------*/
	$(function() {
		$('.drawer').drawer({
			class: {
			nav: 'drawer-nav',
			toggle: 'drawer-toggle',
			overlay: 'drawer-overlay',
			open: 'drawer-open',
			close: 'drawer-close',
			dropdown: 'drawer-dropdown'
			},
			iscroll: {
			// Configuring the iScroll
			// //github.com/cubiq/iscroll#configuring-the-iscroll
			mouseWheel: true,
			preventDefault: false
			},
			showOverlay: true
		});
	});



})(jQuery);



/* ---------------------------------------
	scroll top
---------------------------------------*/
//** jQuery Scroll to Top Control script- (c) Dynamic Drive DHTML code library: http://www.dynamicdrive.com.
//** Available/ usage terms at http://www.dynamicdrive.com (March 30th, 09')
//** v1.1 (April 7th, 09'):
//** 1) Adds ability to scroll to an absolute position (from top of page) or specific element on the page instead.
//** 2) Fixes scroll animation not working in Opera.

var scrolltotop={
	//startline: Integer. Number of pixels from top of doc scrollbar is scrolled before showing control
	//scrollto: Keyword (Integer, or "Scroll_to_Element_ID"). How far to scroll document up when control is clicked on (0=top).
	setting: {startline:100, scrollto: 100, scrollduration:800, fadeduration:[300, 300]},
	controlHTML: '<a href="#top" class="pagetop"></a>', //HTML for control, which is auto wrapped in DIV w/ ID="topcontrol"
	controlattrs: {offsetx:30, offsety:40}, //offset of control relative to right/ bottom of window corner
	anchorkeyword: '#', //Enter href value of HTML anchors on the page that should also act as "Scroll Up" links

	state: {isvisible:false, shouldvisible:false},

	scrollup:function(){
		if (!this.cssfixedsupport) //if control is positioned using JavaScript
			this.$control.css({opacity:0}) //hide control immediately after clicking it
		var dest=isNaN(this.setting.scrollto)? this.setting.scrollto : parseInt(this.setting.scrollto)
		if (typeof dest=="string" && jQuery('#'+dest).length==1) //check element set by string exists
			dest=jQuery('#'+dest).offset().top
		else
			dest=0
		this.$body.animate({scrollTop: dest}, this.setting.scrollduration);
	},

	keepfixed:function(){
		var $window=jQuery(window)
		var controlx=$window.scrollLeft() + $window.width() - this.$control.width() - this.controlattrs.offsetx
		var controly=$window.scrollTop() + $window.height() - this.$control.height() - this.controlattrs.offsety
		this.$control.css({left:controlx+'px', top:controly+'px'})
	},

	togglecontrol:function(){
		var scrolltop=jQuery(window).scrollTop()
		if (!this.cssfixedsupport)
			this.keepfixed()
		this.state.shouldvisible=(scrolltop>=this.setting.startline)? true : false
		if (this.state.shouldvisible && !this.state.isvisible){
			this.$control.stop().animate({opacity:1}, this.setting.fadeduration[0])
			this.state.isvisible=true
		}
		else if (this.state.shouldvisible==false && this.state.isvisible){
			this.$control.stop().animate({opacity:0}, this.setting.fadeduration[1])
			this.state.isvisible=false
		}
	},

	init:function(){
		jQuery(document).ready(function($){
			var mainobj=scrolltotop
			var iebrws=document.all
			mainobj.cssfixedsupport=!iebrws || iebrws && document.compatMode=="CSS1Compat" && window.XMLHttpRequest //not IE or IE7+ browsers in standards mode
			mainobj.$body=(window.opera)? (document.compatMode=="CSS1Compat"? $('html') : $('body')) : $('html,body')
			mainobj.$control=$('<div id="topcontrol">'+mainobj.controlHTML+'</div>')
				.css({position:mainobj.cssfixedsupport? 'fixed' : 'absolute', bottom:mainobj.controlattrs.offsety, right:mainobj.controlattrs.offsetx, opacity:0, cursor:'pointer'})
				.attr({title:''}) //title
				.click(function(){mainobj.scrollup(); return false})
				.appendTo('body')
			if (document.all && !window.XMLHttpRequest && mainobj.$control.text()!='') //loose check for IE6 and below, plus whether control contains any text
				mainobj.$control.css({width:mainobj.$control.width()}) //IE6- seems to require an explicit width on a DIV containing text
			mainobj.togglecontrol()
			$('a[href="' + mainobj.anchorkeyword +'"]').click(function(){
				mainobj.scrollup()
				return false
			})
			$(window).bind('scroll resize', function(e){
				mainobj.togglecontrol()
			})
		})
	}
}

scrolltotop.init();
