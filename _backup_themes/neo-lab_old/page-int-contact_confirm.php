<?php
/*
	template Name: int-confirm2
*/
?>

<?php get_header(); ?>
<main id="contact" class="g_main" role="main">
	<div class="second_visual cf">
		<h1 class="title">
			サマーインターンお申込み
		</h1>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="g_title g_form confs cf">
		<div class="display cf">
			<h2 class="title">サマーインターンお申込み内容の確認</h2>
		</div>
		<div class="step_wrap cf">
			<div class="step_inner cf">
				<div class="step done t-indent-1" data-desc="入力">
					<i class="radius"></i>
				</div>
				<div class="step active t-indent-2" data-desc="確認">
					<i class="radius"></i>
				</div>
				<div class="step t-indent-3" data-desc="完了">
					<i class="radius"></i>
				</div>
			</div>
		</div>
		<?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
	</div>
</main>

<script>
$(function() {
	$('.tel').find('input[type=text]').prop('type', 'tel');
	var submitText = 'この内容で送信';
	$('.btn_submit li.submit input').val(submitText);
});
</script>

<?php get_footer(); ?>