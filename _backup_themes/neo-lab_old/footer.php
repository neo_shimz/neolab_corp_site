
<footer class="g_footer cf wow fadeInUp" role="contentinfo" data-wow-delay="0.6s">
	<div class="g_footer_primary cf">
		<div class="wrap cf">
			<p class="g_footer_logo">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/svg/svg-logo_orange.svg" alt="NEOLAB">
				</a>
			</p>
			<div class="item cf">
				<p class="g_footer_txt">
					ネオラボは、新しい未来の可能性を「オープン化」で追求している企業です。<br>
					オフショア事業では、日本のグローバル人材調達を加速させるために、日本企業の海外開発スタートアップを支援するプラットフォームサービスを提供しています。
				</p>
				<ul class="g_footer_sns cf">
					<li>
						<a class="facebook op" href="https://www.facebook.com/neolabjpve/" target="_blank">
							<i class="social_facebook"></i>
						</a>
					</li>
					<?php /*
					<li>
						<a class="twitter"  href="https://twitter.com/" target="_blank">
							<i class="social_twitter"></i>
						</a>
					</li>
					*/ ?>
				</ul>
			</div>
			<div class="item_center"></div>
			<div class="item cf">
				<ul class="menu cf">
					<li>
						<a href="<?php echo home_url(); ?>/">
							<span>トップ</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/service/">
							<span>サービス</span>
						</a>
					</li>

					<li>
						<a href="<?php echo home_url(); ?>/service/laboratory/">
							<span>オフショア開発</span>
						</a>
					</li>
					<li>
						<a href="https://www.calling.fun" target="_blank">
							<span>Calling</span>
						</a>
					</li>
					<!--li>
						<a href="<?php echo home_url(); ?>/kintone/">
							<span>kintone開発サポートサービス</span>
						</a>
					</li-->
					<li>
						<a href="<?php echo home_url(); ?>/service/professional/">
							<span>プロフェッショナルネットワーク</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/results/">
							<span>導入事例</span>
						</a>
					</li>
				</ul>
				<ul class="menu cf">
					<li>
						<a href="<?php echo home_url(); ?>/company/">
							<span>会社概要</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/neolabox/">
							<span>ニュース・イベント</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/glossary/">
							<span>用語集</span>
						</a>
					</li>
					<!--li>
						<a class="scroll" href="<?php echo home_url(); ?>/member/">
							<span>メンバー</span>
						</a>
					</li-->
					<li>
						<a class="scroll" href="<?php echo home_url(); ?>/contact/">
							<span>お問い合わせ</span>
						</a>
					</li>
					<li>
						<a class="scroll" href="<?php echo home_url(); ?>/policy/">
							<span>プライバシーポリシー</span>
						</a>
					</li>
				</ul>
			</div>
		</div><!-- /wrap -->
	</div>
	<div class="g_footer_secondary cf">
		<div class="wrap cf">
			<div class="contact">
				<span class="title wf2">CONTACT</span>
				<span class="mail">info@neo-lab.co.jp</span>
			</div>
			<div class="copy wf1">
				Copyright(c) NEOLAB CO.,LTD. All rights reserved.
			</div>
		</div>
	</div>
</footer>

<?php if ( is_home() || is_front_page() ) : ?>
</div><!-- #content loging topのみ -->
<?php endif; ?>


<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/smooth-scroll.min.js"></script>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/iscroll.min.js"></script>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/drawer.min.js"></script>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/common.js"></script>

<?php if(!is_mobile()): ?>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/wow.min.js"></script>
<script type="text/javascript">$(function() { new WOW().init(); });</script>
<?php endif; ?>

<?php if ( is_home() || is_front_page() ) : ?>
	<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
	;(function($) {
		$(function() {
			$('.neolab_slider.bxslider').bxSlider({
				maxSlides: 3,
				minSlides: 1,
				prevText: '<i class="arrow_carrot-left"></i>',
				nextText: '<i class="arrow_carrot-right"></i>',
			});
		});
	})(jQuery);
	</script>
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>
