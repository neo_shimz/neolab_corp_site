<?php
/*
	template Name: int-complete2
*/
?>

<?php get_header(); ?>

<main id="contact" class="g_main" role="main">
	<div class="second_visual cf">
		<h1 class="title">
			サマーインターンお申込み
		</h1>
	</div>
	<?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="g_title g_form cf">
		<div class="display cf">
			<h2 class="title">サマーインターンお申込みの完了</h2>
			<div class="step_wrap cf">
				<div class="step_inner cf">
					<div class="step done t-indent-1" data-desc="入力">
						<i class="radius"></i>
					</div>
					<div class="step done t-indent-2" data-desc="確認">
						<i class="radius"></i>
					</div>
					<div class="step active t-indent-3" data-desc="完了">
						<i class="radius"></i>
					</div>
				</div>
			</div>
			<div class="contact_complete cf">
				<h3>お申込みいただき誠に有り難う御座いました。</h3>
				<p>
					ご記入を頂きましたご連絡先に担当のものより近日中にご連絡をさせて頂きます。<br>
					大変恐縮で御座いますが少々お待ち頂けますと幸いで御座います。
				</p>
			</div>
			<ul class="btns mt4 btn_center cf">
				<li>
					<a href="<?php echo home_url(); ?>/" class="btn_bor_orange">
						<span>TOPページへ戻る</span>
						<i class="arrow_carrot-right"></i>
						<span class="hover"></span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>


<?php get_footer(); ?>