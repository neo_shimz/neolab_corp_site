<?php
/*
	template Name: glossary
*/
?>

<?php get_header(); ?>

<main id="glossary" class="g_main" role="main">
	<div class="second_visual cf">
		<h1 class="title b0 wow fadeInUp cf" data-wow-delay=".4s">
			用語集
		</h1>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="wow fadeInUp cf" data-wow-delay=".8s">
		<div style="padding: 3em 0 5em 0; text-align: center; font-size: 2em;">
			ただいま準備中です
		</div>
	</div>
	<?php include_once "inc/c_parts_development_service.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>

<?php get_footer(); ?>