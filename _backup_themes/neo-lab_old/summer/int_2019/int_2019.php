<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="initial-scale=1.0, width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>2019年新卒 サマーインターンシップ | ネオラボ</title>
<link rel="icon" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/favicon.jpg">
<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/icon-apple.png">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/css/int_2019style.css">
<!--link rel="stylesheet" type="text/css" href="./font/linea_complete/style.css"-->
<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- ogp -->
<meta name="description" content="ベトナムで企画実施ができる実践型インターンシップ”Wow Hack”のご案内です。大学１・2年生も参加可能！夏休みにVRやドローン事業を行うネオラボで暑い３日間を過ごしませんか？優勝チームはベトナムにて企画実施！！" />
<link rel="canonical" href="<?php echo home_url(); ?>/int_2019/" />
<meta property="og:title" content="2019年新卒向け サマーインターンシップ | ネオラボ" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo home_url(); ?>/int_2019/" />
<meta property="og:image" content="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/ogp/ogp.png" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="620" />
<meta property="og:site_name" content="オフショア開発×人材サービスの融合｜ネオラボ [NeoLab]" />
<meta property="og:description" content="ベトナムで企画実施ができる実践型インターンシップ”Wow Hack”のご案内です。大学１・2年生も参加可能！夏休みにVRやドローン事業を行うネオラボで暑い３日間を過ごしませんか？優勝チームはベトナムにて企画実施！！" />
<meta name="twitter:card" content="photo">
<meta name="twitter:title" content="2019年新卒向け サマーインターンシップ | ネオラボ" />
<meta name="twitter:description" content="ベトナムで企画実施ができる実践型インターンシップ”Wow Hack”のご案内です。大学１・2年生も参加可能！夏休みにVRやドローン事業を行うネオラボで暑い３日間を過ごしませんか？優勝チームはベトナムにて企画実施！！" />
<meta name="twitter:image" content="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/ogp/ogp.png" />
<meta itemprop="image" content="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/ogp/ogp.png" />
<!-- ogp -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-69406234-2', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_head(); ?>
</head>
<?php $slug_name = $post->post_name; ?>
<body class="<?php echo esc_attr( $post->post_name ); ?>">


<div id="content" class="cf">

	<div class="top_visual_wrap cf">
		<div class="top_visual cf">
			
			<div class="mv_sns_icn">
            <?php
			$url_encode=urlencode(get_permalink());
			$title_encode=urlencode(get_the_title()).'｜'.get_bloginfo('name');
		?>
			    <ul>
			        <li>
			        <a href="http://twitter.com/intent/tweet?url=<?php echo $url_encode ?>&text=<?php echo $title_encode ?>&tw_p=tweetbutton" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					<svg 
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         width="25px" height="20px">
                        <path fill-rule="evenodd"  fill="rgb(85, 172, 238)"
                         d="M25.000,2.367 C24.080,2.769 23.092,3.040 22.054,3.162 C23.113,2.538 23.927,1.548 24.309,0.369 C23.318,0.948 22.221,1.368 21.052,1.594 C20.117,0.613 18.784,0.000 17.309,0.000 C14.476,0.000 12.179,2.260 12.179,5.049 C12.179,5.445 12.225,5.830 12.312,6.199 C8.049,5.989 4.270,3.979 1.740,0.924 C1.299,1.670 1.046,2.537 1.046,3.462 C1.046,5.214 1.951,6.760 3.328,7.665 C2.487,7.639 1.696,7.411 1.004,7.034 C1.004,7.054 1.004,7.076 1.004,7.097 C1.004,9.543 2.772,11.584 5.118,12.048 C4.688,12.163 4.235,12.225 3.767,12.225 C3.436,12.225 3.115,12.193 2.802,12.135 C3.455,14.140 5.349,15.600 7.593,15.641 C5.838,16.995 3.626,17.802 1.223,17.802 C0.809,17.802 0.401,17.778 -0.000,17.732 C2.270,19.164 4.966,20.000 7.862,20.000 C17.297,20.000 22.456,12.306 22.456,5.634 C22.456,5.416 22.451,5.198 22.441,4.981 C23.443,4.269 24.312,3.380 25.000,2.367 Z"/>
                        </svg>
                        <?php if(function_exists('scc_get_share_twitter')) echo (scc_get_share_twitter()==0)?'':scc_get_share_twitter(); ?>
                    </a>
                    </li>
			        <li>
			        <a href="http://www.facebook.com/sharer.php?src=bm&u=<?php echo $url_encode;?>&t=<?php echo $title_encode;?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
                        <svg 
                     xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     width="20px" height="20px">
                    <path fill-rule="evenodd"  fill="rgb(59, 89, 152)"
                     d="M18.897,0.000 L1.103,0.000 C0.493,0.000 -0.000,0.493 -0.000,1.103 L-0.000,18.897 C-0.000,19.506 0.493,20.000 1.103,20.000 L10.703,20.000 L10.703,12.226 L8.086,12.226 L8.086,9.257 L10.703,9.257 L10.703,7.010 C10.703,4.427 12.260,3.008 14.565,3.008 C15.669,3.008 16.637,3.083 16.914,3.120 L16.914,5.859 L15.277,5.859 C14.024,5.859 13.790,6.455 13.790,7.329 L13.790,9.257 L16.789,9.257 L16.400,12.226 L13.790,12.226 L13.790,20.000 L18.897,20.000 C19.506,20.000 20.000,19.506 20.000,18.897 L20.000,1.103 C20.000,0.493 19.506,0.000 18.897,0.000 Z"/>
                    </svg>
                    <?php if(function_exists('scc_get_share_facebook')) echo (scc_get_share_facebook()==0)?'':scc_get_share_facebook(); ?>
                </a>
			        </li>
			        <!--li>instagram</li-->
			    </ul>
			</div>
			<h1 class="logo">
				<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/neo_logo.svg" alt="neolab SUMMER INTERNSHIP 2019">
				<br>
				<div class="lpttl">
				<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/header_title_su_in.svg" alt="neolab SUMMER INTERNSHIP 2019"></div>
			 </h1>
			<div class="main_block">
				<div class="top_visual_description cf">
                       <h2>
                        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/mv_wowhack.svg" alt="Wowhack">
                        </h2>
					    <br>
					<p class="txt1">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/mv_copy.svg" alt="ベトナムで企画実施ができる 実践型インターンシップ">
					</p>
				</div>
			</div>
			<div class="bg_mv"></div>
		</div>
	</div>
	
	<main role="main" id="main" class="main">

		<section class="s1_block cf">
			<div class="c_title_block">
				<h1 class="title" style="margin-top: 0em;">
				    驚きをHackせよ
				</h1>
			</div>
			<div class="inner cf">
				<div class="text_wrap">
					<p>テクノロジーと聞いて何を思い浮かべるだろうか？<br>
                    車に飛行機、身近なところだとパソコンにスマートフォン。<br>
                    はたまた、はやりのAIを浮かべる人もいるかもしれない。<br>
                    テクノロジーとは何か。<br>
                    誤解を恐れず、一つの回答を出すとするならば、<br><br>
                    <strong>”人の心をワクワクさせるモノ”</strong><br><br>
                    ではないだろうか。<br>
                    技術は人の心を動かしてこそテクノロジーだ。<br>
                    人の心をワクワクさせる。<br>
                    それが、真に価値あるテクノロジー。<br>
                    それが、<strong>Wow Hack</strong>だ。
                    </p>
				</div>
			</div>
			<div class="inner_sp cf">
				<div class="text_wrap">
					<p>テクノロジーと聞いて何を思い浮かべるだろうか？車に飛行機、身近なところだとパソコンにスマートフォン。はたまた、はやりのAIを浮かべる人もいるかもしれない。<br>
                    テクノロジーとは何か。<br>
                    誤解を恐れず、一つの回答を出すとするならば、<br>
                    <strong>”人の心をワクワクさせるモノ”</strong><br><br>
                    ではないだろうか。<br>
                    技術は人の心を動かしてこそテクノロジーだ。人の心をワクワクさせる。それが、真に価値あるテクノロジー。<br>
                    それが、<strong>Wow Hack</strong>だ。
                    </p>
				</div>
			</div>
		</section><!-- /s1_block  -->

		<section class="s2_block cf">
			<div class="c_title_block">
				<h1 class="cl_bl">
				    実施概要
				</h1>
			</div>
			<div class="s2_subtxt">
				<p>Wow Hackでは、2泊3日で企画立案を行う。4～5名のチームを組み、最終提案を経て優勝を目指すインターンシップである。単に、発表して終わらせるのではもったいない。コンテストで優勝したチームは実際にベトナムへ行き、企画したアイデアを実行まで移す。まさに、企画実践型のインターンシップである。</p>
			</div>
			<div class="s2-outline_flow cf">
				<ul class="cf">
					<li class="flow_bl cf">
							<img class="fl" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/s2_image_01.jpg" alt="体験する">
                        <div class="inner">
                            <h2 class="flow01">
                                体験する
                            </h2>
                            <ul class="">
                                <li>最新のVR機器でテクノロジーを体感</li>
                                <li>映画にも使用されたモーションキャプチャーを体感</li>
                                <li>ドローンビジネスの最前線のレクチャー</li>
                            </ul>
						</div>
					</li>
					<li class="flow_bl cf">
							<img class="fr" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/s2_image_02.jpg" alt="企画する">
						<div class="inner">
                            <h2 class="flow02">
                                企画する
                            </h2>
                            <ul class="">
                                <li>現場のヒアリングから課題を設定</li>
                                <li>サービス立案には技術専門社員からのサポート</li>
                                <li>最終発表では経営陣からの実践的なフィードバック</li>
                            </ul>
						</div>
					</li>
					<li class="flow_bl cf">
							<img class="fl" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/s2_image_03.jpg" alt="実施する">
				        <div class="inner">
                            <h2 class="flow03">
                                実施する
                            </h2>
                            <ul class="">
                                <li><strong>優勝チームは実際にベトナムで立案した案を実施</strong></li>
                                <li>その他にも賞品多数用意</li>
                            </ul>
						</div>
					</li>
				</ul>
			</div>
		</section><!-- /block_section s1 -->

		<section class="s3_block cf">
			<div class="c_title_block">
				<h1 style="margin-top: 0em;">
					募集要項
				</h1>
			</div>

			<div class="recruit_blocks cf">
				<div class="inner cf">
					<dl>
					    <dt>開催期間</dt>
					    <dd>2017年　8月8日〜10日</dd>
				    </dl>
				    <dl class="cf">
					    <dt>場所</dt>
					    <dd>ネオラボ本社 / ベルサール新宿グラント</dd>
				    </dl>
				    <dl class="cf">
					    <dt>募集人数</dt>
					    <dd>24名</dd>
				    </dl>
                    <dl class="cf">    
				        <dt>参加資格</dt>
					    <dd>大学１〜３年生、大学院１年生の方<br>高等専門学校４〜５年生の方</dd>
					</dl>
                    <dl>
                       <dt>エントリー後のフロー</dt>
					   <dd>(1)エントリーシートの提出  >  (2)書類選考  >  (3)選考会  >  (4)当日参加</dd>
					</dl>
				</div>
				<div class="btn_lg_w cf">
					<a href="/int-contact/" target="_blank">
						<span>
							エントリーする
						</span>
					</a>
            </div>
            </div>
        </section>

		<section class="s4_block cf">
				<div class="c_title_block">
					<h1 class="cl_bl">
						社長メッセージ
					</h1>
				</div>
				<div class="inner cf">
					<p class="">『ワクワクする』とは、一体何なのでしょうか？<br><br>

                    子どもの頃、クリスマスイブの夜にサンタクロースからの贈り物に思いをめぐらせながら眠りにつく夜のワクワク。<br>
                    学校と家の往復という狭い世界しか知らなかった頃、初めてWebに触れて、世界の広さを知った時のワクワク。<br>
                    不安も混じりながらも未来への希望を持ち、前に進もうとする就活生のワクワク。<br><br>

                    世の中には様々なワクワクが存在します。<br><br>

                    辞書によれば、「これから起こるであろう素晴らしいことを考えて高揚するさま」（weblio類語辞典より）<br>
                    これほど前向きで活動的な感情はありません。<br><br>

                    創業から1年半、グローバル全体で250名を超えるメンバーと共に急成長してきたネオラボでは、<br>
                    常に世の中をワクワクさせるサービスを提供し続けることを意識してきました。<br>

                    この『Wow Hack』はVRやドローンのテクノロジーを用いて「驚き」を科学し、<br>
                    サービスを開発する企画提案型インターンシップです。<br><br>

                    最も優れた企画提案については実行に移すべく、<br>
                    ネオラボが展開しているベトナム中部地方のダナンを中心とした研修旅行に参加いただきます。<br><br>

                    2050年には全世界GDPの50%を占めるとされる成長著しいアジアマーケットの中、<br>
                    弊社のメンバーと共に「驚き」とは一体何なのか？　を徹底的に考え抜き、<br>
                    ヒトをテクノロジーでワクワクさせてみたい。<br><br>

                    そんな好奇心旺盛な方の参加をお待ちしています。<br><br><br>

                    <strong>株式会社ネオラボ 取締役社長  大川智弘</strong>
                    </p>
				</div>
				<div class="inner_sp cf">
					<p class="">『ワクワクする』とは、一体何なのでしょうか？<br><br>

                    子どもの頃、クリスマスイブの夜にサンタクロースからの贈り物に思いをめぐらせながら眠りにつく夜のワクワク。学校と家の往復という狭い世界しか知らなかった頃、初めてWebに触れて、世界の広さを知った時のワクワク。不安も混じりながらも未来への希望を持ち、前に進もうとする就活生のワクワク。<br><br>

                    世の中には様々なワクワクが存在します。<br><br>

                    辞書によれば、「これから起こるであろう素晴らしいことを考えて高揚するさま」（weblio類語辞典より）これほど前向きで活動的な感情はありません。<br><br>

                    創業から1年半、グローバル全体で250名を超えるメンバーと共に急成長してきたネオラボでは、常に世の中をワクワクさせるサービスを提供し続けることを意識してきました。<br>

                    この『Wow Hack』はVRやドローンのテクノロジーを用いて「驚き」を科学し、サービスを開発する企画提案型インターンシップです。<br><br>

                    最も優れた企画提案については実行に移すべく、ネオラボが展開しているベトナム中部地方のダナンを中心とした研修旅行に参加いただきます。<br><br>

                    2050年には全世界GDPの50%を占めるとされる成長著しいアジアマーケットの中、弊社のメンバーと共に「驚き」とは一体何なのか？　を徹底的に考え抜き、ヒトをテクノロジーでワクワクさせてみたい。<br><br>

                    そんな好奇心旺盛な方の参加をお待ちしています。<br><br><br>

                    <strong>株式会社ネオラボ 取締役社長  大川智弘</strong>
                    </p>
				</div>
		</section>

		<section class="s5_block cf">
			<div class="c_title_block">
				<h1 style="margin-top: 0em;">
					ネオラボ経営陣のエモい
				</h1>
            </div>
				<div class="inner cf">
					<dl>
					    <dt><img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/s5_mana_01.png" alt="大川智弘"></dt>
					    <dd><h2>＼一緒に新しいWOWを作りましょう／</h2>
                            <h3>大川智弘</h3>
                            <p>学生時代から個人事業主として中小企業・NPOの経営コンサルティングを行う。ランサーズ株式会社入社後、新規事業開発チームリーダーに就任。株式会社ネオキャリア入社後、新規Webサービスのディレクターとして、サービスリリースを実現。子会社出向後は経営企画・事業戦略担当として、組織成長・再生・新規事業開発などの課題解決に従事。その後、ラボ型オフショア開発サービスを展開する株式会社ネオラボ立ち上げに参画し、同社の取締役社長就任。</p>
                        </dd>
				    </dl>
				    <dl>
					    <dt><img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/s5_mana_02.png" alt="酒井佑介"></dt>
					    <dd>
                           <h2>＼全力をぶつけにきてください／</h2>
                            <h3>酒井佑介</h3>
                            <p>UFJ銀行(現東京三菱UFJ銀行)に新卒で入社。大手企業向けコーポレートファイナンス等の業務に従事。リクルートHRカンパニー(現リクルートキャリア)では法人営業として全国年間MVP獲得。ランサーズ株式会社入社後は、営業部長と新規事業を兼任してKDDIとのアライアンス締結等実現。その後、株式会社ネオラボの代表取締役会長CEOとして同社の垂直立ち上げに貢献。</p>
                        </dd>
				    </dl>
				    <dl>
					    <dt><img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/s5_mana_03.png" alt="東出康義"></dt>
					    <dd>
                           <h2>＼Change before you have to.／</h2>
                            <h3>東出康義</h3>
                            <p>株式会社Speeeに新卒で入社。既存顧客に対するSEOのコンサルティングを担当。2013年にJibe Mobile(現Automagi)に転職。主に、米国のベンチャー企業の日本展開と通信事業社向け購買交渉を担当し、米国企業の日本国内の独占販売権の獲得と国内最大手メーカーへのライセンス提供に貢献。2015年9月からネオラボ設立に携わり、現在はマーケティング、新規顧客の開拓を中心とした執行役員兼CMOを担う。</p>
                        </dd>
				    </dl>
				</div>
        </section><!-- /block_section s2 -->
		
		<section class="s6_block cf">
			<div class="c_title_block">
				<h1 class="cl_bl" style="margin-top: 0em;">
					NEOLABとは
				</h1>
				<div class="s6_about_neolab cf">
					<div class="aboutimg fr">
                        <a href="https://neo-lab.co.jp/" target="_blank">
					        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/s6_image_site.png" alt="NEOLABとは">
                        </a>
                    </div>
					<p>ネオラボは、新しい未来の可能性を「オープン化」で追求している企業です。<br>
                    オフショア事業では、日本のグローバル人材調達を加速させるために、<br>
                    日本企業の海外開発スタートアップを支援するプラットフォームサービスを提供しています。<br>
                    その他にもVRやドローンなど、最新テクノロジーを駆使した事業を多方面で展開しています。
                    </p>
                    <div class="btn_sm cf">
                        <a href="https://neo-lab.co.jp/" target="_blank">
                            <span>
                                公式サイトはこちら
                            </span>
                        </a>
                    </div>
				</div>
				
			</div>
			<div class="btn_lg cf">
					<a href="/int-contact/" target="_blank">
						<span>
							エントリーする
						</span>
					</a>
            </div>
		</section>
		<section class="s7_block cf">
    	<div class="share cf">
		<?php
			$url_encode=urlencode(get_permalink());
			$title_encode=urlencode(get_the_title()).'｜'.get_bloginfo('name');
		?>
		<div class="c_title_block">
				<h1 class="cl_bl" style="margin-top: 0em;">
					お問い合わせ
				</h1>
           <p><a href="mailto:wowhack@neo-lab.co.jp">wowhack@neo-lab.co.jp</a></p>
            </div>
		<ul class="cf">
			<li class="facebook">
				<a href="http://www.facebook.com/sharer.php?src=bm&u=<?php echo $url_encode;?>&t=<?php echo $title_encode;?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					<span class="icon-facebook">facebook</span>
					<?php if(function_exists('scc_get_share_facebook')) echo (scc_get_share_facebook()==0)?'':scc_get_share_facebook(); ?>
				</a>
			</li>
			<li class="tweet">
				<a href="http://twitter.com/intent/tweet?url=<?php echo $url_encode ?>&text=<?php echo $title_encode ?>&tw_p=tweetbutton" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					<span class="icon-twitter">tweet</span>
					<?php if(function_exists('scc_get_share_twitter')) echo (scc_get_share_twitter()==0)?'':scc_get_share_twitter(); ?>
				</a>
			</li>
			<li class="googleplus">
				<a href="https://plus.google.com/share?url=<?php echo $url_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;">
					<span class="icon-google-plus">Google+</span>
					<?php if(function_exists('scc_get_share_gplus')) echo (scc_get_share_gplus()==0)?'':scc_get_share_gplus(); ?>
				</a>
			</li>
			<li class="hatena">
				<a href="http://b.hatena.ne.jp/add?mode=confirm&url=<?php echo $url_encode ?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=510');return false;">
					<span class="icon-hatebu">はてブ</span>
					<?php if(function_exists('scc_get_share_hatebu')) echo (scc_get_share_hatebu()==0)?'':scc_get_share_hatebu(); ?>
				</a>
			</li>
			<?php if(is_mobile()) : ?>
			<li class="line">
				<a href="http://line.me/R/msg/text/?<?php echo $title_encode . '%0A' . $url_encode;?>">
					<span class="icon-line">LINE</span>
				</a>
			</li>
			<?php endif; ?>
			<li class="pocket">
				<a href="http://getpocket.com/edit?url=<?php echo $url_encode;?>&title=<?php echo $title_encode;?>"><span class="icon-pocket">Pocket</span>
					<?php if(function_exists('scc_get_share_pocket')) echo (scc_get_share_pocket()==0)?'':scc_get_share_pocket(); ?>
				</a>
			</li>
		</ul>
	</div>
	</section>
	</main><!-- /#main -->
    
	<footer class="g_footer cf" role="contentinfo">
		<div class="inner_first cf">
			<p class="items poricy">
				<a href="http://www.neo-career.co.jp/policy" target="_blank" class="op">
					<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/footer_poricy.png" alt="プライバシーポリシー">
				</a>
			</p>
		</div>
		<div class="inner_second cf">
			<p class="items logo">
				<a href="<?php echo home_url(); ?>">
					<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/footer_logo.svg" alt="ネオラボ">
				</a>
			</p>
			<p class="items scroll">
				<a href="<?php echo home_url(); ?>" data-scroll>
					<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/summer/int_2019/img/img/footer_scrolltop.png" alt="ネオラボトップに戻る">
				</a>
			</p>
		</div>
	</footer>

</div><!--/ #content -->


<!--
<script src="./js/style.min-dist.js"></script>

<script>
  // scroll
  $(window).load(function(){
    smoothScroll.init({
      selector: '[data-scroll]',                  // スムーススクロールが有効なリンクに付ける属性
      selectorHeader: '[data-scroll-header]',     // 固定ナビに付ける属性
      speed: 800,                                 // 到達するまでの総時間(ミリ秒)
      easing: 'easeOutQuart',                     // スピードの種類
      offset: 0,                                  // 到達場所からズラすピクセル数
      updateURL: true,                            // URLを[#〜]に変更するか？
      callback: function () {}                    // コールバック関数 (到達時に実行される関数)
    });
  });
</script>
-->
<!-- // start scrollbar --
<script src="./js/scrollbar/perfect-scrollbar.jquery.min.js"></script>
<script>
;(function($) {
	$(function() {
		$('.p_scrollbar').perfectScrollbar();
	});
})(jQuery);
</script>
!- // end scrollbar -->

<!-- // start skrollr --
<script src="./js/skrollr/skrollr.min.js"></script>
<script>
$(window).on('load', function() {
	if ($(window).width() > 768) {
		$(function() {
			// window が768 以下は実行しない
			var s = skrollr.init({
				edgeStrategy: 'set',
				easing: {
					WTF: Math.random,
					inverted: function(p) {
					  return 1-p;
					}
				}
			});
		});
	}
});
</script>
!-- // end skrollr -->

<?php wp_footer(); ?>
</body>
</html>