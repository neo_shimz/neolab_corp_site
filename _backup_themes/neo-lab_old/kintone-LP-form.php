<?php
/*
	template Name: kintone
*/
?>


    <?php get_header("LP"); ?>
    <main id="landing" class="LP_main" role="main">
      <div class="second_visual">
        <div class="second_visual_img">
          <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/second_visual_img.jpg" width="100%" alt="kintoneのカスタマイズ開発をサポートします">
        </div>
        <div class="second_visual_inner">
          <div class="second_visual_inner_set">
            <h1 class="wow fadeInUp" data-wow-delay="0.2s">kintoneのカスタマイズ開発をサポートします</h1>
            <p class="text_1 wow fadeInUp" data-wow-delay="0.8s">だれでも、簡単に<br>日々の業務を楽チンに。</p>
            <div class="second_visual_btn wow fadeInUp" data-wow-delay="1.4s">
              <a href="#kintone_inquiry">
                <span>まずは無料相談</span>
              </a>
            </div>
          </div>
        </div>

      </div>
<!--section style="margin: 0 auto 6em auto">
  <p style="font-size: 2em;">ChatWorkとKintoneをつなぐ新しいサービスを開始しました。ご利用開始はこちらから</p>
 <a href="https://zen-chat.work/" target="_blank"><img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/bnr_zenx.png"></a>
</section-->

      <section id="kintone_import" class="kintone_import">
        <div class="inner">
          <h2>kintone導入サポートサービス</h2>
          <ul class="kintone_import_list cf">
            <li class="wow fadeInUp" data-wow-delay="0.1s">
              <p class="title"><span>カスタマイズ<br>コンサルティング</span></p>
              <p class="text">kintone導入から、社内アプリケーションの開発まで、あらゆるプロセスにおける研修・トレーニングにより、どなたでもkintoneを使いこなせるようになります。</p>
            </li>
            <li class="wow fadeInUp" data-wow-delay="0.6s">
              <p class="title"><span>業務フロー最適化</span></p>
              <p class="text">kintoneのアプリケーション開発は社内業務のムダを省く作業になります。<br>今まで見落としていた必要のない業務プロセスを発見、効率よく改善していきます。</p>
            </li>
            <li class="wow fadeInUp" data-wow-delay="1.1s">
              <p class="title"><span>業務改善の内製化</span></p>
              <p class="text">kintoneによるアプリケーション開発をマスターすることにより、効率の良い業務改善を内製化ができます。</p>
            </li>
          </ul>
        </div>
      </section>
      <section id="kintone_merit" class="kintone_merit">
        <div class="inner">
          <h2>kintone導入メリット</h2>
          <ul class="kintone_merit_list cf">
            <li class="wow fadeIn" data-wow-delay="0.5s">
              <p class="title">手作業を大幅に<br>減らすことができる</p>
              <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/kintone_case_list_img_001.png" alt="手作業を大幅に減らすことができる">
              <p class="text">今まで手作業で２〜3日かかっていたような作業が、kintoneを導入することでPC上で数分の作業で完了してしまうことも。作業効率を引き上げ、より重要な仕事に時間が割けるようになります。</p>
            </li>
            <li class="wow fadeIn" data-wow-delay="1.1s">
              <p class="title">確認作業の抜け漏れを<br>大幅に減らせる</p>
              <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/kintone_case_list_img_002.png" alt="確認作業の抜け漏れを大幅に減らせる">
              <p class="text">これは誰に確認を取ればいいのか…?しまった、確認するの忘れてしまった！？なんてことありませんか？kintoneならこうした抜け漏れを防げます。</p>
            </li>
            <li class="wow fadeIn" data-wow-delay="1.7s">
              <p class="title">未処理のレコードが<br>何件あるのかがすぐわかる</p>
              <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/kintone_case_list_img_003.png" alt="未処理のレコードが何件あるのかがすぐわかる">
              <p class="text">未処理になっているレコードを一つ一つ確認していくだけで、多くの時間が割かれてしまう。kintoneなら、未処理だけでなく完了されたレコードも瞬時に可視化し、データの管理も楽々です。</p>
            </li>
          </ul>
        </div>
      </section>
      <section id="kintone_dev" class="kintone_dev">
        <div class="inner">
          <h2>kintone導入サポートサービス</h2>
          <ul class="kintone_dev_list cf">
            <li>
              <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/kintone_dev_list_img_001.png" alt="帳票発行">
              <p class="title">帳票発行</p>
              <p class="text">今までエクセルや手作業による業務にて行っていた見積書や発注書などの帳票発行が、kintoneにて専用のアプリケーションを開発することで自動でなされ、業務がスッキリします。</p>
            </li>
            <li>
              <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/kintone_dev_list_img_002.png" alt="bot機能">
              <p class="title">bot機能</p>
              <p class="text">業務プロセスにおいての承認や確認漏れを防ぐために、chatworkなどのコミュニケーションツールと連携することで、リマインダーを送ることもできます。</p>
            </li>
            <li>
              <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/lp/kintone_dev_list_img_003.png" alt="高度なカスタマイズ">
              <p class="title">高度なカスタマイズ</p>
              <p class="text">その他にもご要望に応じて、最適なカスタマイズに対応させていただきます。kintoneではJava Scriptを使用することで、さらなる機能を拡充できます。kintoneの可能性は無限大！</p>
            </li>
          </ul>
        </div><!-- kintone_dev -->


      </section>
      <section id="kintone_inquiry" class="kintone_inquiry">
        <div class="inner">
          <h2>無料相談</h2>
					<?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
            <div class="display text_bottom cf">
             <p>弊社「<a href="<?php echo home_url(); ?>/policy/" target="_blank">プライバシーポリシー</a>」を必ずご確認・ご同意の上、お問い合わせください。</p>
            </div>
        </div>
      </section>
    </main>
  </div>
  <script>
  $(function(){
    var KintoneImportTop = $("#kintone_import").offset().top;
    var KintoneImportTopFlag = false;
    var KintoneImportBlock001 = $(".kintone_import_list li:nth-child(1)");
    var KintoneImportBlock002 = $(".kintone_import_list li:nth-child(2)");
    var KintoneImportBlock003 = $(".kintone_import_list li:nth-child(3)");


    var KintoneDevTop = $("#kintone_dev").offset().top;;
    var KintoneDevTopFlag = false;
    var KintoneDevBlock001 = $(".kintone_dev_list li:nth-child(1)");
    var KintoneDevBlock002 = $(".kintone_dev_list li:nth-child(2)");
    var KintoneDevBlock003 = $(".kintone_dev_list li:nth-child(3)");

    // $(window).on("scroll", function(){
    //    if ($(this).scrollTop() > KintoneImportTop) {
    //     if(KintoneImportTopFlag==false){
    //       KintoneImportBlock001.addClass("show");
    //       KintoneImportBlock002.addClass("show");
    //       KintoneImportBlock003.addClass("show");
    //     }
    //     KintoneImportTopFlag = true;
    //    }
    //
    //    if ($(this).scrollTop() > KintoneDevTop/5) {
    //     if(KintoneDevTopFlag==false){
    //       KintoneDevBlock001.addClass("show");
    //       KintoneDevBlock002.addClass("show");
    //       KintoneDevBlock003.addClass("show");
    //     }
    //     KintoneDevTopFlag = true;
    //    }
    // });

    var LPHeaderInquiry = $(".LP_header_nav li:nth-child(1) a");
    var SecondVisualBtn = $(".second_visual_btn a");

    $(".LP_header_nav li:nth-child(1) a,.second_visual_btn a").click(function(){
        event.preventDefault();
      var target= $(this).attr("href");
      var targetH=$(target).offset().top;
      $("body,html").stop().animate({scrollTop:targetH},800);
    })




  })

  </script>
<?php get_footer(); ?>
