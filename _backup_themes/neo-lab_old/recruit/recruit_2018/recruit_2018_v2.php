<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="initial-scale=1.0, width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>2018年新卒向け採用選考「LAB TRIP」 | ネオラボ</title>
<link rel="icon" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/favicon.jpg">
<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/icon-apple.png">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/css/recruit_2018_v2.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/font/linea_complete/style.css">
<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
<style>
.gf1 { font-family: 'Slabo 27px', serif; }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- ogp -->
<meta name="description" content="ネオラボの2018年新卒向け特別選考会「LAB TRIP」のご案内です。弊社社長が現地(地方)で直接面接！東京での最終面接後、内定者には三ヶ月以上の海外研修も！ネオラボはVR事業、ドローン事業、オフショア開発事業など最先端のサービスをお客様に提供している会社です。" />
<link rel="canonical" href="<?php echo home_url(); ?>/recruit_2018_v2/" />
<meta property="og:title" content="2018年新卒向け特別採用選考「LAB TRIP」| ネオラボ" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo home_url(); ?>/recruit_2018_v2/" />
<meta property="og:image" content="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/ogp/ogp_v2.gif" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="620" />
<meta property="og:site_name" content="オフショア開発×人材サービスの融合｜ネオラボ [NeoLab]" />
<meta property="og:description" content="ネオラボの2018年新卒向け特別選考会「LAB TRIP」のご案内です。弊社社長が現地(地方)で直接面接！東京での最終面接後、内定者には三ヶ月以上の海外研修も！ネオラボはVR事業、ドローン事業、オフショア開発事業など最先端のサービスをお客様に提供している会社です。" />

<meta name="twitter:card" content="photo">

<meta name="twitter:title" content="2018年新卒向け特別採用選考「LAB TRIP」| ネオラボ" />
<meta name="twitter:description" content="ネオラボの2018年新卒向け特別選考会「LAB TRIP」のご案内です。弊社社長が現地(地方)で直接面接！東京での最終面接後、内定者には三ヶ月以上の海外研修も！ネオラボはVR事業、ドローン事業、オフショア開発事業など最先端のサービスをお客様に提供している会社です。" />
<meta name="twitter:image" content="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/ogp/ogp_v2.gif" />
<meta itemprop="image" content="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/ogp/ogp_v2.gif" />
<!-- ogp -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-69406234-1', 'auto');
	ga('send', 'pageview');

</script>


</head>
<?php $slug_name = $post->post_name; ?>

<body class="<?php echo esc_attr( $post->post_name ); ?> loading">

<div id="loading_wrap" class="cf">
	<div id="loading" class=" cf">
		<img class="loding_bubbles" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/common/loading-bubbles.svg" alt="">
		<img class="loding_text" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/loding.svg" alt="">
	</div>
</div>

<div id="content" class="cf">

	<div class="top_visual_wrap cf">
		<div class="top_visual cf">
			<div class="bg_cloud_light_blue"></div>
			<div class="bg_cloud_blue"></div>
			<div class="bg_airplane"></div>
			<div class="bg_Landscape">
			</div>
			<h1 class="logo">
				<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-visual_label.svg" alt="2018新卒特別採用 ネオラボ">
			</h1>
			<div class="main_block">
				<div class="top_visual_description cf">
					<h2 class="ttl1" id="rev-1">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-visual_text1.svg" alt="LABTRIP(ジモ活)とは">
					</h2><br>
					<p class="txt1" id="rev-2">
						<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-visual_text2.svg" alt="ネオラボ東京本社から社長が、面接官として、あなたの地元に。">
					</p>
				</div>
				<p class="shimekiri_label">
					<!--
					<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-visual_shimekiri.svg" alt="応募締切">
					<span class="data">4<i>月</i>25<i>日</i></span>
					-->
					<span class="data" style="top: 60px;">通年採用</span>
				</p>
			</div>
		</div>
	</div>
	<div class="modal_btn_wrap">
		<a href="#" class="modal_btn open-remodal op">
			<svg x="0px" y="0px" width="230px" height="230px">
				<clipPath id="frame">
					<path>
						<animate dur="1s" repeatCount="indefinite" attributeName="d" attributeType="XML" values=" M148.145,9.644c0,0,67.558,14.52,68.826,85.875c5.361,17.995,5.048,44.192-12.947,73.24c-27.153,36.303-45.461,60.927-120.283,49.25 C44.91,206.33,9.555,170.965,4.185,126.138C5.766,75.629,23.439,49.11,41.122,36.163C61.958,21.957,78.372-3.303,148.145,9.644z; M148.145,9.644c0,0,63.855,3.356,65.826,81.875c0.471,18.771,3.77,53.917-14.273,82.935C172,219,158.563,221.686,83.741,210.008 C44.91,198.33,6.185,171.286,6.185,126.138C6.185,68,23.318,40.947,41,28C61.836,13.793,78.372-3.303,148.145,9.644z; M152.5,10c0,0,45.5,3.5,56,82.5c5.509,35.141,10.197,66-1,84.5c-25,35.5-40,56.5-128,32c-66-17.5-72.315-37-72.315-83.861 C7.185,107.951,0,43.5,41.5,26.5C66,15,83.5,0.5,152.5,10z; M148.145,14.644c0,0,64.467,7.837,66.438,86.356c0.471,18.771,3.55,40.683-14.884,69.454C175,209,163,213.012,83.741,210.008 c-43.003-1.63-79.557-46.722-79.557-91.87C4.185,57.333,20.318,41.947,38,29C61.91,12.698,75.297,1.127,148.145,14.644z; M148.145,9.644c0,0,67.558,14.52,68.826,85.875c5.361,17.995,5.048,44.192-12.947,73.24c-27.153,36.303-45.461,60.927-120.283,49.25 C44.91,206.33,9.555,170.965,4.185,126.138C5.766,75.629,23.439,49.11,41.122,36.163C61.958,21.957,78.372-3.303,148.145,9.644z">
						</animate>
					</path>
				</clipPath>
				<image clip-path="url(#frame)" height="100%" width="100%" xlink:href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-modal_btn.png">
			</svg>
		</a>
	</div>


	<main role="main" id="main" class="main">

		<section class="s0_block cf">
			<div class="c_title_block">
				<svg class="c_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" width="209.2px" height="179.2px" viewBox="0 0 209.2 179.2" style="enable-background:new 0 0 209.2 179.2;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#0071bd;}
					</style>
					<g>
						<path class="st0" d="M56.5,178.9l-0.3,0c-8.2,1.3-16-4.2-17.3-12.5l-17.5-52.3l30.1-4.9l17.5,52.3C70.3,169.8,64.7,177.6,56.5,178.9z"/>
						<ellipse transform="matrix(0.9869 -0.1612 0.1612 0.9869 -12.0735 20.058)" class="st0" cx="117.6" cy="84.4" rx="13.9" ry="13.9"/>
						<path class="st0" d="M110,27.3l18.2,111.3c0.7,4.4-3.6,7.9-7.7,6.3l-0.4-0.2c-26.2-9.9-55.6-18.5-83.2-13.9l-11.3,1.8c-9.4,1.5-18.3-4.9-19.9-14.3L0.2,85.5c-1.5-9.4,4.9-18.3,14.3-19.9l11.3-1.8c27.7-4.5,52.8-21.9,74.5-39.7l0.3-0.3C104.1,21,109.3,22.9,110,27.3z"/>
						<path class="st0 lTop" d="M151.7,30.7c-1.1,0.2-2.3-0.1-3.3-0.8c-2-1.4-2.4-4.2-1-6.2l15.7-21.9c1.4-2,4.2-2.4,6.2-1c2,1.4,2.4,4.2,1,6.2l-15.7,21.9C153.9,29.9,152.8,30.5,151.7,30.7z"/>
						<path class="st0 lMiddle" d="M205.5,72l-45.8,7.5c-2.4,0.4-4.7-1.2-5.1-3.7c-0.4-2.4,1.2-4.7,3.7-5.1l45.8-7.5c2.4-0.4,4.7,1.2,5.1,3.7C209.5,69.3,207.9,71.6,205.5,72z"/>
						<path class="st0 lBottom" d="M186.7,139.4c-1.1,0.2-2.3-0.1-3.3-0.8l-19.6-14.1c-2-1.4-2.4-4.2-1-6.2c1.4-2,4.2-2.4,6.2-1l19.6,14.1c2,1.4,2.4,4.2,1,6.2C188.9,138.6,187.8,139.2,186.7,139.4z"/>
					</g>
				</svg>
				<h1 class="title" style="margin-top: 0em;">
					<span class="item">
						<span class="ttl_before">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-s0-title_1.svg" alt="LAB TRIP(ジモ活)への想い">
						</span>
					</span>
				</h1>
			</div>
			<div class="inner cf">
				<svg class="square_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="359px" height="360px" data-0="top:0;" data-200="top:220px;">
					<path fill-rule="evenodd" stroke="rgb(227, 9, 32)" stroke-width="0px" stroke-dasharray="0, 0" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(254, 235, 8)" d="M20.000,0.000 L339.000,0.000 C350.046,0.000 359.000,8.954 359.000,20.000 L359.000,340.000 C359.000,351.046 350.046,360.000 339.000,360.000 L20.000,360.000 C8.954,360.000 0.000,351.046 0.000,340.000 L0.000,20.000 C0.000,8.954 8.954,0.000 20.000,0.000 Z">
				</svg>
				<svg class="square_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="245px" height="156px"  data-0="top:303px;" data-200="top:80px;">
					<path fill-rule="evenodd"  stroke="rgb(227, 9, 32)" stroke-width="0px" stroke-dasharray="0, 0" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(254, 235, 8)" d="M20.000,0.000 L225.000,0.000 C236.046,0.000 245.000,8.954 245.000,20.000 L245.000,136.000 C245.000,147.046 236.046,156.000 225.000,156.000 L20.000,156.000 C8.954,156.000 0.000,147.046 0.000,136.000 L0.000,20.000 C0.000,8.954 8.954,0.000 20.000,0.000 Z">
				</svg>
				<div class="text_wrap">
					<p class="text_1">
						<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-s0-text_1.svg" alt="">
					</p>
					<p class="text_2">
						<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-s0-text_2.svg" alt="">
					</p>
					<p class="text_3">
						<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-s0-text_3.svg" alt="">
					</p>
					<p class="text_4">
						<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-s0-text_4.svg" alt="">
					</p>
					<div class="btn">
						<a href="#" class="open-remodal op">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-s0-text_5.svg" alt="">
						</a>
					</div>
				</div>
			</div>
		</section><!-- /s0_block s2 -->

		<section class="block_section s1_block cf">
			<div class="c_title_block">
				<svg class="c_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" width="209.2px" height="179.2px" viewBox="0 0 209.2 179.2" style="enable-background:new 0 0 209.2 179.2;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#0071bd;}
					</style>
					<g>
						<path class="st0" d="M56.5,178.9l-0.3,0c-8.2,1.3-16-4.2-17.3-12.5l-17.5-52.3l30.1-4.9l17.5,52.3C70.3,169.8,64.7,177.6,56.5,178.9z"/>
						<ellipse transform="matrix(0.9869 -0.1612 0.1612 0.9869 -12.0735 20.058)" class="st0" cx="117.6" cy="84.4" rx="13.9" ry="13.9"/>
						<path class="st0" d="M110,27.3l18.2,111.3c0.7,4.4-3.6,7.9-7.7,6.3l-0.4-0.2c-26.2-9.9-55.6-18.5-83.2-13.9l-11.3,1.8c-9.4,1.5-18.3-4.9-19.9-14.3L0.2,85.5c-1.5-9.4,4.9-18.3,14.3-19.9l11.3-1.8c27.7-4.5,52.8-21.9,74.5-39.7l0.3-0.3C104.1,21,109.3,22.9,110,27.3z"/>
						<path class="st0 lTop" d="M151.7,30.7c-1.1,0.2-2.3-0.1-3.3-0.8c-2-1.4-2.4-4.2-1-6.2l15.7-21.9c1.4-2,4.2-2.4,6.2-1c2,1.4,2.4,4.2,1,6.2l-15.7,21.9C153.9,29.9,152.8,30.5,151.7,30.7z"/>
						<path class="st0 lMiddle" d="M205.5,72l-45.8,7.5c-2.4,0.4-4.7-1.2-5.1-3.7c-0.4-2.4,1.2-4.7,3.7-5.1l45.8-7.5c2.4-0.4,4.7,1.2,5.1,3.7C209.5,69.3,207.9,71.6,205.5,72z"/>
						<path class="st0 lBottom" d="M186.7,139.4c-1.1,0.2-2.3-0.1-3.3-0.8l-19.6-14.1c-2-1.4-2.4-4.2-1-6.2c1.4-2,4.2-2.4,6.2-1l19.6,14.1c2,1.4,2.4,4.2,1,6.2C188.9,138.6,187.8,139.2,186.7,139.4z"/>
					</g>
				</svg>
				<h1 class="title">
					<span class="item">
						<span class="ttl_before">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1_title_a.svg" alt="">
						</span>
					</span>
				</h1>
			</div>
			<div class="s1-labtrip_flow cf">
				<ul class="cf">
					<li>
						<figure class="img">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1-01_img.png" alt="">
						</figure>
						<span class="label_trip">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/trip_1.svg" alt="">
						</span>
						<p class="">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1-01_img_txt.svg" alt="社長が現地面接(地方)">
						</p>
					</li>
					<li>
						<figure class="img sp_mt-2_0">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1-02_img.png" alt="">
						</figure>
						<span class="label_trip">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/trip_2.svg" alt="">
						</span>
						<p class="">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1-02_img_txt.svg" alt="東京で最終面接">
						</p>
					</li>
					<li>
						<figure class="img">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1-03_img.png" alt="">
						</figure>
						<span class="label_trip">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/trip_3.svg" alt="">
						</span>
						<p class="sp_mt2_0">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1-03_img_txt.svg" alt="内定者はベトナムで 三ヶ月以上の研修">
						</p>
					</li>
				</ul>
				<div class="btn cf">
					<a href="#form" data-scroll>
						<span>
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s1-btn_1.svg" alt="NEOLABのLABTRIPに応募">
						</span>
					</a>
				</div>
			</div>
		</section><!-- /block_section s1 -->

		<section class="s2_block cf">
			<div class="c_title_block">
				<svg class="c_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" width="209.2px" height="179.2px" viewBox="0 0 209.2 179.2" style="enable-background:new 0 0 209.2 179.2;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#0071bd;}
					</style>
					<g>
						<path class="st0" d="M56.5,178.9l-0.3,0c-8.2,1.3-16-4.2-17.3-12.5l-17.5-52.3l30.1-4.9l17.5,52.3C70.3,169.8,64.7,177.6,56.5,178.9z"/>
						<ellipse transform="matrix(0.9869 -0.1612 0.1612 0.9869 -12.0735 20.058)" class="st0" cx="117.6" cy="84.4" rx="13.9" ry="13.9"/>
						<path class="st0" d="M110,27.3l18.2,111.3c0.7,4.4-3.6,7.9-7.7,6.3l-0.4-0.2c-26.2-9.9-55.6-18.5-83.2-13.9l-11.3,1.8c-9.4,1.5-18.3-4.9-19.9-14.3L0.2,85.5c-1.5-9.4,4.9-18.3,14.3-19.9l11.3-1.8c27.7-4.5,52.8-21.9,74.5-39.7l0.3-0.3C104.1,21,109.3,22.9,110,27.3z"/>
						<path class="st0 lTop" d="M151.7,30.7c-1.1,0.2-2.3-0.1-3.3-0.8c-2-1.4-2.4-4.2-1-6.2l15.7-21.9c1.4-2,4.2-2.4,6.2-1c2,1.4,2.4,4.2,1,6.2l-15.7,21.9C153.9,29.9,152.8,30.5,151.7,30.7z"/>
						<path class="st0 lMiddle" d="M205.5,72l-45.8,7.5c-2.4,0.4-4.7-1.2-5.1-3.7c-0.4-2.4,1.2-4.7,3.7-5.1l45.8-7.5c2.4-0.4,4.7,1.2,5.1,3.7C209.5,69.3,207.9,71.6,205.5,72z"/>
						<path class="st0 lBottom" d="M186.7,139.4c-1.1,0.2-2.3-0.1-3.3-0.8l-19.6-14.1c-2-1.4-2.4-4.2-1-6.2c1.4-2,4.2-2.4,6.2-1l19.6,14.1c2,1.4,2.4,4.2,1,6.2C188.9,138.6,187.8,139.2,186.7,139.4z"/>
					</g>
				</svg>
				<h1 class="title" style="margin-top: 0em;">
					<span class="item">
						<span class="ttl_before">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s2-title.svg" alt="NEOLABって何してる？">
						</span>
					</span>
				</h1>
			</div>

			<section class="business_blocks s3-all cf">
				<div class="inner cf">
					<div class="illust">
						<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block1_img_1.svg" alt="">
					</div>
					<div class="text_block">
						<h2>
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block1_title_1.svg" alt="">
						</h2>
						<p>
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block1_text_1.svg" alt="">
						</p>
					</div>
				</div>
			</section>

			<section class="business_blocks s3-vr cf">
				<div class="inner cf">
					<div class="illust">
						<div class="bg_vr1">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block2_img_2.svg" alt="">
						</div>
						<div class="bg_drone1">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block2_img_3.svg" alt="">
						</div>
					</div>
					<div class="text_block">
						<h2>
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block2_title_1.svg" alt="">
						</h2>
						<p>
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block2_text_1.svg?01" alt="。">
						</p>
					</div>
				</div>
			</section>

			<section class="business_blocks s3-global cf">
				<div class="inner cf">
					<div class="illust" id="">
					</div>
					<div class="text_block">
						<h2>
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block3_title_1.svg" alt="">
						</h2>
						<p>
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/v2-top-business_block3_text_1.svg?01" alt="">
						</p>
					</div>
				</div>
			</section>
		</section><!-- /block_section s2 -->

		<section class="s3_block cf" id="form">
			<div class="c_title_block">
				<svg class="c_icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" width="209.2px" height="179.2px" viewBox="0 0 209.2 179.2" style="enable-background:new 0 0 209.2 179.2;" xml:space="preserve">
					<style type="text/css">
						.st0{fill:#0071bd;}
					</style>
					<g>
						<path class="st0" d="M56.5,178.9l-0.3,0c-8.2,1.3-16-4.2-17.3-12.5l-17.5-52.3l30.1-4.9l17.5,52.3C70.3,169.8,64.7,177.6,56.5,178.9z"/>
						<ellipse transform="matrix(0.9869 -0.1612 0.1612 0.9869 -12.0735 20.058)" class="st0" cx="117.6" cy="84.4" rx="13.9" ry="13.9"/>
						<path class="st0" d="M110,27.3l18.2,111.3c0.7,4.4-3.6,7.9-7.7,6.3l-0.4-0.2c-26.2-9.9-55.6-18.5-83.2-13.9l-11.3,1.8c-9.4,1.5-18.3-4.9-19.9-14.3L0.2,85.5c-1.5-9.4,4.9-18.3,14.3-19.9l11.3-1.8c27.7-4.5,52.8-21.9,74.5-39.7l0.3-0.3C104.1,21,109.3,22.9,110,27.3z"/>
						<path class="st0 lTop" d="M151.7,30.7c-1.1,0.2-2.3-0.1-3.3-0.8c-2-1.4-2.4-4.2-1-6.2l15.7-21.9c1.4-2,4.2-2.4,6.2-1c2,1.4,2.4,4.2,1,6.2l-15.7,21.9C153.9,29.9,152.8,30.5,151.7,30.7z"/>
						<path class="st0 lMiddle" d="M205.5,72l-45.8,7.5c-2.4,0.4-4.7-1.2-5.1-3.7c-0.4-2.4,1.2-4.7,3.7-5.1l45.8-7.5c2.4-0.4,4.7,1.2,5.1,3.7C209.5,69.3,207.9,71.6,205.5,72z"/>
						<path class="st0 lBottom" d="M186.7,139.4c-1.1,0.2-2.3-0.1-3.3-0.8l-19.6-14.1c-2-1.4-2.4-4.2-1-6.2c1.4-2,4.2-2.4,6.2-1l19.6,14.1c2,1.4,2.4,4.2,1,6.2C188.9,138.6,187.8,139.2,186.7,139.4z"/>
					</g>
				</svg>
				<h1 class="title">
					<span class="item">
						<span class="ttl_before">
							<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/s4_title.svg" alt="エントリー">
						</span>
					</span>
				</h1>
				<div class="entry_pinkup cf">
					<h2>こんな方にエントリーして欲しい！</h2>
					<ul>
						<li>・人材(新卒/中途/派遣)、ヘルスケア(医療/介護)、保育、アドテク(web広告)領域で活躍したい方</li>
						<li>・Webサービスを通して、世の中に新しい価値を提供したい志向を有する方</li>
						<li>・将来、Webサービス/アプリのプロデューサー/ディレクター/プロジェクトマネジャーになりたい方</li>
						<li>・20代で事業責任者・関連企業役員として、圧倒的な成長をしたい方</li>
					</ul>
				</div>
				<div class="form_rule cf">
					<dl class="cf">
						<dt>エントリー締め切り</dt>
						<dd>※ 今後の選考スケジュールにつきましては、合格された方のみにご案内させていただきます。</dd>
						<dd>※ 面接はお住いの地域付近にて実施する予定の為、合否連絡と併せてメールにて連絡させていただきます。</dd>
					</dl>
					<dl class="cf">
						<dt>参加条件</dt>
						<dd>2018年3月・2019年3月に卒業予定の、全国の大学・大学院・短大・高専の学生</dd>
					</dl>
					<p class="form_btn cf">
						<a href="<?php echo home_url(); ?>/company/" target="_blank">
							<span class="">
								<svg xmlns="http://www.w3.org/2000/svg" width="147.594" height="11.56" viewBox="0 0 147.594 11.56">
									<path style="" id="" class="form_btn_color" d="M634.366,3878.24a0.594,0.594,0,0,0-.143-0.84,5.676,5.676,0,0,0-2.575-.72c-0.819-.05-0.9,1.03-0.208,1.07a5.56,5.56,0,0,1,2.133.68A0.548,0.548,0,0,0,634.366,3878.24Zm3.069,6.32a0.663,0.663,0,0,0,0-.91,6.952,6.952,0,0,0-2.366-1.35c-0.807-.29-1.2.72-0.547,0.94a5.963,5.963,0,0,1,2.055,1.35A0.567,0.567,0,0,0,637.435,3884.56Zm-3.888-2.55c0.39-.37.754-0.76,1.079-1.13,0.69-.78.456-1.75-0.819-1.71-0.975.02-2.978,0.04-3.862,0.1a0.614,0.614,0,1,0,.065,1.21c0.832-.04,2.445-0.13,3.134-0.14,0.3-.02.273,0.1,0.1,0.3a14.372,14.372,0,0,1-4.9,3.53,0.642,0.642,0,1,0,.5,1.17,14.667,14.667,0,0,0,3.576-2.29c0.013,1.07-.1,2.5-0.156,3.67a0.625,0.625,0,0,0,.689.68,0.68,0.68,0,0,0,.637-0.74c-0.039-1.21,0-2.81.026-3.77A3.038,3.038,0,0,0,633.547,3882.01Zm13.4-1.19c0.715,0,1.43.02,2.107,0.05a0.63,0.63,0,1,0,.039-1.24c-0.69-.01-1.418,0-2.159,0-0.013-.74-0.026-1.37-0.026-1.74a0.632,0.632,0,1,0-1.261.05c0.013,0.34.039,0.97,0.065,1.71-1.405.01-2.822,0.04-4.123,0.09a0.6,0.6,0,1,0,.039,1.19c1.093-.03,2.25-0.07,3.407-0.1a11.683,11.683,0,0,1-4.369,4.04c-0.819.43-.182,1.67,0.9,0.98a14.974,14.974,0,0,0,4.227-3.97c0.039,1.37.078,2.72,0.091,3.49,0.013,0.69-.677.61-1.21,0.44-0.819-.26-1.3,1.08-0.117,1.29,2.042,0.35,2.6-.47,2.575-1.47-0.013-.35-0.013-0.57-0.039-0.98C647.023,3883.68,646.971,3882.18,646.945,3880.82Zm13.328-2.47a0.638,0.638,0,0,0-.7-0.6c-1.313.01-3.134,0.01-4.382,0a0.613,0.613,0,1,0-.091,1.21c1.4,0.04,3.2,0,4.421,0A0.642,0.642,0,0,0,660.273,3878.35Zm0.832,3.82c0.533-1.27-.1-2-1.4-2.02-1.769-.01-3.954,0-5.631.04a0.623,0.623,0,1,0,.039,1.23c1.391-.06,3.81-0.09,5.136-0.07,0.5,0.01.56,0.22,0.429,0.57a6.719,6.719,0,0,1-4.8,3.92c-1.04.27-.559,1.56,0.507,1.23A8.258,8.258,0,0,0,661.105,3882.17Zm13.691-4.28a4.621,4.621,0,0,0-1.145-1.12c-0.559-.37-1.261.32-0.65,0.76a4.49,4.49,0,0,1,.9,1.04C674.21,3879.19,675.186,3878.57,674.8,3877.89Zm-1.626.7a4.387,4.387,0,0,0-1.1-1.14c-0.547-.39-1.288.25-0.69,0.72a4.683,4.683,0,0,1,.872,1.06C672.546,3879.87,673.534,3879.28,673.17,3878.59Zm0.7,6.62a0.689,0.689,0,0,0,.2-0.93,7.112,7.112,0,0,0-2.223-1.91c-0.742-.45-1.366.54-0.729,0.92a6.771,6.771,0,0,1,1.9,1.77A0.571,0.571,0,0,0,673.872,3885.21Zm-5.1-4.04c0.013,1.36.013,2.85,0,3.77-0.013.71-.585,0.64-1.118,0.47a0.652,0.652,0,1,0-.143,1.28c1.86,0.33,2.484-.45,2.484-1.48,0-.36,0-0.57-0.013-0.99-0.039-.89-0.039-2.01-0.026-3.06,0.975,0,1.938,0,2.9.05a0.621,0.621,0,1,0,.052-1.22c-0.962-.02-1.938-0.03-2.939-0.02,0.013-.67.013-1.27,0.026-1.67a0.626,0.626,0,1,0-1.249.05c0.013,0.36.026,0.95,0.026,1.64-1.053.01-2.119,0.05-3.147,0.09a0.6,0.6,0,1,0,.026,1.19C666.694,3881.23,667.734,3881.2,668.774,3881.17Zm-3.342,4.38a7.958,7.958,0,0,0,1.964-2.31,0.581,0.581,0,1,0-.988-0.61,6.419,6.419,0,0,1-1.73,1.84,0.639,0.639,0,0,0-.169.92A0.666,0.666,0,0,0,665.432,3885.55Zm17.164-6.02a3.438,3.438,0,0,1,1.613.52,2.808,2.808,0,0,1,.715,3.98,4.055,4.055,0,0,1-2.419,1.46c-1.118.26-.715,1.57,0.429,1.27a5.415,5.415,0,0,0,3.174-2.15,3.849,3.849,0,0,0-1.21-5.46,5.464,5.464,0,0,0-5.618.3c-1.742,1.21-2.614,3.63-1.794,5.24,0.572,1.12,1.664,1.63,2.666.7a10.2,10.2,0,0,0,2.626-5.26A0.654,0.654,0,0,0,682.6,3879.53Zm-1.027.07a0.875,0.875,0,0,0-.117.4,7.827,7.827,0,0,1-2.016,4.33c-0.325.28-.663,0.09-0.858-0.29-0.546-1,.1-2.73,1.313-3.66A4.276,4.276,0,0,1,681.569,3879.6Zm18.42,1.99a0.786,0.786,0,0,0,.767-0.77,0.613,0.613,0,0,0-.351-0.56,15.38,15.38,0,0,1-4.213-3.25,1.681,1.681,0,0,0-1.223-.63,1.467,1.467,0,0,0-1.131.63,13.992,13.992,0,0,1-4.109,3.65,0.677,0.677,0,0,0,.3,1.3,0.97,0.97,0,0,0,.546-0.18,20.118,20.118,0,0,0,4.174-3.9,0.467,0.467,0,0,1,.313-0.23,0.53,0.53,0,0,1,.338.23C696.842,3879.49,699.287,3881.59,699.989,3881.59Zm-3-.5a0.574,0.574,0,0,0,.611-0.58,0.582,0.582,0,0,0-.611-0.59h-3.732a0.582,0.582,0,0,0-.611.59,0.574,0.574,0,0,0,.611.58h3.732Zm2.172,2.42a0.576,0.576,0,0,0,.624-0.57,0.588,0.588,0,0,0-.624-0.6H690.9a0.592,0.592,0,0,0-.611.61,0.552,0.552,0,0,0,.611.56l2.458-.01a15.221,15.221,0,0,1-.923,2.61c-0.469.03-.937,0.04-1.392,0.05a0.546,0.546,0,0,0-.533.59,0.616,0.616,0,0,0,.6.65,53.177,53.177,0,0,0,7-.89,5.548,5.548,0,0,1,.429.67,0.6,0.6,0,0,0,.533.35,0.746,0.746,0,0,0,.728-0.71c0-.76-2.341-2.93-3.108-2.93a0.59,0.59,0,0,0-.611.53,0.521,0.521,0,0,0,.273.44,7.822,7.822,0,0,1,.949.76c-1.131.16-2.315,0.31-3.511,0.4a21.369,21.369,0,0,0,.949-2.54Zm14.244,3.6a0.579,0.579,0,0,0,.625-0.58,0.586,0.586,0,0,0-.625-0.59l-2.522.01v-4.53l1.885,0.01a0.585,0.585,0,0,0,.624-0.59,0.569,0.569,0,0,0-.611-0.58h-0.013l-1.885.02,0.013-3.26a0.66,0.66,0,0,0-1.314,0v3.25l-1.69-.01h-0.013a0.554,0.554,0,0,0-.586.58,0.571,0.571,0,0,0,.6.59l1.69-.01,0.013,4.53-2.575-.01a0.585,0.585,0,1,0,0,1.17H713.4Zm-5.787-3.39a0.7,0.7,0,0,0,.638-0.69c0-.64-1.392-1.35-1.639-1.35a0.634,0.634,0,0,0-.611.58,0.381,0.381,0,0,0,.208.33,3.149,3.149,0,0,1,1,.89A0.486,0.486,0,0,0,707.614,3883.72Zm-1.781,3.42s-0.052-3.33-.065-5.35a6.889,6.889,0,0,0,1.612-2.81,0.944,0.944,0,0,0-1.118-.87h-0.429v-1.28a0.646,0.646,0,0,0-1.288,0l0.013,1.3c-0.4.01-.806,0.01-1.209,0.02a0.555,0.555,0,0,0-.585.56,0.567,0.567,0,0,0,.6.56h0.013c0.806-.01,1.365-0.06,2.107-0.06h0.234c0.091,0,.286.01,0.286,0.14a7.4,7.4,0,0,1-3.264,3.43,0.623,0.623,0,0,0-.377.53,0.634,0.634,0,0,0,.663.6,3.842,3.842,0,0,0,1.6-.93l-0.065,4.16v0.01a0.582,0.582,0,0,0,.651.53,0.558,0.558,0,0,0,.624-0.53v-0.01Zm20.72-5.15a0.478,0.478,0,0,0,.482-0.51,0.473,0.473,0,0,0-.482-0.51l-0.988.02a18.5,18.5,0,0,0,.3-3.05l0.507,0.02a0.516,0.516,0,0,0,0-1.03h-3.485a0.516,0.516,0,0,0,0,1.03h0.208v3.01h-0.312a0.5,0.5,0,0,0-.4.21,0.971,0.971,0,0,0,.026-0.21c0.013-.35.013-0.78,0.013-1.21,0-.71-0.013-1.45-0.026-1.93-0.026-.9-0.728-0.95-1.586-0.95-0.989,0-1.639.09-1.639,1.05v6.8c-0.208.09-.4,0.17-0.572,0.24a0.457,0.457,0,0,0-.286.45,0.573,0.573,0,0,0,.481.61,16.616,16.616,0,0,0,3.1-1.56l0.052,0.08a0.55,0.55,0,0,0,.455.3,0.529,0.529,0,0,0,.494-0.55,3.769,3.769,0,0,0-1.2-1.62,0.509,0.509,0,0,0-.338-0.13,0.468,0.468,0,0,0-.481.43,1.221,1.221,0,0,0,.468.72c-0.3.15-.7,0.36-1.132,0.55,0-.82.013-1.81,0.013-2.16,0.117,0,.247.02,0.39,0.02,0.976,0,1.457-.15,1.678-0.64v0.02a0.48,0.48,0,0,0,.494.5l1.535-.01a6.424,6.424,0,0,1-3.85,4.61,0.463,0.463,0,0,0-.3.46,0.6,0.6,0,0,0,.573.61,6.387,6.387,0,0,0,3.693-3.29c-0.013.57-.026,1.25-0.026,1.54,0,1.21.377,1.5,1.261,1.5,0.715,0,1.574-.05,1.574-2.2v-0.09a0.525,0.525,0,0,0-.546-0.53,0.466,0.466,0,0,0-.469.53c0,1.18-.143,1.3-0.494,1.3-0.182,0-.338-0.04-0.338-0.51,0-.5.052-1.96,0.052-2.47a0.475,0.475,0,0,0-.4-0.48c0.117-.32.221-0.66,0.312-0.98Zm-8.543,5.21s-0.052-3.33-.065-5.41a5.427,5.427,0,0,1,.39.56,0.389,0.389,0,0,0,.338.22,0.555,0.555,0,0,0,.494-0.56,1.921,1.921,0,0,0-1.157-1.15h-0.065c0-.25.013-0.52,0.013-0.8l0.585,0.03h0.026a0.556,0.556,0,0,0-.026-1.11h-0.572c0.013-1.17.039-2.19,0.039-2.19v-0.02a0.573,0.573,0,0,0-.6-0.59,0.554,0.554,0,0,0-.572.59v0.02l0.026,2.19h-0.78a0.552,0.552,0,0,0-.572.56,0.524,0.524,0,0,0,.546.55h0.026l0.806-.03v0.28a11.82,11.82,0,0,1-1.638,3.16,0.605,0.605,0,0,0-.143.39,0.622,0.622,0,0,0,.611.59c0.364,0,.624-0.35,1.157-1.58l-0.039,4.3v0.01a0.542,0.542,0,0,0,.585.55,0.549,0.549,0,0,0,.586-0.55v-0.01Zm6.06-6.21v-3.05h0.7v0.26a15.334,15.334,0,0,1-.234,2.79H724.07Zm-3.9-2.01v-0.79c0-.31.208-0.36,0.625-0.36,0.624,0,.624.09,0.624,1.15h-1.249Zm1.249,0.98c0,1.11,0,1.18-.728,1.18-0.2,0-.4,0-0.521-0.01,0-.36-0.013-0.77-0.013-1.17h1.262Zm18.381,3.84a0.526,0.526,0,0,0,.533-0.56,0.516,0.516,0,0,0-.533-0.56h-5.644a5.621,5.621,0,0,0,.26-0.83,0.154,0.154,0,0,0-.013-0.08c1.249,0,2.5-.02,3.485-0.03a1.12,1.12,0,0,0,1.327-1.05c0.013-.17.013-0.46,0.013-0.73,0-.78-0.052-1.47-1.353-1.5-0.416,0-.9-0.01-1.391-0.01v-0.77h3.1a0.518,0.518,0,0,0,.533-0.54,0.526,0.526,0,0,0-.533-0.55h-9.74a0.548,0.548,0,0,0-.547.57,0.5,0.5,0,0,0,.547.52h3.108v0.77c-0.43,0-.846.01-1.223,0.01-1.365.02-1.378,0.77-1.378,1.73,0,1.55.221,1.56,2.835,1.56a0.211,0.211,0,0,0-.026.07c-0.091.3-.182,0.58-0.273,0.86h-3.265a0.559,0.559,0,0,0-.559.58,0.52,0.52,0,0,0,.559.54h2.822a11.238,11.238,0,0,1-.715,1.3,0.59,0.59,0,0,0-.117.32,0.548,0.548,0,0,0,.611.48,0.807,0.807,0,0,0,.651-0.36,12.51,12.51,0,0,1,1.638.21,7.265,7.265,0,0,1-3.81.76c-0.26,0-.546,0-0.832-0.01h-0.026a0.557,0.557,0,0,0-.586.57,0.582,0.582,0,0,0,.6.59c0.195,0,.377.01,0.572,0.01,2.6,0,4.356-.52,5.449-1.57a8.585,8.585,0,0,1,2.783,1.37,0.615,0.615,0,0,0,.4.15,0.7,0.7,0,0,0,.65-0.69,0.61,0.61,0,0,0-.286-0.53,8.837,8.837,0,0,0-2.822-1.24,4.9,4.9,0,0,0,.52-1.36H739.8Zm-3.316-3.06v-1.24h1c0.494,0,.559.12,0.559,0.62a1.88,1.88,0,0,1-.013.28,0.444,0.444,0,0,1-.52.34h-1.027Zm-1.067-2.3h-1.391v-0.76h1.391v0.76Zm0,2.3h-1.391v-1.25h1.391v1.25Zm-2.47,0H732.1a0.493,0.493,0,0,1-.572-0.61c0-.39.039-0.63,0.546-0.63h0.872v1.24Zm2.887,3.06a3.8,3.8,0,0,1-.443,1.09,18.5,18.5,0,0,0-2.015-.26c0.13-.28.247-0.55,0.364-0.83h2.094Zm11.442-5.82a2.4,2.4,0,0,0,.1-0.75,0.664,0.664,0,0,0-1.3.06,5.487,5.487,0,0,1-.143,1.1c-0.065.01-.143,0.01-0.195,0.02a1.983,1.983,0,0,1-1.222-.02c-0.911-.61-1.548.61-0.611,1.08a2.72,2.72,0,0,0,1.612.14,11.826,11.826,0,0,1-.611,1.16,6.442,6.442,0,0,1-1.054,1.22c-0.78.7,0.17,1.44,0.885,0.8a2.356,2.356,0,0,0,.3-0.33c0.013-.02.039-0.04,0.052-0.06,0.845-.97,1.847-1.1,2.2-0.01-0.247.14-.456,0.29-0.638,0.4-2.353,1.55-2.665,3.31-.91,4.29a5.7,5.7,0,0,0,3.55.19c1.2-.32.833-1.72-0.312-1.37a4.108,4.108,0,0,1-2.575.11c-0.728-.37-0.923-1.15.69-2.19,0.1-.06.221-0.13,0.338-0.19,0,0.27-.013.57-0.026,0.84a0.591,0.591,0,0,0,.585.74,0.76,0.76,0,0,0,.689-0.8,10.384,10.384,0,0,0-.026-1.43c0.52-.25,1.1-0.51,1.652-0.72a14.852,14.852,0,0,0,1.4-.53,0.693,0.693,0,1,0-.728-1.17,11.163,11.163,0,0,1-1.014.48c-0.508.22-1.054,0.49-1.587,0.77a1.9,1.9,0,0,0-2.185-1.14,11.968,11.968,0,0,0,.624-1.31,7.228,7.228,0,0,0,1.431-.43c0.962-.48.26-1.56-0.455-1.13a4.513,4.513,0,0,1-.546.24Zm12.247,5.97c-0.013.7-.013,1.68-0.013,2.03,0,1.44,1.131,1.57,2.444,1.57,1.483,0,2.991-.17,2.991-2.4a0.648,0.648,0,0,0-.689-0.7,0.576,0.576,0,0,0-.611.61c0,1.17-.624,1.32-1.625,1.32-1.119,0-1.275-.17-1.275-0.88,0-.42.013-1.03,0.026-1.55,0.221,0,.442-0.02.663-0.02a1.4,1.4,0,0,0,1.418-1.33c0.026-.64.039-1.6,0.039-2.55,0-.86-0.013-1.69-0.039-2.26a1.344,1.344,0,0,0-1.353-1.18c-0.663-.03-1.482-0.03-2.275-0.03s-1.587,0-2.185.03a1.309,1.309,0,0,0-1.431,1.27c-0.026.51-.039,1.4-0.039,2.33,0,0.91.013,1.84,0.039,2.42a1.387,1.387,0,0,0,1.522,1.3C757.882,3883.94,758.714,3883.95,759.521,3883.95Zm-0.963,1.09a0.639,0.639,0,0,0,.065-0.26,0.687,0.687,0,0,0-.689-0.61,0.631,0.631,0,0,0-.585.39,3.847,3.847,0,0,1-3.29,1.99,0.593,0.593,0,0,0-.585.59,0.63,0.63,0,0,0,.663.61S757.4,3887.49,758.558,3885.04Zm-1.794-6.29c0-.19.013-0.36,0.013-0.51a0.592,0.592,0,0,1,.611-0.57c0.546-.01,1.3-0.02,2.029-0.02,0.611,0,1.183.01,1.612,0.02a0.543,0.543,0,0,1,.611.56c0,0.14.026,0.33,0.026,0.52h-4.9Zm4.915,2.02h-4.928v-1.02h4.928v1.02Zm-0.013,1c0,0.58,0,1.06-.624,1.09-0.559.03-1.144,0.04-1.729,0.04-0.664,0-1.314-.01-1.912-0.04a0.542,0.542,0,0,1-.624-0.57c0-.16-0.013-0.33-0.013-0.52h4.9Zm10.986,4.09a1.84,1.84,0,0,0-2.055-1.75c-1.534.05-2.145,1.66-.767,2.56a4.273,4.273,0,0,0,4.3-.33c1.366-.98,1.977-3.27.105-4.39a4,4,0,0,0-3.655.04c0.468-.46.989-0.92,1.509-1.41s1.04-.98,1.456-1.45c0.651-.73-0.208-1.65-1.248-1.35a19.723,19.723,0,0,1-2.861.63c-0.884.11-.689,1.39,0.338,1.17a10.917,10.917,0,0,0,2.185-.66c0.1-.05.234-0.1,0.091,0.08a46.613,46.613,0,0,1-3.928,4.2c-0.806.79,0.5,1.56,0.963,0.87a2.894,2.894,0,0,1,1.092-.98,3.453,3.453,0,0,1,3.394-.22,1.636,1.636,0,0,1-.169,2.58A1.8,1.8,0,0,1,772.652,3885.86Zm-1.105.2a2.333,2.333,0,0,1-1.093-.2,0.412,0.412,0,0,1,.182-0.8A0.858,0.858,0,0,1,771.547,3886.06Z" transform="translate(-627.844 -3876.19)"/>
								</svg>
							</span>
						</a>
					</p>
				</div>
			</div>

			<div class="form_block cf">

				<!-- ============================================ // START form -->

				<!-- / form js start -->
				<script type="text/javascript" src="https://app-webparts-hrbc.porterscloud.com/po-b.js" data-template-id="12065"></script>
				<script type="porters/webparts"></script>
				<!-- / form js end -->

				<!-- ============================================ // END form -->

			</div>
		</section><!-- /block_section s1 -->
	</main><!-- /#main -->

	<div class="share cf">
		<?php
			$url_encode=urlencode(get_permalink());
			$title_encode=urlencode(get_the_title()).'｜'.get_bloginfo('name');
		?>
		<ul class="cf">
			<li class="facebook">
				<a href="http://www.facebook.com/sharer.php?src=bm&u=<?php echo $url_encode;?>&t=<?php echo $title_encode;?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					<span class="icon-facebook">facebook</span>
					<?php if(function_exists('scc_get_share_facebook')) echo (scc_get_share_facebook()==0)?'':scc_get_share_facebook(); ?>
				</a>
			</li>
			<li class="tweet">
				<a href="http://twitter.com/intent/tweet?url=<?php echo $url_encode ?>&text=<?php echo $title_encode ?>&tw_p=tweetbutton" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
					<span class="icon-twitter">tweet</span>
					<?php if(function_exists('scc_get_share_twitter')) echo (scc_get_share_twitter()==0)?'':scc_get_share_twitter(); ?>
				</a>
			</li>
			<li class="googleplus">
				<a href="https://plus.google.com/share?url=<?php echo $url_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;">
					<span class="icon-google-plus">Google+</span>
					<?php if(function_exists('scc_get_share_gplus')) echo (scc_get_share_gplus()==0)?'':scc_get_share_gplus(); ?>
				</a>
			</li>
			<li class="hatena">
				<a href="http://b.hatena.ne.jp/add?mode=confirm&url=<?php echo $url_encode ?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=510');return false;">
					<span class="icon-hatebu">はてブ</span>
					<?php if(function_exists('scc_get_share_hatebu')) echo (scc_get_share_hatebu()==0)?'':scc_get_share_hatebu(); ?>
				</a>
			</li>
			<?php if(is_mobile()) : ?>
			<li class="line">
				<a href="http://line.me/R/msg/text/?<?php echo $title_encode . '%0A' . $url_encode;?>">
					<span class="icon-line">LINE</span>
				</a>
			</li>
			<?php endif; ?>
			<li class="pocket">
				<a href="http://getpocket.com/edit?url=<?php echo $url_encode;?>&title=<?php echo $title_encode;?>"><span class="icon-pocket">Pocket</span>
					<?php if(function_exists('scc_get_share_pocket')) echo (scc_get_share_pocket()==0)?'':scc_get_share_pocket(); ?>
				</a>
			</li>
		</ul>
	</div>
	<footer class="g_footer cf" role="contentinfo">
		<div class="inner_first cf">
			<p class="items poricy">
				<a href="http://www.neo-career.co.jp/policy" target="_blank" class="op">
					<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/footer_poricy.png" alt="プライバシーポリシー">
				</a>
			</p>
			<?php /*
			<p class="items poricy_text">
				<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/footer_poricy_text.png" alt="状況に応じて、Skype面談となる場合があります。交通費の半額を支給します。渡航先は弊社ベトナムオフィスを予定しています。">
			</p>
			*/ ?>
		</div>
		<div class="inner_second cf">
			<p class="items logo">
				<a href="<?php echo home_url(); ?>">
					<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/footer_logo.svg" alt="ネオラボ">
				</a>
			</p>
			<p class="items scroll">
				<a href="<?php echo home_url(); ?>" data-scroll>
					<img class="" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/footer_scrolltop.png" alt="ネオラボトップに戻る">
				</a>
			</p>
		</div>
	</footer>

</div><!--/ #content -->


<div class="remodal ceo_modal2" data-remodal-id="modal" role="dialog" data-remodal-options="hashTracking:false">
	<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
	<article class="profile cf">
		<div class="head">
			<img class="thumbnail" src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/img/ceo_thumbnail_2.png" alt="">
			<div class="head_block">
				<h1>株式会社ネオラボ</h1>
				<p>社長 / 大川 智弘 / 27歳 / 社会人4年目</p>
			</div>
		</div>
		<div class="data_block cf">
			<h2 class="title">PROFILE</h2>
			<div class="data_inner">
				<dl>
					<dd>
						1989年 北海道生まれ。<br>
						中学～高校在学中、ECサイト・ネットオークションを中心に古物商を営み、ビジネス感覚を養う。大学在学中、個人事業主として小規模法人向けの経営コンサルティングを行う。就職活動は、リクルートや博報堂、GREE、人工衛星ベンチャーなどを中心にインターンシップに参加。数社の内定を頂くが辞退し、大学在学中にクラウドソーシングを提供するベンチャー企業に就職する。ネオキャリアへの転職後は、関係会社での新規事業開発、経営企画、組織再生・成長をテーマとして従事。ネオラボの事業立上げに携わり、現在に至る。
					</dd>
				</dl>
			</div>
		</div>
		<div class="data_block cf">
			<h2 class="title">HISTORY</h2>
			<div class="data_inner p_scrollbar">
				<dl>
					<dt>2012年5月 ランサーズ株式会社 入社</dt>
					<dd>
						学業の傍ら、クラウドソーシング・プラットフォームを展開するランサーズに入社。Webサービス開発ディレクター、カスタマーサポートSV、新規事業開発チームリーダーを歴任。
					</dd>
				</dl>
				<dl>
					<dt>2013年3月 大学卒業</dt>
				</dl>
				<dl>
					<dt>2014年6月 株式会社ネオキャリア 入社</dt>
					<dd>
						総合人材サービスを展開するネオキャリアに入社。経営企画部配属。Webサービスのディレクターとして、プロジェクトマネジメント・サービスリリースを行う。
					</dd>
				</dl>
				<dl>
					<dt>2014年11月 マーベリック株式会社 出向</dt>
					<dd>
						DSPを中心としたインターネット広告を展開するマーベリックに出向。経営企画・事業戦略担当として、組織成長・再生・新規事業開発に従事。ぐるなび・読売新聞グループとのチラシ広告分野における業務提携・アライアンスを実現。
					</dd>
				</dl>
				<dl>
					<dt>2015年8月 株式会社ネオキャリア 経営企画部 帰任</dt>
					<dd>
						ベトナムIT人材を中心とした、ラボ型オフショア開発を展開するネオラボに参画。スタートアップメンバーとして、事業企画・法人営業・管理機能を含むオペレーション構築に従事。
					</dd>
				</dl>
				<dl>
					<dt>2015年11月 株式会社ネオラボ 出向</dt>
				</dl>
				<dl>
					<dt>2016年4月 株式会社ネオラボ 執行役員COO 就任</dt>
				</dl>
				<dl>
					<dt>2016年10月 株式会社ネオラボ 取締役社長COO 就任</dt>
				</dl>
			</div>
		</div>
		<div class="data_block cf">
			<h2 class="title">MESSAGE</h2>
			<div class="data_inner">
				<dl>
					<dd>
						日々、『あなたはどんな人？』『何をしたい？どうなりたい？』といった質問を通じて、自分の過去を振り返るだけでなく、将来像を具体的に考えていることだと思います。<br>
						就職活動という機会を通じてみなさん自身が選択する道には、正解も不正解もないと思います。限られた時間の中でひたすら自分自身と向き合い、自分の意思や志といった内なる声をたよりに、自分なりに納得のゆく選択ができるか否かが、本質的に問われているのかもしれません。<br>
						さて、LAB TRIPというこのキャンペーンは、みなさんが「納得のゆく選択とは何か？」を問う思考の旅をされる中で、みなさんの元に私が伺って直接面接することをコンセプトとして企画しました。<br>
						３つの旅を通じて、少しでもみなさんの選択の一助になれれば幸いです。ぜひご応募ください。
					</dd>
				</dl>
			</div>
		</div>

		<a href="#form" class="wantedly_btn exit-remodal op">
			<span>LAB TRIPに応募</span>
		</a>
	</article>
</div>

<script>
$( function() {
	var inst = $( '[data-remodal-id=modal]' ).remodal();
	$( document ).on( 'click', '.open-remodal', function() {
		inst.open();
		return false;
	} );
	$( document ).on( 'click', '.exit-remodal', function() {
		inst.close();
	} );
} );
</script>


<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/js/style.min-dist.js"></script>
<script>
;(function() {
	// Fake loading.
	setTimeout(init, 1000);

	function init() {
			document.body.classList.remove('loading');

			var rev1 = new RevealFx(document.querySelector('#rev-1'), {
					revealSettings : {
							bgcolor: '#7f40f1',
							onCover: function(contentEl, revealerEl) {
									contentEl.style.opacity = 1;
							}
					}
			});
			rev1.reveal();

			var rev2 = new RevealFx(document.querySelector('#rev-2'), {
					revealSettings : {
							bgcolor: '#fcf652',
							delay: 250,
							onCover: function(contentEl, revealerEl) {
									contentEl.style.opacity = 1;
							}
					}
			});
			rev2.reveal();

			// var rev3 = new RevealFx(document.querySelector('#rev-3'), {
			// 		revealSettings : {
			// 				bgcolor: '#fcf652',
			// 				//direction: 'rl',
			// 				delay: 450,
			// 				onCover: function(contentEl, revealerEl) {
			// 						contentEl.style.opacity = 1;
			// 				}
			// 		}
			// });
			// rev3.reveal();

	}
})();
</script>

<script>
  // scroll
  $(window).load(function(){
    smoothScroll.init({
      selector: '[data-scroll]',                  // スムーススクロールが有効なリンクに付ける属性
      selectorHeader: '[data-scroll-header]',     // 固定ナビに付ける属性
      speed: 800,                                 // 到達するまでの総時間(ミリ秒)
      easing: 'easeOutQuart',                     // スピードの種類
      offset: 0,                                  // 到達場所からズラすピクセル数
      updateURL: true,                            // URLを[#〜]に変更するか？
      callback: function () {}                    // コールバック関数 (到達時に実行される関数)
    });
  });
</script>

<!-- // start scrollbar -->
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/js/scrollbar/perfect-scrollbar.jquery.min.js"></script>
<script>
;(function($) {
	$(function() {
		$('.p_scrollbar').perfectScrollbar();
	});
})(jQuery);
</script>
<!-- // end scrollbar -->

<!-- // start skrollr -->
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/js/skrollr/skrollr.min.js"></script>
<script>
$(window).on('load', function() {
	if ($(window).width() > 768) {
		$(function() {
			// window が768 以下は実行しない
			var s = skrollr.init({
				edgeStrategy: 'set',
				easing: {
					WTF: Math.random,
					inverted: function(p) {
					  return 1-p;
					}
				}
			});
		});
	}
});
</script>
<!-- // end skrollr -->

<?php wp_footer(); ?>
</body>
</html>