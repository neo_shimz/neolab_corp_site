(function() {
		// Fake loading.
		setTimeout(init, 1000);

		function init() {
				document.body.classList.remove('loading');

				//************************ Example 1 - reveal on load ********************************

				var rev1 = new RevealFx(document.querySelector('#rev-1'), {
						revealSettings : {
								bgcolor: '#7f40f1',
								onCover: function(contentEl, revealerEl) {
										contentEl.style.opacity = 1;
								}
						}
				});
				rev1.reveal();

				var rev2 = new RevealFx(document.querySelector('#rev-2'), {
						revealSettings : {
								bgcolor: '#fcf652',
								delay: 250,
								onCover: function(contentEl, revealerEl) {
										contentEl.style.opacity = 1;
								}
						}
				});
				rev2.reveal();

				var rev3 = new RevealFx(document.querySelector('#rev-3'), {
						revealSettings : {
								bgcolor: '#fcf652',
								//direction: 'rl',
								delay: 450,
								onCover: function(contentEl, revealerEl) {
										contentEl.style.opacity = 1;
								}
						}
				});
				rev3.reveal();

		}
})();

;(function($) {


		// ------------------------------------
		// eye
		// ------------------------------------
		$(function() {
				// If you use this code, please link to this pen (cdpn.io/rkcjt). Thanks :)
				var DrawEye = function(eyecontainer, pupil, eyeposx, eyeposy){
						// Initialise core variables
						var r = $(pupil).width()/2;
						var center = {
								x: $(eyecontainer).width()/2 - r,
								y: $(eyecontainer).height()/2 - r
						};
						var distanceThreshold = $(eyecontainer).width()/2 - r;
						var mouseX = 0, mouseY = 0;

						// Listen for mouse movement
						$(window).mousemove(function(e){
								var d = {
										x: e.pageX - r - eyeposx - center.x,
										y: e.pageY - r - eyeposy - center.y
								};
								var distance = Math.sqrt(d.x*d.x + d.y*d.y);
								if (distance < distanceThreshold) {
										mouseX = e.pageX - eyeposx - r;
										mouseY = e.pageY - eyeposy - r;
								} else {
										mouseX = d.x / distance * distanceThreshold + center.x;
										mouseY = d.y / distance * distanceThreshold + center.y;
								}
						});

						// Update pupil location
						var pupil = $(pupil);
						var xp = 0, yp = 0;
						var loop = setInterval(function(){
								// change 1 to alter damping/momentum - higher is slower
								xp += (mouseX - xp) / 1;
								yp += (mouseY - yp) / 1;
								pupil.css({left:xp, top:yp});
						}, 1);
				};

				var chihuahuaeye1 = new DrawEye("#eye_block", "#eye", 175, 261);
				//var chihuahuaeye2 = new DrawEye("#dogeyeright", "#dogpupilright", 210, 259);
		});


		$(function() {
				var wh  = $('.full_window');
				var h   = $(window).height();
				var w   = $(window).width();
						wh.css('height', h + 'px');
						wh.css('width', w + 'px');
				$(window).resize(function() {
						var h = $(window).height();
						var w = $(window).width();
						wh.css('height', h + 'px');
						wh.css('width', w + 'px');
				});
		});


		/* ---------------------------------------
				smoothScroll
		---------------------------------------*/
		$(window).load(function(){
				smoothScroll.init({
						selector: '[data-scroll]',                  // スムーススクロールが有効なリンクに付ける属性
						selectorHeader: '[data-scroll-header]',   // 固定ナビに付ける属性
						speed: 1000,                                 // 到達するまでの総時間(ミリ秒)
						easing: 'easeOutQuart',                   // スピードの種類
						offset: 0,                                  // 到達場所からズラすピクセル数
						updateURL: true,                            // URLを[#〜]に変更するか？
						callback: function () {}                    // コールバック関数 (到達時に実行される関数)
				});
		});



		/* ---------------------------------------
				TweenMax
		---------------------------------------*/
		$(function() {

				// var controller = new ScrollMagic();

				// var tween = TweenMax.fromTo(".anime_title", 0, {opacity: 0, width: 0} , { opacity: 1, width: 100+'%'});

				// var sceneTitle = new ScrollScene({triggerElement: ".anime_title"})
				//  .setTween(tween)
				//  .addTo(controller);


				// var controller = new ScrollMagic();

				//  // 動かしたい要素のアニメーションを作る
				//  var tween = TweenMax.fromTo(".anime_title", 0.5, {opacity:0 , left: -100+'%'} , {opacity: 1, left: 0});

				//  // トリガーになる位置を指定してアニメーションを設定する
				//  var scene = new ScrollScene({triggerElement: ".anime_title"})
				//                  .setTween(tween)
				//                  .addTo(controller);

		});

})(jQuery);