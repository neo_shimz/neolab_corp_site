
;(function($) {


  // skrollr
  $(window).load(function(){
    var s = skrollr.init({
      edgeStrategy: 'set',
      easing: {
        WTF: Math.random,
        inverted: function(p) {
          return 1-p;
        }
      }
    });
  });



  // acoodion
  $(function() {
    var acContents = $('.ac-contents');
    var acHead = $('.ac-head');
    acHead.on('click', function () {
      $(this).toggleClass('open');
      $(this).next(acContents).slideToggle();
    });
  });



  // scroll
  $(window).load(function(){
    smoothScroll.init({
      selector: '[data-scroll]',                  // スムーススクロールが有効なリンクに付ける属性
      selectorHeader: '[data-scroll-header]',     // 固定ナビに付ける属性
      speed: 800,                                 // 到達するまでの総時間(ミリ秒)
      easing: 'easeOutQuart',                     // スピードの種類
      offset: 0,                                  // 到達場所からズラすピクセル数
      updateURL: true,                            // URLを[#〜]に変更するか？
      callback: function () {}                    // コールバック関数 (到達時に実行される関数)
    });
  });



  // load
  $(window).on('load', function () {
    $('body').addClass('loaded');
  });



  // header height & clone
  $(document).ready(function() {
    var headerHeight = $('.g-header').height();
    // var $cloneHeader = $('.g-header').clone();
    //     $cloneHeader.appendTo('#clone-g-header');
    $('.g-header').height(headerHeight);
  });



  // stickky
  $(function() {
    $('body').each(function() {
      var $window = $(window),
          $header = $('body'),
          headerOffsetTop = $header.offset().top;

      $window.on('scroll', function() {
        if ($window.scrollTop() > headerOffsetTop) {
          $header.addClass('sticky');
        } else {
          $header.removeClass('sticky');
        }
      });
      $window.trigger('scroll');
    });
  });



  // scrollbar
  $(function() {
    $('#scrollbar1').perfectScrollbar();
  });



  // wow
  $(function(){
    new WOW().init();
  });


})(jQuery);

