
<?php if(is_mobile()) : ?>

<style type="text/css">
.bnr_2018_sp_wrap {
    position: relative;
    margin: 50px 0 0 0;
}
.bnr_2018_sp_link {
    position: absolute;
    top: -50px;
    width: 100%;
    background: #feeb08;
    display: block;
    height: 50px;
    border: 2px solid #0071bd;
    margin: 0 0 0 0;
}
.bnr_2018_sp_link img {
	position: relative;
	top: 14px;
}
</style>
<div class="bnr_2018_sp_wrap">
	<a class="bnr_2018_sp_link" href="<?php echo home_url(); ?>/recruit_2018/" target="_blank">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/banner/sp_recruit.svg">
	</a>
</div>
<?php else : ?>
<style type="text/css">
.bnr_2018 {
	position: fixed;
	top: 0;
	right: 0;
	z-index: 5;
}
.bnr_2018 .bnr_2018_link {
	display: block;
}
.bnr_2018 .bnr_2018_img_wrap {
	position: relative;
	width: 60px;
	height: 276px;
	overflow: hidden;
	background: #feeb08 url("<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/banner/step1.svg") no-repeat;
}
.bnr_2018 .bnr_2018_img_wrap span {
	display: block;
	position: absolute;
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
  opacity: 0;
  z-index: 0;
	animation: imageAnimation 12s linear 0s;
}
.bnr_2018 .bnr_2018_img_wrap span.step1 {
	background: #feeb08 url("<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/banner/step1.svg") no-repeat;
}
.bnr_2018 .bnr_2018_img_wrap span.step2 {
	background: #feeb08 url("<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/banner/step2.svg") no-repeat;
	animation-delay: 3s;
}
.bnr_2018 .bnr_2018_img_wrap span.step3 {
	background: #feeb08 url("<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/banner/step3.svg") no-repeat;
	animation-delay: 6s;
}
.bnr_2018 .bnr_2018_img_wrap span.step4 {
	background: #feeb08 url("<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/recruit/recruit_2018/img/banner/step1.svg") no-repeat;
	animation-delay: 9s;
}
@keyframes imageAnimation {
  0% { opacity: 1; animation-timing-function: ease-in; }
  8% { opacity: 1; animation-timing-function: ease-out; }
  17% { opacity: 1; }
  70% { opacity: 1; }
  80% { opacity: 1; }
  100% { opacity: 1; }
}
</style>
<div class="bnr_2018 cf">
	<a href="<?php echo home_url(); ?>/recruit_2018/" target="_blank" class="bnr_2018_link op">
		<div class="bnr_2018_img_wrap cf">
			<span class="step1">
			</span>
			<span class="step2">
			</span>
			<span class="step3">
			</span>
			<span class="step4">
			</span>
		</div>
	</a>
</div>
<?php endif; ?>
