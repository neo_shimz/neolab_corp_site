<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="initial-scale=1.0, width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php
global $page, $paged;
wp_title( '|', true, 'right' );
bloginfo( 'name' );
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
	echo " | $site_description";
if ( $paged >= 2 || $page >= 2 )
	echo ' | ' . sprintf( __( 'Page %s', 'neo-lab' ), max( $paged, $page ) );
?></title>
<link rel="icon" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/favicon.jpg">
<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/icon-apple.png">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/css/style.css">
<link href='//fonts.googleapis.com/css?family=Roboto:100italic' rel='stylesheet' type='text/css'>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/jquery.min.js"></script>
<?php wp_head(); ?>
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<?php include_once "inc/tag.php"; ?>
<body class="drawer drawer--right <?php echo esc_attr( $post->post_name ); ?>">

<?php if ( is_home() || is_front_page() ) : ?>
	<div id="loading_wrap" class="cf">
		<div id="loading" class="wf2 cf">
			<div class="loding_logo" class="cf">
				<svg id="" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
					<path data-name="NEOLAB LOGO" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)" alt="NEOLAB"/>
				</svg>
			</div>
			<div class="cf">
				<span class="loding_txt">LOADING</span>
				<span id="load">0%</span>
				<div id="bar"><span></span></div>
			</div>
		</div>
	</div>
	<script>
	$(function() {
		Array.prototype.remove = function(element) {
		  for (var i = 0; i < this.length; i++)
			if (this[i] == element) this.splice(i,1);
		};
		function preload(images, progress) {
			var total = images.length;
			$(images).each(function(){
				var src = this;
				$('<img/>')
					.attr('src', src)
					.load(function() {
						images.remove(src);
						progress(total, total - images.length);
					});
			});
		}
		var now_percent = 0;
		var displaying_percent= 0;
		preload([
			'<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/top/visual_01.jpg',
			'<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/top/top_bg_01.jpg',
			'<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/top/top_bg_02.jpg',
			'<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/top/top_bg_03.jpg',
			'<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/top/top_bg_04.jpg'
		], function(total, loaded){
			now_percent = Math.ceil(100 * loaded / total);
		});

		var timer = window.setInterval(function() {
			if (displaying_percent >= 100) {
				window.clearInterval(timer);
				$('#loading_wrap').fadeOut('slow', function() {
					$('#content').fadeIn('slow');
				});
			} else {
				if (displaying_percent < now_percent) {
					displaying_percent++;
					$('#load').html(displaying_percent + '%');
					$('#bar span').css('width', displaying_percent + '%');
				}
			}
		},
		10);	// この数字を変えるとスピードを調整できる
	});
	</script>
	<div id="content" style="display: none;">
<?php endif; ?>



<button type="button" class="drawer-toggle drawer-hamburger">
	<span class="drawer-hamburger-icon"></span>
</button>
<nav class="drawer-nav cf" role="navigation">
	<ul class="drawer-menu cf">
		<li>
			<a href="<?php echo home_url(); ?>/">
				<span>トップ</span>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service/">
				<span>サービス</span>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/service/laboratory/">
				<span>オフショア開発</span>
			</a>
		</li>
		<li>
			<a href="https://www.calling.fun" target="_blank">
				<span>Calling</span>
			</a>
		</li>
		<!--li>
			<a href="<?php echo home_url(); ?>/kintone/">
				<span>kintone開発サポートサービス</span>
			</a>
		</li-->
		<li>
			<a href="<?php echo home_url(); ?>/service/professional/">
				<span>プロフェッショナルネットワーク</span>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/results/">
				<span>導入事例</span>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/company/">
				<span>会社概要</span>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/neolabox/">
				<span>ニュース・イベント</span>
			</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/glossary/">
				<span>用語集</span>
			</a>
		</li>
		<!--li>
			<a href="<?php echo home_url(); ?>/service/professional/#member">
				<span>メンバー</span>
			</a>
		</li-->
		<li>
			<a href="<?php echo home_url(); ?>/contact/">
				<span>お問い合わせ</span>
			</a>
		</li>
		<li>
			<a class="scroll" href="<?php echo home_url(); ?>/policy/">
				<span>プライバシーポリシー</span>
			</a>
		</li>
	</ul>
</nav>

<header class="g_header cf" role="banner" data-scroll-header>
	<h1 class="g_header_site_title">
		<?php
			global $page, $paged;
			wp_title('|', true, 'right');
			bloginfo('name');
			$site_description = get_bloginfo( 'description', 'display');
		?><?php if ( $site_description && ( is_home() || is_front_page())) { ?><?php echo $site_description; ?><?php } elseif(is_single()) { ?><?php } else { ?><?php } ?>
	</h1>
	<div class="g_header_wrap cf">
		<p class="g_header_logo">
			<a href="<?php echo home_url(); ?>/">
				<svg id="path_logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
				  <path data-name="NEOLAB LOGO" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)" alt="NEOLAB"/>
				</svg>
			</a>
		</p>
		<nav class="g_header_nav drawer-clone cf" role="navigation">
			<ul class="menu cf">
				<li>
					<a href="<?php echo home_url(); ?>/">
						<span>トップ</span>
					</a>
				</li>
				<li class="pulldown">
					<a href="<?php echo home_url(); ?>/service/">
						<span>サービス</span>
						<i class="arrow_carrot-down down"></i>
						<i class="arrow_carrot-up up"></i>
					</a>
					<ul class="sub cf">
						<li class="cf">
							<a href="<?php echo home_url(); ?>/service/laboratory/">
								<span>オフショア開発</span>
							</a>
						</li>
						<li>
							<a href="https://www.calling.fun" target="_blank">
								<span>Calling</span>
							</a>
						</li>
						<!--li class="cf">
							<a href="<?php echo home_url(); ?>/kintone/">
								<span>kintone開発サポートサービス</span>
							</a>
						</li-->
						<li class="cf">
							<a href="<?php echo home_url(); ?>/service/professional/">
								<span>プロフェッショナルネットワーク</span>
							</a>
						</li>

					</ul>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/results/">
						<span>導入事例</span>
					</a>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/company/">
						<span>会社概要</span>
					</a>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/neolabox/">
						<span>ニュース・イベント</span>
					</a>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/glossary/">
						<span>用語集</span>
					</a>
				</li>
				<!--li>
					<a href="<?php echo home_url(); ?>/service/professional/#member">
						<span>メンバー</span>
					</a>
				</li-->
				<li>
					<a href="<?php echo home_url(); ?>/contact/">
						<span>お問い合わせ</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
</header>
<div class="g_header" id="clone_header" data-scroll-header>
	<div class="g_header_wrap cf">
		<p class="g_header_logo">
			<a href="<?php echo home_url(); ?>/">
				<svg id="path_logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
				  <path data-name="NEOLAB LOGO" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)" alt="NEOLAB"/>
				</svg>
			</a>
		</p>
		<nav class="g_header_nav drawer-clone cf" role="navigation">
			<ul class="menu cf">
				<li>
					<a href="<?php echo home_url(); ?>/">
						<span>トップ</span>
					</a>
				</li>
				<li class="pulldown">
					<a href="<?php echo home_url(); ?>/service/">
						<span>サービス</span>
						<i class="arrow_carrot-down down"></i>
						<i class="arrow_carrot-up up"></i>
					</a>
					<ul class="sub cf">
						<li class="cf">
							<a href="<?php echo home_url(); ?>/service/laboratory/">
								<span>オフショア開発</span>
							</a>
						</li>
						<li>
							<a href="https://www.calling.fun" target="_blank">
								<span>Calling</span>
							</a>
						</li>
						<!--li class="cf">
							<a href="<?php echo home_url(); ?>/kintone/">
								<span>kintone開発サポートサービス</span>
							</a>
						</li-->
						<li class="cf">
							<a href="<?php echo home_url(); ?>/service/professional/">
								<span>プロフェッショナルネットワーク</span>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/results/">
						<span>導入事例</span>
					</a>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/company/">
						<span>会社概要</span>
					</a>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/neolabox/">
						<span>ニュース・イベント</span>
					</a>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/glossary/">
						<span>用語集</span>
					</a>
				</li>
				<!--li>
					<a href="<?php echo home_url(); ?>/service/professional/#member">
						<span>メンバー</span>
					</a>
				</li-->
				<li>
					<a href="<?php echo home_url(); ?>/contact/">
						<span>お問い合わせ</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
</div>
