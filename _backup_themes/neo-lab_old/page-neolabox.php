<?php
/*
	template Name: neolabox
*/
?>

<?php get_header(); ?>
<main id="neolabox" class="g_main" role="main">
	<div class="top_visual second_visual bg cf">
		<h1 class="title b0 wow fadeInUp cf" data-wow-delay=".4s">
			ニュース・イベント
		</h1>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
		<div class="foot_neolabox archive cf">
		<div class="inner cf">
			<ul class="post_list text_list cf">
				<?php
				$args = array(
					'showposts'     => -1,
						'post_type' => 'post', /* 投稿タイプ */
						'paged' => $paged
				); ?>
				<?php query_posts( $args ); ?>
				<?php if (have_posts()) : while (have_posts()) : the_post();　?>

				<?php
					$category = get_the_category();
					$cat_id   = $category[0]->cat_ID;
					$cat_name = $category[0]->cat_name;
					$cat_slug = $category[0]->category_nicename;
				?>
				<li>
					<a href="<?php the_permalink(); ?>" class="op cf">
						<article class="article cf">
							<div class="post_data cf">
								<h1 class="post_title cf">
									<?php the_title(); ?>
								</h1>
								<div class="post_data_group cf">
									<time class="time" datetime="<?php echo date('Y.m.d', strtotime($post-> post_date)); ?>">
										<?php echo date('Y.m.d', strtotime($post-> post_date)); ?>
										<?php upData(); ?>
									</time>
									<span class="category <?php echo $cat_slug; ?>">
										<?php echo $cat_name; ?>
									</span>
								</div>
							</div>
						</article>
					</a>
				</li>

				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>

			</ul>
		</div>
	</div>


<?php
/*
	<?php include_once "inc/c_parts_development_service.php"; ?>
	<?php include_once "inc/c_parts_development.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
		*/
?>
</main>


<?php get_footer(); ?>