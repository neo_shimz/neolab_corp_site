<?php

add_action('after_setup_theme', 'load_custom_functions');
function load_custom_functions() {
	$custom_funcsions_path = dirname(__FILE__);


	// オプション
	require_once("{$custom_funcsions_path}/functions/functions-option.php");

	// 企業ロゴ
	require_once("{$custom_funcsions_path}/functions/functions-logo.php");

	// 用語集
	require_once("{$custom_funcsions_path}/functions/functions-glossary.php");

	// メンバー
	require_once("{$custom_funcsions_path}/functions/functions-member.php");

}