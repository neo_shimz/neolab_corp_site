<section class="c_parts_kintone bg cf">
	<div class="inner cf">
		<h2 class="fadeInUp cf" data-wow-delay="0.4s">
			<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/top/kintone_logo.png" alt="kintone開発サポートサービス">
		</h2>
		<!--h2 class="title fadeInUp cf" data-wow-delay="0.4s">
			Calling
		</h2-->
		<div class="txt_1_wrap cf wow fadeInUp cf" data-wow-delay="0.8s">

			<p class="txt_1">
				だれでも、簡単に日々の業務を楽チンに。御社の社内業務のムダを改善しませんか？<br>Kintoneを導入する事で大幅な手作業、及び確認作業を減らし、業務を効率的にする事が可能になります。
			</p>
		</div>
		<ul class="btns btn_center cf wow fadeInUp cf" data-wow-delay="1.2s">
			<li>
				<a href="https://neo-lab.co.jp/kintone/" class="btn_bor_orange">
					<span>詳しく見る</span>
					<i class="arrow_carrot-right"></i>
				</a>
			</li>
		</ul>
	</div>
</section>
