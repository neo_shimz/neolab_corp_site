
<div class="c_parts_contact_bottom cf">
	<h3 class="title cf wow fadeInUp cf" data-wow-delay="0.6s">
		オフショア開発に革新を起こすならネオラボ
	</h3>
	<a href="<?php echo home_url(); ?>/contact/" class="op cf wow fadeInUp cf" data-wow-delay="1.2s">
		<span class="logo">
			<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/svg/svg-logo_white.svg" alt="NEOLAB">
		</span>
		<span class="txt">へのお問い合わせはこちらから</span>
		<i class="arrow_carrot-right"></i>
	</a>
</div>
