
	<section class="neolab_contents lab_contents_3 cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".4s">
				ラボ型オフショア開発のメリット・デメリット
			</h2>
			<div class="advantages_disadvantages cf">
				<div class="item wow fadeInLeft cf" data-wow-delay=".8s">
					<dl class="cf">
						<dt>メリット</dt>
						<dd>・チームビルディングを伴う中長期での自社専属の海外組織づくりが可能</dd>
						<dd>・要求仕様が正確かつ明確に固まっていない状態でもスタートが切れる</dd>
						<dd>・エンジニアに直接指示が出せるため、こまかな修正を速やかに行える</dd>
					</dl>
				</div>
				<div class="item wow fadeInRight cf" data-wow-delay="1.2s">
					<dl class="cf">
						<dt>デメリット</dt>
						<dd>・単発の開発案件には向いておらず、ある程度の継続性を必要とする</dd>
						<dd>・貴社向けのオーダーメイド組織であり、立ち上がりに一定期間を要する</dd>
						<dd>・貴社からマネジメント・ディレクション人材をアサインいただくことが必要</dd>
						<dd>（→アサインが難しい場合は「受託型開発」も併せてご検討ください）</dd>
					</dl>
				</div>
			</div>
		</div>
	</section>
