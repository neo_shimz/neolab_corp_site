

<section class="c_parts_development bg cf">
	<div class="inner cf">
		<div class="item cf wow fadeInLeft cf" data-wow-delay=".4s">
			<h2 class="title">ベトナム・受託型開発</h2>
			<div class="txt_1_wrap cf">
				<p class="txt_1">
					オフショア開発をプロジェクト単位で利用する場合は「受託型開発（請負契約）」が適しています。<br>
					ネオラボは「企画〜設計」を弊社の日本人PMが日本側でお客さまと実施し、「実装〜テスト」を弊社のベトナム法人に在籍する現地プログラマ（単価は日本の3分の1程度）が担当する、というような開発体制が可能になります。
				</p>
			</div>
			<ul class="btns btn_center cf">
				<li>
					<a href="<?php echo home_url() ?>/service/entrusted/" class="btn_bor_orange">
						<span>詳しく見る</span>
						<i class="arrow_carrot-right"></i>
					</a>
				</li>
			</ul>
		</div>
		<div class="item cf wow fadeInRight cf" data-wow-delay=".8s">
			<h2 class="title">ベトナム・ラボ型開発</h2>
			<div class="txt_1_wrap cf">
				<p class="txt_1">
					オフショア開発で自社の現地開発チームを育てる場合は「ラボ型開発（ラボ契約）が適しています。<br>
					特定の現地エンジニア（ネオラボベトナム所属）を自社開発業務や開発プロジェクトに継続的にアサインできることで、一回きりではなく中長期観点から、国内と海外を連携させた開発組織づくりに取り組むことが可能になります。
				</p>
			</div>
			<ul class="btns btn_center cf">
				<li>
					<a href="<?php echo home_url() ?>/service/laboratory/" class="btn_bor_orange">
						<span>詳しく見る</span>
						<i class="arrow_carrot-right"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>

