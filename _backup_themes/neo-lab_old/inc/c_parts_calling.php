
<section class="c_parts_calling bg cf">
	<div class="inner cf">
		<h2 class="fadeInUp cf" data-wow-delay="0.4s">
			<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/top/Calling-logo-yoko.png">
		</h2>
		<!--h2 class="title fadeInUp cf" data-wow-delay="0.4s">
			Calling
		</h2-->
		<div class="txt_1_wrap cf wow fadeInUp cf" data-wow-delay="0.8s">
			
			<p class="txt_1">
				Callingは、Web会議・オンライン商談・チャット接客・チャットボットが一つになったビジネスコミュニケーションツールです。問い合わせ対応、営業活動、取引先とのコミュニケーション、社内会議...社外も社内も、あらゆるビジネスシーンをCalling一つで。情報の分散や複数のツールを利用する手間を省き、業務効率を高めます。
			</p>
		</div>
		<ul class="btns btn_center cf wow fadeInUp cf" data-wow-delay="1.2s">
			<li>
				<a href="https://www.calling.fun" target="_blank" class="btn_bor_orange">
					<span>詳しく見る</span>
					<i class="arrow_carrot-right"></i>
				</a>
			</li>
		</ul>
	</div>
</section>

