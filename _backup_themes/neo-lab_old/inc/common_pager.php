

<div class="pager_wrap cf">
	<?php if(wp_is_mobile()) : ?>
		<div class="cf">
			<?php
				// SP
				pagination_sp($wp_query->max_num_pages);
			?>
		</div>
	<?php else: ?>
		<div class="cf">
			<?php
				// PC
				wp_pagenavi();
			?>
		</div>
	<?php endif; ?>
</div>
