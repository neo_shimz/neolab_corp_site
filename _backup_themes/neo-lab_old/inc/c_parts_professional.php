
<section class="neolab_contents bg2 cf">
	<div class="inner cf">
		<h2 class="title wow fadeInUp cf" data-wow-delay="0.4s">
			ネオラボ・プロフェッショナルネットワーク
		</h2>
		<div class="txt_1_wrap cf wow fadeInUp cf" data-wow-delay="0.8s">
			<p class="txt_1">
				ネオラボはオフショア開発ができる事業開発集団です。システム開発機能だけでなく、事業拡大や新規事業に際して、ビジネスサイドで不足しがちなエキスパート機能について独自のネットワークを構築しています。例えば「メディア」「アプリ」「AI」「VR」「マーケティング」「ビジネス開発」「法務・会計」「海外管理」など、各分野に精通したプロフェッショナル達による付加価値をご提供可能です。
			</p>
		</div>
		<ul class="btns btn_center cf wow fadeInUp cf" data-wow-delay="1.2s">
			<li>
				<a href="<?php echo home_url(); ?>/service/professional/" class="btn_bor_white">
					<span>詳しく見る</span>
					<i class="arrow_carrot-right"></i>
				</a>
			</li>
		</ul>
	</div>
</section>

