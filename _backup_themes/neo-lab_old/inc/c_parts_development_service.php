
<div class="c_parts_development_link cf wow fadeInUp cf" data-wow-delay=".4s">
	<a href="<?php echo home_url(); ?>/service/" class="op">
		<span class="logo">
			<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/svg/svg-logo_white.svg" alt="NEOLAB">
		</span>
		<span class="txt">が提供するオフショア開発サービスはこちらから</span>
		<i class="arrow_carrot-right"></i>
	</a>
</div>