<?php
/*
	template Name: service-laboratory

*/
?>

<?php get_header(); ?>

<main id="laboratory" class="g_main" role="main">
	<div class="second_visual service_top">
		<h1 class="title c_white wow fadeInUp cf" data-wow-delay=".4s">
			ベトナムでラボ型オフショア開発ならネオラボ
		</h1>
		<div class="logo wow fadeInUp cf" data-wow-delay=".8s">
			<svg id="path_logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
				<path data-name="NEOLAB LOGO" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)" alt="NEOLAB"/>
			</svg>
		</div>
		<div class="visual_btn btn_center tc cf wow fadeInUp cf" data-wow-delay="1.4s">
			<a href="<?php echo home_url(); ?>/contact/" class="btn op size_m orange">
				<i class="icon_mail_alt"></i>
				<span>お問い合わせはこちらから</span>
				<i class="arrow_carrot-right"></i>
			</a>
		</div>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>

	<section class="neolab_contents c_black cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".4s">
				ラボ型オフショア開発とは？
			</h2>
			<div class="txt_1_wrap m_width_s tj wow fadeInUp cf" data-wow-delay=".4s" style="max-width: 840px;">
				<p class="txt_1">
					オフショア開発で自社の海外開発チームを育成したい場合（中長期想定）には「ラボ型開発（ラボ契約※）」が適しています。特定の現地エンジニア（ネオラボベトナム所属）を自社開発業務や開発プロジェクトに専属かつ継続的にアサインできることで、一度きりではなく中長期的な観点から国内と海外を連携させた開発組織づくりに取り組むことが可能になります。
				</p>
			</div>
		</div>
	</section>

	<section class="neolab_contents lab_contents_1 c_black cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".4s">
				このようなおなやみは御座いませんか?
			</h2>
			<ul class="lab_list cf">
				<li class="wow fadeInUp cf" data-wow-delay=".8s">
					<dl>
						<dt>
							悩み
							<i class="wf2">1</i>
						</dt>
						<dd>
							開発の内製化を進めたいが、国内の転職市場をどれだけ探しても優秀なエンジニアが出てこない。そもそもエンジニア自体の採用が難しい。
						</dd>
					</dl>
				</li>
				<li class="wow fadeInUp cf" data-wow-delay="1.2s">
					<dl>
						<dt>
							悩み
							<i class="wf2">2</i>
						</dt>
						<dd>
							エンジニア獲得競争の激化で採用コストがどんどん上がっている。お金と時間をかけて採用活動をしても、選考途中で他社に決まってしまうことも多い。
						</dd>
					</dl>
				</li>
				<li class="wow fadeInUp cf" data-wow-delay="1.6s">
					<dl>
						<dt>
							悩み
							<i class="wf2">3</i>
						</dt>
						<dd>
							社内での人材不足が常態化し、優秀なIT人材が作業に忙殺されている。開発業務の分業化や開発組織の役割分担に大きな改善余地が残されている。
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</section>

	<section class="neolab_contents lab_contents_2 c_black cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".3s">
				ネオラボのベトナムラボ型オフショア開発の特徴
			</h2>
			<ul class="pro_list cf">
				<li class="cf wow fadeInUp cf" data-wow-delay=".6s">
					<div class="number lab_txt en wf2">1</div>
					<dl class="mt1">
						<dt>
							非常駐ラボ (開発)
						</dt>
						<dd>
							ネオラボベトナムの既存所属メンバー、または貴社人材要望をもとにした新規求人活動による応募者の中から選んだ、特定エンジニアを一定の契約期間のあいだ貴社専属人員としてアサインします。貴社PMがベトナム現地に常駐する必要がない場合、または数ヶ月に1回程度の訪問による対応で十分である場合、この非常駐ラボをお選びいただくことが多くあります。
						</dd>
					</dl>
				</li>
				<li class="cf wow fadeInUp cf" data-wow-delay="0.9s">
					<div class="number lab_txt en wf2">2</div>
					<dl class="mt1">
						<dt>
							常駐ラボ（開発）
						</dt>
						<dd>
							非常駐ラボと異なる点として、貴社PMがベトナム現地に常駐していることで、ベトナム人エンジニアたちと常時直接コミュニケーションを取ることが可能です。チームビルディング強化やコミュニケーションミス軽減から、そうしたことが求められる開発内容に適しているラボ形態となっています。遠隔による認識ずれが起こりやすい開発の場合、じっくりと現地開発体制を固めていきたい場合に向いていると言えます。
						</dd>
					</dl>
				</li>
				<li class="cf wow fadeInUp cf" data-wow-delay="1.2s">
					<div class="number lab_txt en wf2">3</div>
					<dl class="mt1">
						<dt>
							卒業前提
						</dt>
						<dd>
							常駐ラボを選ばれるお客様のうち、そのオフショア開発が成功すればするほど、現地の開発人員を増加させていただく傾向があります。そして貴社の組織規模が増加し、安定的に組織が運営されていると、ラボ契約ではなく現地法人を設立されたほうが費用対効果は高くなる場合がございます。その際にはネオラボベトナムから卒業（独立・現地法人化）いただくかたちで、貴社専属メンバーを新設現地法人に転籍させることや、現地法人設立に際するサポートも実施可能です。
						</dd>
					</dl>
				</li>
				<li class="cf wow fadeInUp cf" data-wow-delay="1.5s">
					<div class="number lab_txt en wf2">4</div>
					<dl class="mt1">
						<dt>
							コーディング・BPO
						</dt>
						<dd>
							プロジェクトレベルの開発対象に加えて、ネオラボではコーディングやBPO分野のオフショアメニューもワンストップでご提供することが可能です。コーディングメニューを通して、オフショア先で実施する開発体制を整備することで、開発組織の業務効率化や役割分担の高度化が進み、国内組織をより付加価値の高い業務に集中させることが出来ます。また、価格競争力のある地域でBPOのオフショアセンターを立ち上げることで、貴社の膨大な作業対応に関わるコストダウンを図ることも併せて可能となっています。
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</section>

	<?php include_once "inc/c_advantages_disadvantages.php"; ?>
	<?php include_once "inc/c_parts_development.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>

<?php get_footer(); ?>