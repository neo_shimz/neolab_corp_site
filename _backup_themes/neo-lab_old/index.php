
<?php get_header(); ?>

<div class="top_visual cf">
	<div class="visual_txt cf">
		<p class="txt_1 wow fadeInUp cf" data-wow-delay="0.2s">
			アジアの本気が日本を再び熱くする
		</p>
		<p class="txt_2 wow fadeInUp cf" data-wow-delay="0.8s">
			<strong>日本の開発を加速させる</strong>
			<strong>「新世代のオフショア開発プラットフォーム」</strong>
		</p>
	</div>
	<div class="visual_btn btn_center tc cf wow fadeInUp cf" data-wow-delay="1.4s">
		<a href="<?php echo home_url(); ?>/contact/" class="btn op size_m orange">
			<i class="icon_mail_alt"></i>
			<span>お問い合わせはこちらから</span>
			<i class="arrow_carrot-right"></i>
		</a>
	</div>
</div>

<section class="top_contents_1 cf wow fadeInUp cf" data-wow-delay="1.8s">
	<div class="inner cf">
		<h1 class="title cf">
			オフショア開発に革新を起こすならネオラボ
		</h1>
		<p class="txt_1_wrap cf">
			<span class="txt_1">New Experience Open LABoratory = </span>
			<span class="txt_2"><img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/svg/svg-logo_orange.svg" alt="NEOLAB"></span>
		</p>
		<div class="txt_2_wrap cf">
			<p class="txt_1">
				ネオラボは、グローバル規模で人材調達を志向される日本企業のための「オフショアプラットフォーム」です。オフショア開発領域に革新を起こし、日本のアジアにおけるオフショア開発活動を加速させることで、アジアの若き才能と日本企業が未来をともにえがける世界を創ることをミッションとしています。
			</p>
		</div>
	</div>
</section>

<div class="foot_neolabox cf">
	<div class="inner cf">
		<h2 class="title wow fadeInUp cf" data-wow-delay="0.4s">
			<span class="txt">
				最新ニュース・イベント情報
			</span>
		</h2>
		<ul class="post_list neolab_slider bxslider cf wow fadeInUp cf" data-wow-delay="0.8s">
			<?php
				$loop = new WP_Query(array(
					'post_type'			=> 'post',
					'order'				=> 'DESC',
					'post_status' => 'publish',
                    'orderby'     => 'rand',
                    'posts_per_page'      => 1,
					'showposts'			=> 10,
					'tax_query'   => array(
                                     'relation' => 'AND',
                                     $tax_array
                                        ),
				));
				while ($loop->have_posts()) : $loop->the_post();
			?>
			<?php
				$category = get_the_category( "parent=$cat_ID&exclude=170&number=5" );
				$cat_id   = $category[0]->cat_ID;
				$cat_name = $category[0]->cat_name;
				$cat_slug = $category[0]->category_nicename;
			?>
			<li>
				<a href="<?php the_permalink(); ?>" class="op cf">
					<article class="article cf">
						<div class="image">
							<?php if (has_post_thumbnail()) : ?>
								<?php the_post_thumbnail('post-thumbs'); ?>
							<?php else : ?>
								<img src="<?php bloginfo('template_url'); ?>/static/assets/img/common/not_thumbs.png" alt="<?php the_title(); ?>">
							<?php endif ; ?>
						</div>
						<div class="post_data cf">
							<h1 class="post_title cf">
								<?php the_title(); ?>
							</h1>
							<div class="post_data_group cf">
								<time class="time" datetime="<?php echo date('Y.m.d', strtotime($post-> post_date)); ?>">
									<?php echo date('Y.m.d', strtotime($post-> post_date)); ?>
									<?php upData(); ?>
								</time>
								<span class="category <?php echo $cat_slug; ?>">
									<?php echo $cat_name; ?>
								</span>
							</div>
						</div>
					</article>
				</a>
			</li>
			<?php endwhile;　endif; ?>

		</ul>
	</div>
</div>

<section class="neolab_contents bg1 cf">
	<div class="inner cf">
		<h2 class="title wow fadeInUp cf" data-wow-delay=".3s">
			ネオラボのオフショア開発とは？
		</h2>
		<div class="txt_1_wrap wow fadeInUp cf" data-wow-delay=".6s">
			<p class="txt_1">
				ネオラボ（NEOLAB＝New Experience Open LABoratory）は、オフショア開発と人材サービスの融合をコンセプトに、オフショア開発で一挙に注目されている「ベトナム・ダナン」を本拠地として、幅広いオフショアメニュー（Webサービス、スマートフォンアプリ、コーディング、BPO等）を提供している企業です。
				私たちはネオキャリアグループとして、その国内外50を超える地域での総合人材サービスの知見を活かし、<br>
				従来のオフショア開発とは一線を画す、新たなオフショアプラットフォームの構築に取り組んでいきます。
			</p>
		</div>
		<ul class="btns btn_center cf wow fadeInUp cf" data-wow-delay=".9s">
			<li>
				<a href="<?php echo home_url(); ?>/service/laboratory/" class="btn_bor_white">
					<span>詳しく見る</span>
					<i class="arrow_carrot-right"></i>
					<span class="hover"></span>
				</a>
			</li>
		</ul>
	</div>
</section>

<?php include_once "inc/c_parts_development.php"; ?>
<?php include_once "inc/c_parts_calling.php"; ?>
<?php
 /*
<?php include_once "inc/c_parts_kintone.php"; ?>
*/
 ?>
<?php include_once "inc/c_parts_professional.php"; ?>


<section class="top_contents_4 cf">
	<div class="inner cf">
		<h2 class="title wow fadeInUp cf" data-wow-delay="0.4s">
			ネオラボのオフショア開発参加企業
		</h2>
		<ul class="logo_list on cf wow fadeInUp cf" data-wow-delay="0.8s">
			<?php
				$loop = new WP_Query(array(
					'post_type'			=> 'logo',
					'order'				=> 'DESC',
					'showposts'			=> -1,
				));
				while ($loop->have_posts()) : $loop->the_post();
			?>
			<li>
				<?php if(get_field('logo_img_link')) : ?>
				<a href="<?php echo get_field('logo_img_link'); ?>" <?php if(get_field('logo_img_link_tab') == '2') : ?> target="_blank"<?php endif; ?> >
					<img src="<?php echo get_field('logo_img'); ?>" alt="<?php the_title(); ?>">
				</a>
				<?php else : ?>
					<img src="<?php echo get_field('logo_img'); ?>" alt="<?php the_title(); ?>">
				<?php endif; ?>
			</li>
			<?php endwhile; ?>
		</ul>
	</div>
</section>



<?php get_footer(); ?>
