<?php
/*
	template Name: company
*/
?>

<?php get_header(); ?>
<main id="company" class="g_main" role="main">
	<div class="top_visual second_visual bg cf">
		<h1 class="title b0 wow fadeInUp cf" data-wow-delay=".4s">
			会社概要
		</h1>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="wow fadeInUp cf" data-wow-delay=".8s">
	<?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
	</div>
	<?php include_once "inc/c_parts_development_service.php"; ?>
	<?php include_once "inc/c_parts_development.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>

<?php get_footer(); ?>