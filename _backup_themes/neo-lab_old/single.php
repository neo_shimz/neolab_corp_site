
<?php get_header();?>

<style type="text/css">
.image_post {
	max-height: 400px;
	overflow: hidden;
}
.image_post img {
	width: 100%;
	height: auto;
}
.post_wrap {
	text-align: left;
	max-width: 800px;
	padding: 0 1.6em;
	width: 100%;
	margin: 0 auto;
}
blockquote {
    padding: 14.5px 29px;
    margin: 0 0 29px;
    background-color: #f2f2f5;
    border-left: 5px solid #eee;
}
.post_title h1 {
	font-size: 2em;
	margin: 2em 0;
}
h2, h3, h4 {
    font-size: 1.65em;
    letter-spacing: 0.2em;
    line-height: 1.6em;
    -webkit-margin-before: 1.7em;
    -webkit-margin-after: 1.7em;
}
p {
    font-size: 1.2em;
}
</style>

<?php if(have_posts()):while(have_posts()):the_post(); ?>
<div class="image_post">
	<?php if (has_post_thumbnail()) : ?>
		<?php the_post_thumbnail(); ?>
	<?php else : ?>
		<img src="<?php bloginfo('template_url'); ?>/static/assets/img/common/not_thumbs.png" alt="<?php the_title(); ?>">
	<?php endif ; ?>
</div>
<div class="post_wrap cf">
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="post_title cf">
		<h1><?php the_title(); ?></h1>
	</div>
	<div class="post_content cf">
		<?php the_content(); ?>
	</div>
</div>
<?php endwhile;endif; ?>

<?php get_footer();?>


