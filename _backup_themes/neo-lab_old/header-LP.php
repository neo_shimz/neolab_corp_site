<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="initial-scale=1.0, width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php
global $page, $paged;
wp_title( '|', true, 'right' );
bloginfo( 'name' );
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
	echo " | $site_description";
if ( $paged >= 2 || $page >= 2 )
	echo ' | ' . sprintf( __( 'Page %s', 'neo-lab' ), max( $paged, $page ) );
?></title>
<link rel="icon" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/favicon.jpg">
<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/img/ogp/icon-apple.png">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/css/LP_style.css">
<link href='//fonts.googleapis.com/css?family=Roboto:100italic' rel='stylesheet' type='text/css'>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/static/assets/js/jquery.min.js"></script>
<?php wp_head(); ?>
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<?php include_once "inc/tag.php"; ?>
<body>
	<div id="wrapper">
    <header class="LP_header" role="banner">
        <h1 class="LP_header_site_title">ベトナムでオフショア開発ならネオラボ</h1>
        <div class="LP_header_wrap cf">
          <div class="LP_header_logo">
            <a href="https://neo-lab.co.jp/">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
                <defs>
                  <style>
                    .cls-1 {
                      fill: #ffffff;
                      fill-rule: evenodd;
                    }
                  </style>
                </defs>
                <path id="NEOLAB_LOGO" data-name="NEOLAB LOGO" class="cls-1" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)"/>
              </svg>
            </a>
          </div>
          <nav class="LP_header_nav" role="navigation">
            <ul class="menu cf">
              <li>
                <a href="#kintone_inquiry">お問い合わせ</a>
              </li>
              <li>
                <a href="https://neo-lab.co.jp/company/">運営会社</a>
              </li>
            </ul>
          </nav>
        </div>
    </header>
