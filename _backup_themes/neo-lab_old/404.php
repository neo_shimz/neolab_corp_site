<?php get_header(); ?>


<main class="g_main" role="main">
	<div class="second_visual visual404 cf">
		<h1 class="title wf1">
			404 NOT FOUND
		</h1>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="cf mt4 prl2">
		<div class="fs20 tc f-bold cf">
			アクセスしようとしたページが見つかりません
		</div>
		<div class="mt2 tc fs14 cf">
			目的のページは削除されたか、アドレスが変更されている可能性があります。
		</div>
		<ul class="btns btn_center pb40 cf">
			<li>
				<a href="<?php echo home_url(); ?>/" class="btn_bor_orange">
					<span>TOPページへ戻る</span>
					<i class="arrow_carrot-right"></i>
					<span class="hover"></span>
				</a>
			</li>
		</ul>
		<br><br><br>
	</div>
	<?php include_once "inc/c_parts_development_service.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>


<?php get_footer(); ?>
