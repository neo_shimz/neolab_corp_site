<?php
/*
	template Name: service-professional

*/
?>

<?php get_header(); ?>

<main id="professional" class="g_main" role="main">
	<div class="second_visual service_top">
		<h1 class="title wow fadeInUp cf" data-wow-delay=".4s">
			ネオラボのプロフェッショナルネットワーク
		</h1>
		<div class="logo wow fadeInUp cf" data-wow-delay=".8s">
			<svg id="path_logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
				<path data-name="NEOLAB LOGO" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)" alt="NEOLAB"/>
			</svg>
		</div>
		<div class="visual_btn btn_center tc cf wow fadeInUp cf" data-wow-delay="1.2s">
			<a href="<?php echo home_url(); ?>/contact/" class="btn op size_m orange">
				<i class="icon_mail_alt"></i>
				<span>お問い合わせはこちらから</span>
				<i class="arrow_carrot-right"></i>
			</a>
		</div>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>

	<section class="neolab_contents service_pro_1 cf">
		<div class="inner tc">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".4s">
				ネオラボのプロフェッショナルネットワークとは
			</h2>
			<div class="txt_1_wrap m_width_s tj wow fadeInUp cf" data-wow-delay=".8s" style="max-width: 720px;">
				<p class="txt_1">
					ネオラボは、エンジニアサイドとしての開発機能だけでなく、事業拡大や新規事業に際して、ビジネスサイドで一時的に不足しがちなエキスパート機能について独自のネットワークを築いています。<br>
					事業開発集団として「メディア」「アプリ」「ビジネス開発」「マーケティング」「AI」「VR」「法務・会計」「海外管理」など、各分野に精通したプロフェッショナル達による追加支援が可能です。
				</p>
			</div>
		</div>
	</section>

	<section class="neolab_contents service_pro_2 cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".3s">
				プロフェッショナルネットワークでできること
			</h2>
			<ul class="pro_list cf">
				<li class="cf wow fadeInUp cf" data-wow-delay=".6s">
					<div class="number en wf2">a,</div>
					<dl>
						<dt>
							「アプリ、メディア」<small>の場合</small>
						</dt>
						<dd>
							もっとこうしたら良くなるのに...と思っているアプリ・メディアを言われるがままに開発するだけがプロではありません。第三者から見れば自社の強みを持っている企業がそれに気づかず、作るべきオウンドメディアを作れていないこともあります。開発すべきメディアを開発し、強みを活かしたメディアづくりをネオラボは支援しています。
						</dd>
					</dl>
				</li>
				<li class="cf wow fadeInUp cf" data-wow-delay=".9s">
					<div class="number en wf2">b,</div>
					<dl>
						<dt>
							「ビジネス開発、マーケティング」<small>の場合</small>
						</dt>
						<dd>
							新しい事業を始める際に失敗はつきものです。成功を導くための失敗を早い段階で経験し、高速でPDCAを繰り返していくことがスタートアップ・ベンチャーのみならず、社内新規事業でも必要とされます。ネオラボの開発では、これまでに新規事業で数多く失敗そして成功を積み重ねてきたメンターたちが伴走可能です。
						</dd>
					</dl>
				</li>
				<li class="cf wow fadeInUp cf" data-wow-delay="1.2s">
					<div class="number en wf2">c,</div>
					<dl>
						<dt>
							「AI、VR」<small>の場合</small>
						</dt>
						<dd>
							最先端のテクノロジートレンドに精通している開発事業者はそう多くありません。ネオラボはオフショア開発事業を中心としながら、実際の関係者には「新規事業/スタートアップ界隈」に知見のあるメンバーがそろっています。そのため、AIやVRなど昨今急注目される技術分野にも深く広く対応することが可能です。
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</section>

	<section class="neolab_contents neo_member cf" id="member">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".4s">
				メンバー紹介
			</h2>
			<ul class="member_list cf wow fadeInUp cf" data-wow-delay=".8s">
				<?php
					$loop = new WP_Query(array(
						'post_type'			=> 'member',
						'order'				=> 'DESC',
						'showposts'			=> -1,
					));
					while ($loop->have_posts()) : $loop->the_post();
				?>
				<li>
					<div class="thumb">
						<?php if( get_field('member_profile_image') ): ?>
						    <img src="<?php the_field('member_profile_image'); ?>" alt="<?php echo the_title(''); ?>">
						<?php endif; ?>
					</div>
					<div class="profile_text">
						<p class="profile cf">
							<span class="name">
								<?php echo the_title(''); ?>
							</span>
							<span class="name_en">
								<?php echo get_field('member_name_jp'); ?>
							</span>
						</p>
						<?php if(get_field('member_more_btn') == 1) : ?>
						<div class="more_btn op cf">
							<span class="more">read more</span>
							<span class="close">close</span>
							<i class="arrow_carrot-down down"></i>
							<i class="arrow_carrot-up up"></i>
						</div>
						<?php endif; ?>
						<div class="text cf">
							<?php echo get_field('member_profile_text'); ?>
						</div>
					</div>
				</li>
				<?php endwhile; wp_reset_query(); ?>
			</ul>
		</div>
	</section>




	<?php include_once "inc/c_parts_development.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>

<script>
$(function() {
	$('.more_btn').on('click', function() {
		$(this).next('.text').prev().stop().toggleClass('open');
	});
});
</script>

<?php get_footer(); ?>