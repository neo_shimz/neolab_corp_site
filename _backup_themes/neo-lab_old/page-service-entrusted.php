<?php
/*
	template Name: service-entrusted

*/
?>

<?php get_header(); ?>

<main id="entrusted" class="g_main" role="main">
	<div class="second_visual service_top">
		<h1 class="title wow fadeInUp cf" data-wow-delay=".4s">
			ベトナムで受託型オフショア開発ならネオラボ
		</h1>
		<div class="logo wow fadeInUp cf" data-wow-delay=".8s">
			<svg id="path_logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 497 102">
				<path data-name="NEOLAB LOGO" d="M402,238.741v64.838H390V210l73.859,73.255V218.419h12V312ZM618.969,310.7a49.945,49.945,0,0,1-49.749-43.746H496.883v24.723h71.376v11.905H484.881v-85.16h83.378v11.9H496.883v24.724H569.22A49.945,49.945,0,0,1,618.969,211.3c27.63,0,50.108,22.293,50.108,49.7S646.6,310.7,618.969,310.7Zm-37.627-43.746a37.794,37.794,0,1,0,0-11.9h23.187a15.51,15.51,0,1,1,0,11.9H581.342ZM811,303.579l-33.22-65.9-33.22,65.9h-66.19v-85.16h12v73.255H737.14l40.639-80.613,40.639,80.613h39.27a17.17,17.17,0,1,0,0-34.338h-3.52v-11.9h3.52a7.555,7.555,0,1,0,0-15.11H832.2v-11.9h25.484a19.411,19.411,0,0,1,15.48,31.392A28.874,28.874,0,0,1,887,274.506a29.228,29.228,0,0,1-29.313,29.073H811Z" transform="translate(-390 -210)" alt="NEOLAB"/>
			</svg>
		</div>
		<div class="visual_btn btn_center tc cf wow fadeInUp cf" data-wow-delay="1.4s">
			<a href="<?php echo home_url(); ?>/contact/" class="btn op size_m orange">
				<i class="icon_mail_alt"></i>
				<span>お問い合わせはこちらから</span>
				<i class="arrow_carrot-right"></i>
			</a>
		</div>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>

	<section class="neolab_contents lab_contents_1 c_black cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".3s">
				このようなおなやみは御座いませんか?
			</h2>
			<ul class="lab_list cf">
				<li class="wow fadeInUp cf" data-wow-delay=".6s">
					<dl>
						<dt>
							悩み
							<i class="wf2">1</i>
						</dt>
						<dd>
							プロジェクトマネージャーを担当できる人材が社内で見つからない。自社だけで企画・ディレクションしきれない。【PM人材不足】
						</dd>
					</dl>
				</li>
				<li class="wow fadeInUp cf" data-wow-delay=".9s">
					<dl>
						<dt>
							悩み
							<i class="wf2">2</i>
						</dt>
						<dd>
							社内にエンジニア組織がいなくてもよいので、社外開発パートナーとの連携から新しいサービスを作っていきたい。【外部開発連携】
						</dd>
					</dl>
				</li>
				<li class="wow fadeInUp cf" data-wow-delay="1.2s">
					<dl>
						<dt>
							悩み
							<i class="wf2">3</i>
						</dt>
						<dd>
							受託開発の会社に出してもらった開発プロジェクトの見積もり費用が高すぎるため、開発できる対象が絞られてしまう。【コストダウン】
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</section>

	<section class="neolab_contents lab_contents_2 c_black cf">
		<div class="inner tc cf">
			<h2 class="title d_inb w_auto wow fadeInUp cf" data-wow-delay=".3s">
				ネオラボのベトナム受託型オフショア開発の特徴
			</h2>
			<ul class="pro_list cf">
				<li class="wow fadeInUp cf" data-wow-delay=".6s">
					<div class="number lab_txt en wf2">1</div>
					<dl>
						<dd class="mt1">
							ネオラボ所属のプロジェクトマネージャー（PM）が開発統括となり、お客様と密な連携をとりながら、企画・設計段階から成果物の納品まで一貫したマネジメントを行うことで、プロジェクトを成功裏に導きます。
						</dd>
					</dl>
				</li>
				<li class="wow fadeInUp cf" data-wow-delay=".9s">
					<div class="number lab_txt en wf2">2</div>
					<dl>
						<dd class="mt1">
							Webサービス・スマホアプリ分野に豊富な新規開発経験を持つPMをプロジェクトアサインすることで、ご依頼された開発対象を完成させるのみならず、より良い成果物に向けたブラッシュアップのご支援も行うことが可能です。
						</dd>
					</dl>
				</li>
				<li class="wow fadeInUp cf" data-wow-delay="1.2s">
					<div class="number lab_txt en wf2">3</div>
					<dl>
						<dd class="mt1">
							日本国内において上流工程を担当するネオラボに加えて、オフショア現地でのネオラボベトナムを下流工程に組み合わせることで、総合的なコスト優位性を持つ受託開発プロジェクトのお見積り・ご提案をすることが出来ます。
						</dd>
					</dl>
				</li>
				<li class="wow fadeInUp cf" data-wow-delay="1.5s">
					<div class="number lab_txt en wf2">4</div>
					<dl>
						<dd class="mt1">
							ネオラボはオフショア開発拠点として中長期的な価格競争力を維持できる「ベトナム（ダナン）」を選んでいるため、オフショア先の新興国における賃金/物価上昇に伴う開発コストの値上がり幅を最小限に留めることが可能です。
						</dd>
					</dl>
				</li>
			</ul>
		</div>
	</section>

	<?php include_once "inc/c_advantages_disadvantages.php"; ?>
	<?php include_once "inc/c_parts_development.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
	<?php include_once "inc/c_parts_contact_bottom.php"; ?>
</main>

<?php get_footer(); ?>