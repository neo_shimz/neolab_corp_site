<?php
/*
	template Name: int-contact2
*/
?>

<?php get_header(); ?>

<main id="contact" class="g_main" role="main">
	<div class="second_visual cf">
		<h1 class="title">
			サマーインターンお申込み
		</h1>
	</div>
	<?php include_once "inc/c_breadcrumbs.php"; ?>
	<div class="g_form cf">
		<div class="g_title display cf">
			<h2 class="title">エントリーフォーム</h2>
			<div class="txt1">
				下記項目をご入力ください。<br>面接日にskypeを選択する場合は、skype IDを入力してください。
			</div>
		</div>
		<?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
		<div class="display text_bottom cf">
			<p>
				弊社「<a href="http://www.neo-career.co.jp/policy.html" target="_blank">プライバシーポリシー</a>
				」を必ずご確認・ご同意の上、お問い合わせください。
			</p>
		</div>
	</div>
	<?php include_once "inc/c_parts_development_service.php"; ?>
	<?php include_once "inc/c_parts_professional.php"; ?>
</main>

<script>
$(function() {
	$('.tel').find('input[type=text]').prop('type', 'tel');
	var submitText = '送信内容を確認';
	$('.btn_submit li.submit input').val(submitText);
});
</script>

<?php get_footer(); ?>