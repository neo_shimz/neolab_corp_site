<?php
/**
 * 【商品投稿タイプ定義】
 */
add_action( 'init', 'create_logo', 0);
function create_logo() {
	$top_type = array(
		'exclude_from_search' => true, // 検索結果表示しない
		'label' => '企業ロゴ',
		'labels' => array(
			'name' => '企業ロゴ',
			'singular_name' => '企業ロゴ',
			'add_new_item' => '企業ロゴを追加',
			'new_item' => '新規企業ロゴ',
			'view_item' => '企業ロゴを表示',
			'not_found' => '企業ロゴは見つかりませんでした',
			'not_found_in_trash' => 'ゴミ箱に企業ロゴはありません。',
			'search_items' => '企業ロゴを検索'
		),
		'public' => true,
		'rewrite' => array('slug' => 'logo', 'with_front' => false),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title')
	);
	register_post_type( 'logo', $top_type);

}
