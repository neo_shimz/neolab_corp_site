<?php
/**
 * 【商品投稿タイプ定義】
 */
add_action( 'init', 'create_member', 0);
function create_member() {
	$top_type = array(
		'exclude_from_search' => true, // 検索結果表示しない
		'label' => 'メンバー',
		'labels' => array(
			'name' => 'メンバー',
			'singular_name' => 'メンバー',
			'add_new_item' => 'メンバーを追加',
			'new_item' => '新規メンバー',
			'view_item' => 'メンバーを表示',
			'not_found' => 'メンバーは見つかりませんでした',
			'not_found_in_trash' => 'ゴミ箱にメンバーはありません。',
			'search_items' => 'メンバーを検索'
		),
		'public' => true,
		'rewrite' => array('slug' => 'member', 'with_front' => false),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title', 'thumbnail')
	);
	register_post_type( 'member', $top_type);

}
