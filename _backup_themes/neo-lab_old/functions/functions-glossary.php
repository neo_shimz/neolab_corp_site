<?php

/**
 * 特集投稿タイプ定義
 */

add_action( 'init', 'create_glossary' , 0);

function create_glossary() {
	$glossary_type = array(
		'label' => '用語集',
		'labels' => array(
				'name' => '用語集',
				'singular_name' => '用語集',
				'add_new_item' => '用語集を追加',
				'new_item' => '用語集を追加',
				'view_item' => '用語集を表示',
				'not_found' => '用語集は見つかりませんでした',
				'not_found_in_trash' => 'ゴミ箱に用語集はありません。',
				'search_items' => '用語集を検索'
		),
		'public' => true,
		'rewrite' => array(
			'slug' => 'glossary',
			'with_front' => false
		),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type( 'glossary', $glossary_type);



	// カテゴリータクソノミー
	$glossary_category_taxonomy = array(
		'hierarchical' => true,
		'label' => '用語集カテゴリー',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'glossary_category'
		),
		'singular_label' => 'glossary_category'
	);
	register_taxonomy(
		'glossary_category',
		'glossary',
		$glossary_category_taxonomy
	);



	// キーワードタクソノミー
	$glossary_keyword_taxonomy = array(
		'hierarchical' => false,
		'label' => '用語集キーワード',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'glossary_keywords'
		),
		'singular_label' => 'glossary_keywords'
	);
	register_taxonomy(
		'glossary_keywords',
		'glossary',
		$glossary_keyword_taxonomy
	);


}
