<?php

// バージョン自動更新の無効化
add_filter('automatic_updater_disabled', '__return_true');
// END


// サムネイルの有効化
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
	// regenerate-thumbnails【プラグインの画像設定】 trueで画像をリサイズ, falseで切り抜きなし
	// コンテンツアイキャッチ画像
	add_image_size('member-thumbs', 200, 200, true);
	//add_image_size('contents-thumbs_s', 130, 80, true); // PC & SP
	// END
};
// END


// 日本語のスラッグ名を自動変更
function auto_post_slug( $slug, $post_ID, $post_status, $post_type ) {
	if ( preg_match( '/(%[0-9a-f]{2})+/', $slug ) ) {
		$slug = utf8_uri_encode( $post_type ) . '-' . $post_ID;
	}
	return $slug;
}
add_filter( 'wp_unique_post_slug', 'auto_post_slug', 10, 4  );
// END



// いらないメタタグ削除
// remove_action('wp_head','rest_output_link_wp_head');
// remove_action( 'wp_head', 'feed_links_extra', 3 );
// remove_action( 'wp_head', 'feed_links', 2 );
// remove_action( 'wp_head', 'rsd_link' );
// remove_action( 'wp_head', 'wlwmanifest_link' );
// remove_action( 'wp_head', 'index_rel_link' );
// remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
// remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
// remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );

remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );	//絵文字
remove_action( 'wp_print_styles', 'print_emoji_styles' );		//絵文字
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');
// END


// USERAGENT
function is_mobile(){
	$useragents = array(
	'iPhone', // iPhone
	'iPod', // iPod touch
	'Android.*Mobile', // 1.5+ Android *** Only mobile
	'Windows.*Phone', // *** Windows Phone
	'dream', // Pre 1.5 Android
	'CUPCAKE', // 1.5+ Android
	'blackberry9500', // Storm
	'blackberry9530', // Storm
	'blackberry9520', // Storm v2
	'blackberry9550', // Storm v2
	'blackberry9800', // Torch
	'webOS', // Palm Pre Experimental
	'incognito', // Other iPhone browser
	'webmate' // Other iPhone browser
	);
	$pattern = '/'.implode('|', $useragents).'/i';
	return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}
// END


// 日付にUPDATA 日を表示
function upData() {
	$days			= 7;
	$daysInt 		= ($days - 1) * 86400;
	$today 			= time();
	$entry 			= get_the_time('U');
	$dayago 		= $today - $entry;
	if ( $dayago < $daysInt ) {
		echo '<i class="new">NEW</i>';
	}
}
// END


// ページャー SP
function pagination_sp($pages = '') {
	global $paged;
	if(empty($paged)) {
		$paged = 1;
	}
	if($pages === '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages) {
			$pages = 1;
		}
	}
	if(1 != $pages) {
		echo "<div class=\"pagination cf\">";
		if($paged > 2) {
			echo "<a class='arrow-left-2' href='".get_pagenum_link(1)."'><i class='arrow_carrot-2left'></i></a>";
		}
		if($paged > 1) {
			echo "<a class='arrow-left-1' href='".get_pagenum_link($paged - 1)."'><i class='arrow_carrot-left'></i></a>";
		}
		echo "<span class=\"current\">".$paged." / ".$pages."</span>";
		if ($paged < $pages) {
			echo "<a class='arrow-right-1' href=\"".get_pagenum_link($paged + 1)."\"><i class='arrow_carrot-right'></i></a>";
		}
		if ($paged < $pages-1) {
			echo "<a class='arrow-right-2' href='".get_pagenum_link($pages)."'><i class='arrow_carrot-2right'></i></a>";
		}
		echo "</div>\n";
	}
}
// END


// ページャー 次へ 前へ
function pagerSingle() { ?>
	<ul class="single_pager cf">
		<?php
			$prev_post = get_previous_post();
			if(!empty($prev_post)) :
		?>
		<li class="f_left">
			<a href="<?php echo get_permalink($prev_post->ID); ?>" class="icon-left-open-big prev">
				<i class="arrow_carrot-left"></i>
				<span>PREV</span>
			</a>
		</li>
		<?php endif; ?>
		<?php
			$next_post = get_next_post();
			if(!empty($next_post)) :
		?>
		<li class="f_right">
			<a href="<?php echo get_permalink($next_post->ID); ?>" class="icon-right-open-big next">
				<span>NEXT</span>
				<i class="arrow_carrot-right"></i>
			</a>
		</li>
		<?php endif; ?>
	</ul>
<?php }




// ソーシャルボタン
function socialButton() { // ここのアカウントは、ジンジャー ?>


<!-- Facebook SclBtn -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id; js.async = true;
js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.3";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



<ul class="sns_wrap cf">
	<li class="facebook">
		<div class="fb-like" data-href="<?php the_permalink(); ?>" data-width="100" data-layout="button_count" data-show-faces="false" data-send="false">&nbsp;
		</div>
	</li>
	<li class="twitter">
		<a href="//twitter.com/share" data-count="horizontal" class="twitter-share-button" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" data-lang="jp">&nbsp;</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.async=true;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	</li>
	<?php /*
	<li class="hatena">
		<div class="wsbl_hatena_button">
			<a href="//b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="standard-balloon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加">
			    <img src="//b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" />
			</a>
		</div>
	</li>
	*/ ?>
	<li class="google">
		<div class="g-plusone" data-size="medium" data-href="<?php the_permalink(); ?>">&nbsp;</div>
		<script type="text/javascript" src="//apis.google.com/js/plusone.js" async="async" gapi_processed="true">{lang: 'ja'}</script>
	</li>
	<?php if ( function_exists('wp_is_mobile') && wp_is_mobile() ) :?>
	<li class="line">
		<span>
			<script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script>
			<script type="text/javascript">
				new media_line_me.LineButton({"pc":false,"lang":"ja","type":"a","text":"<?php the_permalink(); ?>","withUrl":true});
			</script>
		</span>
	</li>
	<?php endif; ?>
</ul>


<?php }

// END


