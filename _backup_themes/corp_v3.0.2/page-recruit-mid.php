<?php
/*
	template Name: 中途採用
*/
?>
<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-recruit'); ?>
<div id="particle-canvas" style="display: none;"></div>

<div class="r_visual__area js-full-window">
  <div class="r_visual__area-inner">
    <ul class="r_css__slider cb-slideshow-new">
      <li></li>
      <li></li>
      <li></li>
    </ul>
    <div class="r_visual__area-description">
      <h1 class="-title">
        <span class="gf1 -en">graduates</span>
        <span class="-jp">新卒採用</span>
      </h1>
      <p class="-description">
        若手社員でも、ビックプロジェクトに参加出来ます。<br>
        先輩社員と一緒に楽しく働きませんか？
      </p>
      <div class="btn_group">
        <a href="<?php echo home_url(); ?>/recruit-top/new/form/" class="op -btn -wave -white">
          <span>新卒採用フォーム</span>
        </a>
        <a href="<?php echo home_url(); ?>/recruit-top/mid/" class="-link">
          <span>中途採用はこちら</span>
        </a>
      </div>
    </div>
    <div class="r_visual__join">
      <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/recruit/join.svg" alt="join us!">
    </div>
  </div>
</div>
<main class="g_main r_new__main" role="main" id="main">

<div style="font-size: 3rem; padding: 4rem; text-align: center; background: #eee;">途中</div>

  <div class="r_section -user">

    <?php wpBreadcrumbs(); ?>
    <div class="r_form__area">
      <div class="r_form__area-inner">
        <p class="-description">
          将来どんな自分になり、どんな仕事をしたいか。<br>
          ネオラボは、新卒でも活躍出来きワクワク出来る環境があります。
        </p>
        <ul class="btn_group">
          <li>
            <a href="<?php echo home_url(); ?>/recruit-top/new/form/" class="-btn op">
              <span>新卒採用フォーム</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/recruit-top/mid/" class="-link">
              <span>中途採用はこちら</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="r_interview__slider">
    <h3><?php echo the_title(''); ?>インタビュー</h3>
    <div class="r_interview__slider-inner js-interview_slider">
      <?php
      $loop = new WP_Query(array(
        'post_type'      => 'interview',
        'order'        => 'DESC',
        'showposts'     => 5,
        'tax_query' => array(
          array(
            'taxonomy' => 'interview_category',
            'field'    => 'slug',
            'terms'    => 'mid', // 新卒のみ表示 new(新卒) or  mid(中途)
          ),
        ),
      ));
      while ($loop->have_posts()) : $loop->the_post(); ?>
        <article class="r_slider__items">
          <a href="<?php the_permalink(); ?>" class="r_slider__items-inner">
            <?php the_post_thumbnail('', array('class' => 'r_slider__img')); ?>
            <div class="r_slider__info">
              <p class="r_user__name"><?php the_title(''); ?></p>
              <!-- <p class="r_user__message"><?php echo get_field('interviewCopy'); ?></p>
                      <p class="r_user__year"><?php echo get_field('interviewJoiningYear'); ?></p> -->
              <p class="r_user__position"><?php echo get_field('interviewPost'); ?></p>
            </div>
          </a>
        </article>
      <?php endwhile; wp_reset_query(); ?>
    </div>
  </div>

</main>


<?php RecruitFooterBnner(); ?>
<?php get_template_part('_include/footer'); ?>