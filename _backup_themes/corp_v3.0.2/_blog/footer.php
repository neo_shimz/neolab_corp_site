			</div><!-- g_barba__main-inner -->
		</div><!-- barba-container -->
	</main><!-- #barba-wrapper -->
	<div class="pjax_share__loader"></div>
	<div class="pjax_overlay cf">
		<div class="loader">
			<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/logo-default.svg" alt="<?php echo bloginfo( 'name' ); ?>" />
		</div>
	</div>
</div><!-- g_page__wrap -->

<footer class="g_footer cf" role="contentinfo">
	<div class="g_footer__inner">
		<div class="g_footer__primary">
			<nav class="g_footer__nav-items">
				<p class="g_title__dot wfont1">Category</p>
				<ul class="list">
					<li>
						<a href="<?php echo home_url(); ?>/">
							<span>ホーム</span>
						</a>
					</li>
					<?php
            $terms = get_terms('blog_category');
            foreach ( $terms as $term ) {
              echo '<li class="items"><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
            }
          ?>
				</ul>
			</nav>
			<nav class="g_footer__nav-items">
				<p class="g_title__dot wfont1">Menu</p>
				<ul class="list">
					<li>
						<a href="<?php echo home_url(); ?>/about/" class="no-barba">
							<span>ネオラボとは</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/solution/" class="no-barba">
							<span>ソリューション</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/recruit-top/" class="no-barba">
							<span>採用情報</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/interview/" class="no-barba">
							<span>インタビュー</span>
						</a>
					</li>
				</ul>
			</nav>
			<nav class="g_footer__nav-items">
				<p class="g_title__dot wfont1">Info</p>
				<ul class="list">
					<li>
						<a href="<?php echo home_url(); ?>/company/" class="no-barba">
							<span>会社概要</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/policy/" class="no-barba">
							<span>プライバシーポリシー</span>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/contact/" class="no-barba">
							<span>お問い合わせ</span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="g_footer__secondary">
			<p class="g_copy">
				<i class="icon-copyright"></i>
				<span class="wfont1">NEOLAB CO.ltd</span>
			</p>
			<ul class="g_footer__sns">
				<li class="facebook">
					<a href="https://www.facebook.com/neolabjpve/" target="_blank">
						<i class="icon-facebook"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</footer>
<?php // 共通 ?>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/js_blog/style-dist.js"></script>
<?php // 共通 END ?>
<script>
function sns_window( item, height, width ){
	var size = 'menubar=no, toolbar=no, resizable=yes, scrollbars=yes, height='+height+', width='+width;
	window.open( item.href, '_blank', size );
	return false;
}
</script>
<?php wp_footer(); ?>
</body>
</html>
