
<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-recruit'); ?>
<div id="particle-canvas" style="display: none;"></div>

<?php function slideCalling () { ?>
  <section class="t_main__visual -calling js-full-window">
    <div class="js-loading_1 l_loading calling">
      <div class="l_loading__bg">
        <span class="gf1">NEOLAB Produce</span>
      </div>
      <div class="l_loading__conts js-loading_2">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-calling.svg" alt="calling" />
      </div>
    </div>
    <div class="t_circle c_1"></div>
    <div class="t_circle c_2"></div>
    <div class="t_circle c_3"></div>
    <div class="t_main__visual-inner">
      <div class="t_main__visual-description__area">
        <p class="-head">NEOLAB Produce</p>
        <h1 class="-title">
          <span>全ての</span><br>
          <span>コミュニケーションを</span><br>
          <span>Calling一つで</span>
        </h1>
      </div>
      <a data-scroll href="#t_about" class="t_main__visual-arrow ">
        <span class="gf1">More Projects</span>
        <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M8.00021 6.62732L14.6274 6.62744L7.93598 14.7608L1.37286 6.62714L8.00021 6.62732Z" fill="#333333"/>
        </svg>
      </a>
    </div>
  </section>
<?php } ?>

<?php function slideJinjer () { ?>
  <section class="t_main__visual -jinjer js-full-window">
    <div class="js-loading_1 l_loading jinjer">
      <div class="l_loading__bg">
        <span class="gf1">NEOLAB Produce</span>
      </div>
      <div class="l_loading__conts js-loading_2">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-jinjer.svg" alt="jinjer" />
      </div>
    </div>
    <div class="t_circle c_1"></div>
    <div class="t_circle c_2"></div>
    <div class="t_circle c_3"></div>
    <div class="t_main__visual-inner">
      <div class="t_main__visual-description__area">
        <p class="-head">NEOLAB Produce</p>
        <h1 class="-title">
          <span>誰でも簡単に利用できる</span><br>
          <span>マルチデバイス対応の</span><br />
          <span>クラウド型勤怠システム</span>
        </h1>
      </div>
      <a data-scroll href="#t_about" class="t_main__visual-arrow ">
        <span class="gf1">More Projects</span>
        <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M8.00021 6.62732L14.6274 6.62744L7.93598 14.7608L1.37286 6.62714L8.00021 6.62732Z" fill="#333333"/>
        </svg>
      </a>
    </div>
  </section>
<?php } ?>

<?php function slideEnigma () { ?>
  <section class="t_main__visual -enigma js-full-window">
    <div class="js-loading_1 l_loading enigma">
      <div class="l_loading__bg">
        <span class="gf1">NEOLAB Produce</span>
      </div>
      <div class="l_loading__conts js-loading_2">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/solution/logo-enigma.svg" alt="enigma" />
      </div>
    </div>
    <div class="t_circle c_1"></div>
    <div class="t_circle c_2"></div>
    <div class="t_circle c_3"></div>
    <div class="t_main__visual-inner">
      <div class="t_main__visual-description__area">
        <p class="-head">NEOLAB Produce</p>
        <h1 class="-title">
          <span>従業員は働いた分の</span><br>
          <span>給与をいつでも</span><br>
          <span>受け取り可能に</span>
        </h1>
      </div>
      <a data-scroll href="#t_about" class="t_main__visual-arrow ">
        <span class="gf1">More Projects</span>
        <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M8.00021 6.62732L14.6274 6.62744L7.93598 14.7608L1.37286 6.62714L8.00021 6.62732Z" fill="#333333"/>
        </svg>
      </a>
    </div>
  </section>
<?php } ?>

<?php
  $min = 1;
  $max = 3;
  $r = rand($min,$max);
  if ($r == 1) {
    slideCalling();
  } elseif ($r == 2) {
    slideJinjer();
  } elseif ($r == 3) {
    slideEnigma();
  }
?>


<section class="t_about__area" id="t_about">
	<div class="t_about__area-inner">
		<h1 class="-title js-view e_revealers">
      <span class="e_inner">
        新しい未来を創造する
        <span class="e_block"></span>
      </span>
    </h1>
    <p class="-description js-view e_revealers">
      <span class="e_inner">
        新規事業を専門とした共同事業開発サービスを提供するスペシャリスト集団です。<br />
        ネオラボのエンジニアはAI、VR、ドローン、Fintechなどのトレンド技術を得意とします。<br />
        さらにビジネスサイドのエキスパートが最適な事業戦略を立案・実行し、<br />
        Tech×Businessのトータルソリューションを提案します。
        <span class="e_block"></span>
      </span>
    </p>
	</div>
</section>

<section class="solution__area">
	<div class="solution__area-inner">
		<?php get_template_part('_include/inc-solution'); ?>
	</div>
</section>

<section class="service_flow__area">
	<div class="service_flow__inner">
		<div class="illust">
			<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/illust-service_flow.svg" alt="" />
		</div>
		<p class="description">
			ネオラボは、ドローン・VR・AR・アプリ開発といった様々な<br />
			開発をしております。お気軽にお問い合わせください。
		</p>
		<div class="service_btn__area">
			<div class="service_btn__items">
				<a href="<?php echo home_url(); ?>/solution/" class="btn_effect__roll ">
					<div class="-text gf1">SOLUTION</div>
					<div class="-text gf1">SOLUTION</div>
				</a>
			</div>
			<div class="service_btn__items">
				<a href="<?php echo home_url(); ?>/contact/" class="btn_effect__roll ">
					<div class="-text gf1">CONTACT</div>
					<div class="-text gf1">CONTACT</div>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="t_client__area -top">
	<div class="t_client__area-inner">
		<div class="t_title__area">
			<h1 class="-title">
				<span class="-en">CLIENT</span>
			</h1>
			<p class="-text">
        ネオラボは多くの企業様と新規事業の開発を行っております。<br />
        「次回もネオラボと一緒に開発したい」と言っていただける信頼を築いてまいります。
			</p>
		</div>
		<div class="t_logo__area">
			<div class="t_logo__area-inner">
				<ul>
					<?php
						$loop = new WP_Query(array(
							'post_type'			=> 'client',
							'order'					=> 'DESC',
							'showposts'			=> -1,
						));
						while ($loop->have_posts()) : $loop->the_post();
					?>
						<li>
							<div class="-img_wrap">
								<?php if (has_post_thumbnail()) : ?>
									<?php the_post_thumbnail('client-logo-thumbs', array('class' => 'thumb', 'alt' => get_the_title() )); ?>
								<?php endif; ?>
							</div>
						</li>
					<?php endwhile; ?>
          <?php wp_reset_query(); ?>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="t_news__area -top">
	<div class="t_news__area-inner">
		<div class="t_title__area">
			<h1 class="-title">
				<span class="-en">NEWS</span>
			</h1>
		</div>
		<div class="t_news__list">
			<?php
				$loop = new WP_Query(array(
					'post_type'			=> 'post',
					'order'					=> 'DESC',
					'showposts'			=> 2,
				));
				while ($loop->have_posts()) : $loop->the_post();
			?>
				<?php newsPost(); ?>
			<?php endwhile; ?>
      <?php wp_reset_query(); ?>
		</div>
	</div>
</section>
<?php RecruitFooterBnner(); ?>
<?php get_template_part('_include/footer'); ?>
