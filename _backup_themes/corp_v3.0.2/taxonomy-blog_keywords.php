<?php get_template_part('./_blog/header'); ?>

<div class="g_page__area cf">
	<div class="g_main cf">
		<div>
			<h1 class="s_sub_title">
				<span>
					<?php echo get_term_by('slug',$term,$taxonomy)->name ?>
				</span>
			</h1>
		</div>
		<div class="g_main__inner">
			<?php
			$loop = new WP_Query(array(
				'post_type'			=> 'blog',
				'order'				=> 'DESC',
				'taxonomy'			=> 'blog_keywords',
				'term'				=> get_term_by('slug', $term, $taxonomy)->name
			));
				while(have_posts()): the_post();
			?>
				<?php postBlogArticle(); ?>
			<?php endwhile; ?>
		</div>
		<?php wpPagenavi(); ?>
	</div>
	<?php get_template_part('./_blog/sidebar'); ?>
</div>


<?php wpBreadcrumbs(); ?>
<?php get_template_part('./_include/inc-sns'); ?>
<?php get_template_part('./_blog/footer'); ?>
