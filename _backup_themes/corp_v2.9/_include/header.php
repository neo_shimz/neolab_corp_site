<?php
get_template_part('_include/inc-tag');
get_template_part('_include/inc-post');
$page = get_post( get_the_ID() );
$slug = $page->post_name;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php wp_enqueue_script('jquery'); ?>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="initial-scale=1.0, width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php siteTitle(); ?></title>
<meta name="google-site-verification" content="mk7g_asRHiapFA5L8OXNdWTtTLbECJLQXEj3RSkOpW0" />
<link rel="shortcut icon" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/favicons/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/favicons/android-chrome-256x256.png">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/font/icomoon/style.css?v1">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:500,700,900" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/style.css<?php // echo '?' . mt_rand(1, 300); ?>">
<?php wp_head(); ?>
<?php tagGTM(); ?>
</head>
<body class="<?php bodyAddClass(); ?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.2&appId=214224325296092&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<?php tagGTM_Noscript(); ?>



<a href="<?php echo home_url(); ?>/" class="g_logo -fixed ">
  <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/logo-default.svg" alt="<?php bloginfo( 'name' ); ?>" />
</a>
<div class="g_menu__area">
  <div class="hamburger js-hover">
    <div class="hamburger__line hamburger__line--01">
      <div class="hamburger__line-in hamburger__line-in--01"></div>
    </div>
    <div class="hamburger__line hamburger__line--02">
      <div class="hamburger__line-in hamburger__line-in--02"></div>
    </div>
    <div class="hamburger__line hamburger__line--03">
      <div class="hamburger__line-in hamburger__line-in--03"></div>
    </div>
    <div class="hamburger__line hamburger__line--cross01">
      <div class="hamburger__line-in hamburger__line-in--cross01"></div>
    </div>
    <div class="hamburger__line hamburger__line--cross02">
      <div class="hamburger__line-in hamburger__line-in--cross02"></div>
    </div>
  </div>
  <div class="global-menu">
    <div class="global-menu__wrap">
      <a class="global-menu__item global-menu__item--style-3 no-barba no-effect" href="<?php echo home_url(); ?>/about/"><span>ABOUT</span></a>
      <a class="global-menu__item global-menu__item--style-3 no-barba no-effect" href="<?php echo home_url(); ?>/solution/"><span>SOLUTION</span></a>
      <a class="global-menu__item global-menu__item--style-3 no-barba no-effect" href="<?php echo home_url(); ?>/company/"><span>COMPANY</span></a>
      <a class="global-menu__item global-menu__item--style-3 no-barba no-effect" href="<?php echo home_url(); ?>/news/"><span>NEWS</span></a>
      <a class="global-menu__item global-menu__item--style-3 no-barba no-effect" href="<?php echo home_url(); ?>/recruit-top/"><span>RECRUIT</span></a>
      <a class="global-menu__item global-menu__item--style-3 no-barba no-effect" href="<?php echo home_url(); ?>/interview/"><span>INTERVIEW</span></a>
      <a class="global-menu__item global-menu__item--style-3 no-barba no-effect" href="<?php echo home_url(); ?>/contact/"><span>CONTACT</span></a>
    </div>
  </div>
  <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
    <path class="shape-overlays__path"></path>
    <path class="shape-overlays__path"></path>
    <path class="shape-overlays__path"></path>
  </svg>
</div>
<div id="cursorBlob"></div>


<div id="magic-cursor">
<div id="ball">
</div>
</div>
<div id="barba-wrapper">
  <div <?php body_class('barba-container'); ?> data-namespace="<?php if ( is_home() || is_front_page() ) : ?>home<?php else : ?><?php echo $slug; ?><?php endif; ?>">
