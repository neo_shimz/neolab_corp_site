</div><!-- /barba-container -->
</div><!-- /barba-wrapper -->

<div class="pjax_overlay cf">
	<div class="loader"></div>
</div>

<footer class="f__footer" role="contentinfo">
	<div class="f__footer-inner">
		<div class="f__footer-inner__primary">
			<div class="item -logo">
				<a href="<?php echo home_url(); ?>/" class="logo ">
					<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/logo-default.svg" alt="<?php bloginfo('name'); ?>">
				</a>
				<p class="text">
					ネオラボは、IT領域における包括的経営戦略ソリューションを提供する、BusinessとTechのスペシャリスト集団です。
				</p>
			</div>
			<div class="item -menu">
				<div class="menu__area-item">
					<p class="f__title gf1">SOLUTION</p>
					<ul class="f__menu">
						<li>
							<a href="<?php echo home_url(); ?>/solution/" class="">
								<span>ソリューション一覧</span>
							</a>
						</li>
						<li>
							<a href="https://www.calling.fun/" target="_blank" class="">
								<span>calling</span>
								<i class="-new_window"></i>
							</a>
						</li>
						<li>
							<a href="https://hcm-jinjer.com/" target="_blank" class="">
								<span>jinjer</span>
								<i class="-new_window"></i>
							</a>
						</li>
						<li>
							<a href="https://www.enigma.co.jp/" target="_blank" class="">
								<span>enigma</span>
								<i class="-new_window"></i>
							</a>
						</li>
					</ul>
				</div>
				<div class="menu__area-item">
					<p class="f__title gf1">COMPANY</p>
					<ul class="f__menu">
						<li>
							<a href="<?php echo home_url(); ?>/about/" class="">
								<span>ネオラボについて</span>
							</a>
						</li>
						<li>
							<a href="<?php echo home_url(); ?>/company/" class="">
								<span>企業情報</span>
							</a>
						</li>
						<li>
							<a href="<?php echo home_url(); ?>/news/" class="">
								<span>ニュース</span>
							</a>
						</li>
						<li>
							<a href="<?php echo home_url(); ?>/contact/" class="">
								<span>お問い合わせ</span>
							</a>
						</li>
						<li>
							<a href="<?php echo home_url(); ?>/policy/" class="no-barba ">
								<span>プライバシーポリシー</span>
							</a>
						</li>
						<li>
							<a href="https://www.facebook.com/neolabjpve/" target="_blank" class="">
								<span>facebook</span>
								<i class="-new_window"></i>
							</a>
						</li>
					</ul>
				</div>
				<div class="menu__area-item">
					<p class="f__title gf1">RECRUIT</p>
					<ul class="f__menu">
						<li>
							<a href="<?php echo home_url(); ?>/recruit-top/" class="">
								<span>採用情報</span>
							</a>
						</li>
						<li>
							<?php /*
							<a href="<?php echo home_url(); ?>/recruit/new_graduates/">
							*/ ?>
							<a href="https://www.wantedly.com/companies/neo-lab/projects" target="_blank" class="">
								<span>新卒採用</span>
								<i class="-new_window"></i>
							</a>
						</li>
						<li>
							<?php /*
							<a href="<?php echo home_url(); ?>/recruit/mid/">
							*/ ?>
							<a href="https://www.wantedly.com/companies/neo-lab/projects" target="_blank" class="">
								<span>中途採用</span>
								<i class="-new_window"></i>
							</a>
						</li>
						<li>
							<a href="<?php echo home_url(); ?>/interview/" class="">
								<span>社員を知る</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="f__footer-inner__secondary">
		<div class="item -copy">
			<p class="text">
				<i class="icon-copyright"></i>
				<small class="gf1">NEOLAB CO.ltd</small>
			</p>
		</div>
		<div class="item -pagetop">
			<a href="#" data-scroll>
				<i class="icon-arrow_upward"></i>
			</a>
		</div>
	</div>
</footer>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/js/plugin/menu/easings.js"></script>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/js/plugin/menu/style.js"></script>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/js/plugin/inview/jquery.inview.js"></script>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/js/style-dist.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/js/pointer.js"></script>




<?php wp_footer(); ?>
</body>
</html>
