<?php

/**
 * 特集投稿タイプ定義
 */

add_action( 'init', 'create_blog' , 0);

function create_blog() {
	$blog_type = array(
		'label' => 'ブログ',
		'labels' => array(
				'name' => 'ブログ',
				'singular_name' => 'ブログ',
				'add_new_item' => 'ブログを追加',
				'new_item' => 'ブログを追加',
				'view_item' => 'ブログを表示',
				'not_found' => 'ブログは見つかりませんでした',
				'not_found_in_trash' => 'ゴミ箱にブログはありません。',
				'search_items' => 'ブログを検索'
		),
		'public' => true,
		'rewrite' => array(
			'slug' => 'blog',
			'with_front' => false
		),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type( 'blog', $blog_type);



	// カテゴリータクソノミー
	$blog_category_taxonomy = array(
		'hierarchical' => true,
		'label' => 'ブログカテゴリー',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'blog_category'
		),
		'singular_label' => 'blog_category'
	);
	register_taxonomy(
		'blog_category',
		'blog',
		$blog_category_taxonomy
	);



	// キーワードタクソノミー
	$blog_keyword_taxonomy = array(
		'hierarchical' => false,
		'label' => 'ブログキーワード',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'blog_keywords'
		),
		'singular_label' => 'blog_keywords'
	);
	register_taxonomy(
		'blog_keywords',
		'blog',
		$blog_keyword_taxonomy
	);


}
