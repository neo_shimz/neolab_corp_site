<?php
/*
	template Name: プライバシーポリシー
*/
?>

<?php get_template_part('_include/header'); ?>


<main class="m_main__area" role="main">
  <div class="h_sub__header">
    <div id="particle-canvas" class="js_header__canvas"></div>
    <div class="h_sub__header-inner -short">
      <h1 class="h_title">
        <span class="-jp">プライバシーポリシー</span>
        <span class="-en">PRIVACY POLICY</span>
      </h1>
    </div>
  </div>
  <?php wpBreadcrumbs(); ?>
  <div class="m_main__area-inner -policy">
    <div class="cf">
      <?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
    </div>
  </div>
</main>


<?php get_template_part('_include/footer'); ?>
