<?php
/*
	template Name: 新卒採用
*/
?>
<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-recruit'); ?>
<div id="particle-canvas" style="display: none;"></div>

<div class="r_visual__area js-full-window">
  <div class="r_visual__area-inner">
    <ul class="r_css__slider cb-slideshow-new">
      <li></li>
      <li></li>
      <li></li>
    </ul>
    <div class="r_visual__area-description">
      <h1 class="-title">
        <span class="gf1 -en">graduates</span>
        <span class="-jp">新卒採用</span>
      </h1>
      <p class="-description">
        若手社員でも、ビックプロジェクトに参加出来ます。<br>
        先輩社員と一緒に楽しく働きませんか？
      </p>
      <div class="btn_group">
        <a href="<?php echo home_url(); ?>/recruit-top/new/form/" class="op -btn -wave -white">
          <span>新卒採用フォーム</span>
        </a>
        <a href="<?php echo home_url(); ?>/recruit-top/mid/" class="-link">
          <span>中途採用はこちら</span>
        </a>
      </div>
    </div>
    <div class="r_visual__join">
      <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/recruit/join.svg" alt="join us!">
    </div>
  </div>
</div>
<main class="g_main r_new__main" role="main" id="main">
  <section class="r_section">
    <div class="r_section__item r_1">
      <figure class="-img p_1">
        <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/recruit/new/illust_new_01.svg" alt="" width="560">
      </figure>
      <div class="-description">
        <h2>
          新卒からでも<br>
          活躍できる場所
        </h2>
        <p>
          私たちネオラボは新卒の方々へ、裁量を持って仕事ができる環境をお約束します。ネオラボでは、新卒でもプロジェクトマネージャーに抜擢します。プロジェクトマネージャーというポジションは、開発エンジニアの仕事に加えて、社外・社内の他の人との折衝、顧客の求めるシステムが作れているかという見えずらい問題を前にして成果物をだす判断、これらを俯瞰しながらプロジェクト全体の利益率を最大化するという、抽象度と難易度の高い仕事をこなすことが求められます。
        </p>
      </div>
    </div>
    <div class="r_section__item r_2">
      <figure class="-img -img_right">
        <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/recruit/new/illust_new_02.svg" alt="" width="450">
      </figure>
      <div class="-description">
        <h2>
          困難を乗り越えて
        </h2>
        <p>
          若くしてプロジェクトマネージャーとして業務にあたる中で、時に辛いこともあると思いますが、その経験は将来非常に役に立ちます。<br>
          新卒で入った方々には、将来何らかの形でネオラボを離れる方もいらっしゃると思います。それは、独立して会社を作ることかもしれません。
          若くして経営を考えながら開発リーダーをした経験は、そうした場面でも必ず役に立つものだと信じています。<br>
          ネオラボで培った経験をもとに、独立してサービスを作っていける人材を創り出したいと考えています。
        </p>
      </div>
    </div>
  </section>
  <div class="r_section -user">
    <div class="r_section__title-block">
      <h1 class="-title">
        求める人物像について
      </h1>
      <p class="-description">
        このように、ネオラボでは非常に圧縮して成長できる機会と環境を用意したいと考えています。<br>
        この環境で最大限成長できる人、言い換えれば、ネオラボという場所を使い倒せる人を採りたいと考えています。
      </p>
    </div>
    <div class="r_section__title-block">
      <p class="-description_1">
        裁量を与えられた場所で急成長できる人には、<br>
        ３つの要素があると感じております。
      </p>
      <p class="-description_2">
        <span>素直さ、軸、理解力</span>
      </p>
    </div>
    <div>
      <div class="r_section__item r_3">
        <figure class="-img">
          <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/recruit/new/illust_new_03.svg" alt="" width="450">
        </figure>
        <div class="-description">
          <h2>
            素直さ
          </h2>
          <p>
            人は、自分の欠点や失敗を指摘されると反発心を持ってしまうものです。
            反発すること自体は正常なのですが、それが強いと成長する機会を失うことにもなります。
            指摘してくれることを吸収することはあなたの成長そのものです。
            ちなみに、フリーランスではこうした指摘を受ける機会は少なくなります。
            人に指摘してもらえるのは、会社という組織に入るメリットの一つだと考えられるのではないでしょうか。
          </p>
        </div>
      </div>
      <div class="r_section__item r_4">
        <figure class="-img -img_right">
          <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/recruit/new/illust_new_04.svg" alt="" width="450">
        </figure>
        <div class="-description">
          <h2>
            軸
          </h2>
          <p>
            素直さは大事ですが、人から言われたこと全部に「承知しました」とだけ返答している人は、イエスマンになってしまいます。
            自分の軸で物事を考え、一見うまく回っているシステムの中に改善可能な点を指摘することができなければ、マネージャーは務まりません。
            その人の軸は考え方の根幹になるものであり、ここがグラグラしていると悩む事が増え、結果として成長の足かせになってしまいます。
          </p>
        </div>
      </div>
      <div class="r_section__item r_5">
        <figure class="-img">
          <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/recruit/new/illust_new_05.svg" alt="" width="450">
        </figure>
        <div class="-description">
          <h2>
            理解力
          </h2>
          <p>
            １を言われたら５を理解する力が大事です。手取り足取り全て教えてもらいながらスキルを身につけるという方法では、成長の速度に限界があります。
            人から何か言われた際にその裏にあることを理解し、その理解を再度言葉にして表現する能力は、どの仕事でも役に立ちます。
            特にプロジェクトマネージャーなどの上流工程の役職では必須と呼べるものです。
          </p>
        </div>
      </div>
    </div>
    <?php wpBreadcrumbs(); ?>
    <div class="r_form__area">
      <div class="r_form__area-inner">
        <p class="-description">
          将来どんな自分になり、どんな仕事をしたいか。<br>
          ネオラボは、新卒でも活躍出来きワクワク出来る環境があります。
        </p>
        <ul class="btn_group">
          <li>
            <a href="<?php echo home_url(); ?>/recruit-top/new/form/" class="-btn op">
              <span>新卒採用フォーム</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/recruit-top/mid/" class="-link">
              <span>中途採用はこちら</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="r_interview__slider">
    <h3><?php echo the_title(''); ?>インタビュー</h3>
    <div class="r_interview__slider-inner js-interview_slider">
      <?php
      $loop = new WP_Query(array(
        'post_type'      => 'interview',
        'order'        => 'DESC',
        'showposts'     => 5,
        'tax_query' => array(
          array(
            'taxonomy' => 'interview_category',
            'field'    => 'slug',
            'terms'    => 'new', // 新卒のみ表示 new(新卒) or  mid(中途)
          ),
        ),
      ));
      while ($loop->have_posts()) : $loop->the_post(); ?>
        <article class="r_slider__items">
          <a href="<?php the_permalink(); ?>" class="r_slider__items-inner">
            <?php the_post_thumbnail('', array('class' => 'r_slider__img')); ?>
            <div class="r_slider__info">
              <p class="r_user__name"><?php the_title(''); ?></p>
              <!-- <p class="r_user__message"><?php echo get_field('interviewCopy'); ?></p>
                        <p class="r_user__year"><?php echo get_field('interviewJoiningYear'); ?></p> -->
              <p class="r_user__position"><?php echo get_field('interviewPost'); ?></p>
            </div>
          </a>
        </article>
      <?php endwhile;
    wp_reset_query(); ?>
    </div>
  </div>

</main>


<?php RecruitFooterBnner(); ?>
<?php get_template_part('_include/footer'); ?>