
<?php get_template_part('_include/header'); ?>

<?php if(have_posts()):while(have_posts()):the_post(); ?>
<main class="m_main__area" role="main">

	<div class="s_single__main">

		<div class="s_post__head">
			<?php the_category(); ?>
			<time class="update">
				<?php echo get_the_date( 'Y.m.d' ); ?>
				<?php upData(); ?>
			</time>
		</div>
		<div class="s_single__title cf">
			<h1><?php the_title(); ?></h1>
		</div>
		<?php singleSnsBtn(); ?>
		<div class="s_single__thumb">
			<?php if (has_post_thumbnail()) : ?>
				<?php the_post_thumbnail('news-thumbs', array('class' => 'thumb')); ?>
			<?php else : ?>
				<img src="<?php bloginfo('template_url'); ?>/static/assets/img/common/logo-no_thumb.png" alt="<?php the_title(); ?>">
			<?php endif ; ?>
		</div>
		<div class="s_single__content cf">
			<?php the_content(); ?>
		</div>
		<div class="s_post__food">
			<?php the_category(); ?>
			<?php the_tags('<ul class="s_tag"><li>','</li><li>','</li></ul>'); ?>
		</div>
		<div class="">
			<?php singleSnsBtn(); ?>
		</div>
		<div class="s_facebook_area">
			<div class="s_img">
				<?php if (has_post_thumbnail()) : ?>
					<?php the_post_thumbnail('post-thumbs', array('class' => 'thumbs__size -large')); ?>
				<?php else : ?>
					<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/thumbs-not_img-L.png" alt="<?php the_title(); ?>｜イメージ" />
				<?php endif; ?>
			</div>
			<div class="s_btn">
				<p>facebookでも<br />随時配信しています</p>
				<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fneolabjpve%2F&width=114&layout=button_count&action=like&size=large&show_faces=false&share=false&height=21&appId=242333472476268" width="114" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
			</div>
		</div>
		<?php pagerSingle(); ?>
		<?php /*
		<section class="s_recommend__area cf">
			<h2 class="s_sub_title">
				<span>関連記事</span>
			</h2>
			<div class="s_recommend__area-inner">
				<?php
				$post_id = get_the_ID();
				foreach((get_the_category()) as $cat) {
					$cat_id = $cat->cat_ID ;
					break ;
				}
				query_posts(
					array(
						'cat' => $cat_id,
						'showposts' => 3,
						'post__not_in' => array($post_id)
					)
				);
				if(have_posts()) :
					?>
					<?php while (have_posts()) : the_post(); ?>
						<?php postArticle(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
			</div>
		</section>
		*/ ?>
	</div>

</main>
<?php endwhile;endif; ?>

<?php get_template_part('_include/footer'); ?>
