<?php get_template_part('./_blog/header'); ?>

<div class="g_page__area cf">
	<div class="g_main cf">
		<div class="g_main__inner -home">
			<?php
			$loop = new WP_Query(array(
				'post_type'			=> 'blog',
				'order'				=> 'DESC',
			));
			while(have_posts()): the_post();
			?>
				<?php postBlogArticle(); ?>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<?php wpPagenavi(); ?>
	</div>
	<?php get_template_part('./_blog/sidebar'); ?>
</div>


<section class="g_recruit__bnr">
	<div class="g_recruit__bnr-inner">
		<a href="https://www.wantedly.com/companies/neo-lab/projects" target="_blank">
			<p class="g_recruit__title">
				<span class="text_jp">リクルート</span>
				<span class="text_en wfont1">RECRUIT</span>
			</p>
			<p class="g_recruit___description">
				たくさんの業種を随時募集しております。<br />
				お気軽にお問い合わせください。
			</p>
			<div class="g_recruit__icon-arrow icon-arrow_forward"></div>
		</a>
	</div>
</section>


<?php wpBreadcrumbs(); ?>
<?php get_template_part('./_include/inc-sns'); ?>
<?php get_template_part('./_blog/footer'); ?>
