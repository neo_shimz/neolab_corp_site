<?php
/*
	template Name: お問い合わせ
*/
?>

<?php get_template_part('_include/header'); ?>


<main class="m_main__area" role="main">
	<div class="h_sub__header">
    <div id="particle-canvas" class="js_header__canvas"></div>
    <div class="h_sub__header-inner -short">
      <h1 class="h_title">
        <span class="-jp">お問い合わせ</span>
        <span class="-en">CONTACT</span>
      </h1>
    </div>
  </div>
	<?php wpBreadcrumbs(); ?>
	<div class="m_main__area-inner -contact">
		<div class="f_top__description">
			ネオラボに関するご要望・ご質問は、下記のフォームからお気軽にお問合せ下さい。
		</div>
		<div class="m_main__area-body">
			<?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
		</div>
		<div class="f_bottom__description">
			<p>
				弊社「<a href="<?php echo home_url(); ?>/policy/" target="_blank">プライバシーポリシー</a>
				」を必ずご確認・ご同意の上、お問い合わせください。
			</p>
		</div>
	</div>
</main>


<script>
jQuery.event.add(window, "load", function(){
	if($(".-company span").hasClass("error")) {
		$(".-company input").addClass("error");
	}
	if($(".-staff span").hasClass("error")) {
		$(".-staff input").addClass("error");
	}
	if($(".-tel span").hasClass("error")) {
		$(".-tel input").addClass("error");
	}
	if($(".-mail span").hasClass("error")) {
		$(".-mail input").addClass("error");
	}
	if($(".-info span").hasClass("error")) {
		$(".-info textarea").addClass("error");
	}
});
</script>


<?php get_template_part('_include/footer'); ?>
