<?php
/*
template Name: 企業情報
*/
?>
<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-company'); ?>


<main class="m_main__area" role="main">
	<div class="h_sub__header">
		<div id="particle-canvas" class="js_header__canvas"></div>
		<div class="h_sub__header-inner -short">
			<h1 class="h_title">
				<span class="-jp">企業情報</span>
				<span class="-en">COMPANY</span>
			</h1>
		</div>
	</div>
	<?php wpBreadcrumbs(); ?>
	<div class="m_main__area-inner -company">
		<?php companyNav(); ?>
		<header class="company__head-title">
			<h2>
				<strong class="gf1">JAPAN</strong>
				<span>本社(新宿)</span>
			</h2>
		</header>
		<div class="company__contents-area -profile" data-text="TOKYO">
			<div class="company__contents-inner">
				<h3 class="company__contents-title">
					<span class="-en gf1">CORPORATE PROFILE</span>
					<span class="-jp">会社情報</span>
				</h3>
				<section class="company__contents-info">
					<dl>
						<dt>会社名</dt>
						<dd>株式会社ネオラボ</dd>
					</dl>
					<dl>
						<dt>創業</dt>
						<dd>2015年11月</dd>
					</dl>
					<dl>
						<dt>資本金</dt>
						<dd>１億円</dd>
					</dl>
					<dl>
						<dt>事業内容</dt>
						<dd>共同事業開発サービス</dd>
					</dl>
					<dl>
						<dt>本社住所</dt>
						<dd>
							〒160-0023<br />
							<a href="https://goo.gl/maps/opqUsrMVJ6J2" target="_blank">
								東京都新宿区西新宿1-22-2 新宿サンエービル 3F
							</a>
						</dd>
					</dl>
				</section>
			</div>
		</div>
		<div class="company__contents-google">
			<div id="map" class="-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1620.2611462654493!2d139.69490905814357!3d35.6887628950481!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188cd3de06ae41%3A0xa4d7ab180fab7158!2z44CSMTYwLTAwMjMg5p2x5Lqs6YO95paw5a6_5Yy66KW_5paw5a6_77yR5LiB55uu77yS77yS4oiS77ySIOaWsOWuv-OCteODs-OCqOODvOODk-ODqw!5e0!3m2!1sja!2sjp!4v1554981250269!5m2!1sja!2sjp" width="980" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<a class="-btn" href="https://goo.gl/maps/opqUsrMVJ6J2" target="_blank">
				<span>Google Maps</span>
				<i class="icon-filter_none"></i>
			</a>
		</div>
		<p class="company__contents-description">
			新宿駅から徒歩5分の東京本社は、ネオラボのサービスをユーザーに届ける窓口となっています。東京本社では多数のエンジニアやプロジェクトマネージャーのほか、経営部門を含むメンバーが一つの空間に集い、開発・広報・サポートへ取り組んでいます。<br />
			この体制は、ネオラボが一体となって邁進できるようにするだけではなく、ユーザーからのフィードバックを速やかに取り込み、提供しているサービスを高速で改善することを可能にしています。
		</p>
		<div class="company__contents-inner -gallery">
			<h3 class="company__contents-title">
				<span class="-en gf1">CORPORATE GALLERY</span>
				<span class="-jp">会社写真</span>
			</h3>
			<section class="company__contents-gallery">
				<ul class="js-image-gallery">
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-tokyo-1">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/01.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-tokyo-2">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/02.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-tokyo-3">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/03.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-tokyo-4">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/04.jpg" alt="" />
						</a>
					</li>
				</ul>
			</section>
		</div>
	</div>
</main>


<div id="modal-tokyo-1" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/01.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-tokyo-2" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/02.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-tokyo-3" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/03.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-tokyo-4" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/tokyo/04.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
</div>

<?php get_template_part('_include/footer'); ?>
