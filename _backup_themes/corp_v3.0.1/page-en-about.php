<?php
/*
template Name: アバウト｜英語
*/
?>
<?php get_template_part('_include/header'); ?>

<main class="m_main__area" role="main">
	<section class="a_visual">
		<div id="particle-canvas" class="js_header__canvas"></div>
		<div class="a_visual__area">
			<div class="a_visual__area-inner">
				<img class="neolab" src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/about/visual-title.svg" alt="NEOLAB" />
				<div class="a_visual__description">
				</div>
				<?php wpBreadcrumbs(); ?>
			</div>
		</div>
	</section>
	<div class="a_vision__area">
		<section class="a_vision__area-inner -primary">
			<div class="-inner">
				<ul class="a_vision__area-items">
					<li class="cf">
						<p class="-head">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="133px" height="26px">
								<path fill-rule="evenodd" fill="rgb(0, 0, 0)" d="M126.769,25.774 C122.989,25.774 120.923,23.534 120.923,19.614 L120.923,6.872 C120.923,2.952 122.989,0.712 126.769,0.712 C130.549,0.712 132.615,2.952 132.615,6.872 L132.615,19.614 C132.615,23.534 130.549,25.774 126.769,25.774 ZM128.764,6.627 C128.764,4.877 127.994,4.212 126.769,4.212 C125.544,4.212 124.774,4.877 124.774,6.627 L124.774,19.859 C124.774,21.609 125.544,22.274 126.769,22.274 C127.994,22.274 128.764,21.609 128.764,19.859 L128.764,6.627 ZM113.590,21.784 L117.301,21.784 L117.301,25.494 L113.590,25.494 L113.590,21.784 ZM104.630,6.802 L101.655,6.802 L101.655,4.072 C104.630,4.072 105.295,2.637 105.890,0.992 L108.481,0.992 L108.481,25.494 L104.630,25.494 L104.630,6.802 ZM82.509,25.494 L76.453,25.494 L76.453,0.992 L82.264,0.992 C86.254,0.992 87.970,2.847 87.970,6.627 L87.970,7.502 C87.970,10.023 87.200,11.598 85.484,12.403 L85.484,12.473 C87.550,13.278 88.355,15.098 88.355,17.688 L88.355,19.684 C88.355,23.464 86.359,25.494 82.509,25.494 ZM84.119,7.012 C84.119,5.262 83.524,4.492 82.159,4.492 L80.304,4.492 L80.304,10.968 L81.809,10.968 C83.244,10.968 84.119,10.338 84.119,8.377 L84.119,7.012 ZM84.504,17.443 C84.504,15.168 83.769,14.468 82.019,14.468 L80.304,14.468 L80.304,21.994 L82.509,21.994 C83.804,21.994 84.504,21.399 84.504,19.579 L84.504,17.443 ZM68.269,21.049 L63.544,21.049 L62.878,25.494 L59.343,25.494 L63.263,0.992 L68.899,0.992 L72.820,25.494 L68.934,25.494 L68.269,21.049 ZM65.924,5.332 L65.854,5.332 L64.034,17.723 L67.744,17.723 L65.924,5.332 ZM46.450,0.992 L50.301,0.992 L50.301,21.994 L56.637,21.994 L56.637,25.494 L46.450,25.494 L46.450,0.992 ZM36.730,25.774 C32.949,25.774 30.884,23.534 30.884,19.614 L30.884,6.872 C30.884,2.952 32.949,0.712 36.730,0.712 C40.510,0.712 42.575,2.952 42.575,6.872 L42.575,19.614 C42.575,23.534 40.510,25.774 36.730,25.774 ZM38.725,6.627 C38.725,4.877 37.955,4.212 36.730,4.212 C35.505,4.212 34.735,4.877 34.735,6.627 L34.735,19.859 C34.735,21.609 35.505,22.274 36.730,22.274 C37.955,22.274 38.725,21.609 38.725,19.859 L38.725,6.627 ZM17.078,0.992 L27.579,0.992 L27.579,4.492 L20.928,4.492 L20.928,11.318 L26.214,11.318 L26.214,14.818 L20.928,14.818 L20.928,21.994 L27.579,21.994 L27.579,25.494 L17.078,25.494 L17.078,0.992 ZM4.417,7.747 L4.347,7.747 L4.347,25.494 L0.882,25.494 L0.882,0.992 L5.712,0.992 L9.598,15.658 L9.668,15.658 L9.668,0.992 L13.098,0.992 L13.098,25.494 L9.143,25.494 L4.417,7.747 Z" />
							</svg>
						</p>
						<dl>
							<dt>A group of sharp development team
							</dt>
							<dd>
								Neolab was established in November 2015. Established as a development team aimed at in-house system development and cost reduction for the entire group. Business and Tech specialists gathered from inside and outside the group.
							</dd>
						</dl>
					</li>
					<li class="cf">
						<p class="-head">
							<svg xmlns:xlink="http://www.w3.org/1999/xlink" width="134px" height="26px">
								<path fill-rule="evenodd" fill="rgb(0, 0, 0)" d="M127.400,25.774 C123.620,25.774 121.554,23.534 121.554,19.614 L121.554,6.872 C121.554,2.952 123.620,0.712 127.400,0.712 C131.181,0.712 133.246,2.952 133.246,6.872 L133.246,19.614 C133.246,23.534 131.181,25.774 127.400,25.774 ZM129.395,6.627 C129.395,4.877 128.625,4.212 127.400,4.212 C126.175,4.212 125.405,4.877 125.405,6.627 L125.405,19.859 C125.405,21.609 126.175,22.274 127.400,22.274 C128.625,22.274 129.395,21.609 129.395,19.859 L129.395,6.627 ZM114.222,21.784 L117.932,21.784 L117.932,25.494 L114.222,25.494 L114.222,21.784 ZM103.090,21.469 C103.090,21.644 103.090,21.819 103.125,21.994 L110.406,21.994 L110.406,25.494 L99.275,25.494 L99.275,22.484 C99.275,19.754 100.255,17.688 103.125,14.678 C106.066,11.563 106.906,9.637 106.906,7.012 C106.906,4.807 106.136,4.212 104.910,4.212 C103.685,4.212 102.915,4.877 102.915,6.627 L102.915,9.252 L99.275,9.252 L99.275,6.872 C99.275,2.952 101.235,0.712 105.015,0.712 C108.796,0.712 110.756,2.952 110.756,6.872 C110.756,10.128 109.566,12.753 105.996,16.568 C103.720,19.018 103.090,20.174 103.090,21.469 ZM82.509,25.494 L76.453,25.494 L76.453,0.992 L82.264,0.992 C86.254,0.992 87.970,2.847 87.970,6.627 L87.970,7.502 C87.970,10.023 87.200,11.598 85.484,12.403 L85.484,12.473 C87.550,13.278 88.355,15.098 88.355,17.688 L88.355,19.684 C88.355,23.464 86.359,25.494 82.509,25.494 ZM84.119,7.012 C84.119,5.262 83.524,4.492 82.159,4.492 L80.304,4.492 L80.304,10.968 L81.809,10.968 C83.244,10.968 84.119,10.338 84.119,8.377 L84.119,7.012 ZM84.504,17.443 C84.504,15.168 83.769,14.468 82.019,14.468 L80.304,14.468 L80.304,21.994 L82.509,21.994 C83.804,21.994 84.504,21.399 84.504,19.579 L84.504,17.443 ZM68.269,21.049 L63.544,21.049 L62.878,25.494 L59.343,25.494 L63.263,0.992 L68.899,0.992 L72.820,25.494 L68.934,25.494 L68.269,21.049 ZM65.924,5.332 L65.854,5.332 L64.034,17.723 L67.744,17.723 L65.924,5.332 ZM46.450,0.992 L50.301,0.992 L50.301,21.994 L56.637,21.994 L56.637,25.494 L46.450,25.494 L46.450,0.992 ZM36.730,25.774 C32.949,25.774 30.884,23.534 30.884,19.614 L30.884,6.872 C30.884,2.952 32.949,0.712 36.730,0.712 C40.510,0.712 42.575,2.952 42.575,6.872 L42.575,19.614 C42.575,23.534 40.510,25.774 36.730,25.774 ZM38.725,6.627 C38.725,4.877 37.955,4.212 36.730,4.212 C35.505,4.212 34.735,4.877 34.735,6.627 L34.735,19.859 C34.735,21.609 35.505,22.274 36.730,22.274 C37.955,22.274 38.725,21.609 38.725,19.859 L38.725,6.627 ZM17.078,0.992 L27.579,0.992 L27.579,4.492 L20.928,4.492 L20.928,11.318 L26.214,11.318 L26.214,14.818 L20.928,14.818 L20.928,21.994 L27.579,21.994 L27.579,25.494 L17.078,25.494 L17.078,0.992 ZM4.417,7.747 L4.347,7.747 L4.347,25.494 L0.882,25.494 L0.882,0.992 L5.712,0.992 L9.598,15.658 L9.668,15.658 L9.668,0.992 L13.098,0.992 L13.098,25.494 L9.143,25.494 L4.417,7.747 Z" />
							</svg>
						</p>
						<dl>
							<dt>Development firm with new business at its core</dt>
							<dd>
								Shifted to joint business development mainly focusing on new joint ventures with major companies and emerging mega ventures. We have expanded our development base to Vietnam beyond Japan. We have established a unique agile development system optimized for global development while expanding the scale of the organization. The number of employees, which was about 100 at the end of 2016, exceeded 300 by the end of 2018.
							</dd>
						</dl>
					</li>
				</ul>
			</div>
		</section>
		<section class="a_vision__area-inner -secondary">
			<div class="-inner">
				<p class="neolab3">
					<img class="neolab" src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/img/about/neolab3.svg" alt="NEOLAB 3.0" />
				</p>
				<h1 class="-title">
					<span>To Dream Tech Company</span>
					<span>At NEOLAB 3.0, NEOLAB is <br />To evolve into Dream Tech Company</span>
				</h1>
				<div class="-description">
					<p>
						Neolab has built a strong global development system. Companies with advanced image recognition technology will collaborate with our global network. This will enable AI to accelerate drone and other joint business development services and in-house service solutions.
					</p>
					<p>
						Neolab has developed control systems for unmanned drones from an early stage, and has also worked on a variety of services that create additional value with AI technology. As a Dream Tech Company, Neolab will continue to create exciting new solutions for everyone.
					</p>
				</div>
			</div>
		</section>
	</div>
	<div class="a_vision__area -about">
		<section class="-inner">
			<div class="t_title__area">
				<h1 class="-title">
					<span class="-en">VISION</span>
				</h1>
			</div>
			<ul class="a_vision__area-list">
				<li class="item">
					<span class="-number gf1">01</span>
					<h2 class="-title">
						Discover business opportunities
					</h2>
					<p class="-description">
						Neolab is constantly looking for changes in the way technology is used throughout society and seeking the best solutions that can be offered to customers. Neolab is good at IT and related businesses, but will also try to solve unknown problems for which there is no solution in hand.
					</p>
				</li>
				<li class="item">
					<span class="-number gf1">02</span>
					<h2 class="-title">
						Building an organization
					</h2>
					<p class="-description">
						Neolab members take care of the extraordinary. We are looking forward to seeing new colleagues with aspirations. The unitized and agile development system mainly for agile development enables joint business development at any scale.
					</p>
				</li>
				<li class="item">
					<span class="-number gf1">03</span>
					<h2 class="-title">
						Creating customers value
					</h2>
					<p class="-description">
						Neolab provides added value to customers through business tech solutions such as joint business development services, next generation communication platforms, more convenient payment systems, and personnel management software. Neolab is always on the lookout for technological trends around the world and aims to produce the best products that can be implemented at the present time.
					</p>
				</li>
				<li class="item">
					<span class="-number">04</span>
					<h2 class="-title">
						Realize a competitive business structure
					</h2>
					<p class="-description">
						Neolab takes care of your profits. High profits increase development resources and enable business development in new areas. Improving the benefits and leisure time of Neolab members increases the possibility of creating new ideas and creates more exciting products.
					</p>
				</li>
			</ul>
		</section>
	</div>
	<div class="a_executives__area">
		<section class="a_executives__area-inner">
			<div class="t_title__area">
				<h1 class="-title">
					<span class="-en">EXECUTIVES</span>
				</h1>
			</div>
			<div class="a_executives__list">
				<div class="a_executives__item">
					<div class="a_executives__user cf">
						<p class="a_executives__user-name">
							<strong class="-position">Representative Director and Chairman CEO</strong>
							<span class="-name">Keisuke Sakai</span>
						</p>
						<div class="a_executives__user-description">
							<p>
								In college, he tried to be a lawyer and tried a bar exam, but gave up on the way. After joining UFJ Bank (currently Mitsubishi UFJ Bank) as a new graduate in 2001, he moved to Recruit (currently Recruit Holdings) in 2005. Engaged in sales and organizational management for major corporations in the finance and HR fields.
							</p>
							<p>
								In 2013, at the age of 35, he turned into the world of IT startups. Engaged in sales, business development and recruitment at “Lancers” of crowdsourcing and “Team Spirit” of Saas.
							</p>
							<p>
								Joined Neo Career, a comprehensive human resources service company in June 2014. After taking charge of General Manager of Corporate Planning Department, assumed office as Executive Officer in charge of business development in October 2016 (current position).
							</p>
							<p>
								In parallel, “Neolab” was launched in November 2015 as President and CEO. In October 2016, he assumed the position of Chairman and CEO (current position). To the present.
							</p>
						</div>
					</div>
				</div>
				<div class="a_executives__item">
					<div class="a_executives__user cf">
						<p class="a_executives__user-name">
							<strong class="-position">Managing director COO</strong>
							<span class="-name">Tomohiro Okawa</span>
						</p>
						<div class="a_executives__user-description">
							<p>
								During the university, he worked as an intern at a major publishing company, advertising agency, game company, and satellite venture.<br>
								Declined the nomination of the leading companies, 2012 while attending (four-year undergraduate), joined to "Ransazu" was a few start-up companies at that time, Web Service Development Director, Customer Success SV, team leader of the new business development. Engaged as.
							</p>
							<p>
								In 2014, moved to “Neo Career”, a comprehensive human resources service company, and after working as a project manager for a new Web service, was seconded to “Maverick”, a group company that develops Internet advertising and DSP. As manager in charge of business planning and business strategy, he contributed to new business development such as reorganization and realization of business alliances.
							</p>
							<p>
								Participated in launching Neolab after returning to work. In charge of corporate planning, he engaged in a wide range of activities from corporate sales to the construction of back offices for Japanese and Vietnamese subsidiaries. In April 2016, he became Operating Officer COO of Neolab Inc. In October of the same year, he was appointed Managing Director COO. To the present.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>


<?php get_template_part('_include/footer'); ?>