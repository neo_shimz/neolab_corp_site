<?php

/**
 * 特集投稿タイプ定義
 */

add_action( 'init', 'create_interview' , 0);

function create_interview() {
	$interview_type = array(
		'label' => 'インタビュー',
		'labels' => array(
				'name' => 'インタビュー',
				'singular_name' => 'インタビュー',
				'add_new_item' => 'インタビューを追加',
				'new_item' => 'インタビューを追加',
				'view_item' => 'インタビューを表示',
				'not_found' => 'インタビューは見つかりませんでした',
				'not_found_in_trash' => 'ゴミ箱にインタビューはありません。',
				'search_items' => 'インタビューを検索'
		),
		'public' => true,
		'rewrite' => array(
			'slug' => 'interview',
			'with_front' => false
		),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type( 'interview', $interview_type);



	// カテゴリータクソノミー
	$interview_category_taxonomy = array(
		'hierarchical' => true,
		'label' => 'インタビューカテゴリー',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'interview_category'
		),
		'singular_label' => 'interview_category'
	);
	register_taxonomy(
		'interview_category',
		'interview',
		$interview_category_taxonomy
	);



	// キーワードタクソノミー
	// $interview_keyword_taxonomy = array(
	// 	'hierarchical' => false,
	// 	'label' => 'インタビューキーワード',
	// 	'show_ui' => true,
	// 	'query_var' => true,
	// 	'rewrite' => array(
	// 		'slug' => 'interview_keywords'
	// 	),
	// 	'singular_label' => 'interview_keywords'
	// );
	// register_taxonomy(
	// 	'interview_keywords',
	// 	'interview',
	// 	$interview_keyword_taxonomy
	// );


}
