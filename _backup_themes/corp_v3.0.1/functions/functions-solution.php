<?php

/**
 * 特集投稿タイプ定義
 */

add_action( 'init', 'create_solution' , 0);

function create_solution() {
	$solution_type = array(
		'label' => 'ソリューション',
		'labels' => array(
				'name' => 'ソリューション',
				'singular_name' => 'ソリューション',
				'add_new_item' => 'ソリューションを追加',
				'new_item' => 'ソリューションを追加',
				'view_item' => 'ソリューションを表示',
				'not_found' => 'ソリューションは見つかりませんでした',
				'not_found_in_trash' => 'ゴミ箱にソリューションはありません。',
				'search_items' => 'ソリューションを検索'
		),
		'public' => true,
		'rewrite' => array(
			'slug' => 'solution',
			'with_front' => false
		),
		'has_archive' => true,
		'show_ui' => true,
		'query_var' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 6,
		'can-export' => true,
		'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type( 'solution', $solution_type);



	// カテゴリータクソノミー
	$solution_category_taxonomy = array(
		'hierarchical' => true,
		'label' => 'ソリューションカテゴリー',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'solution_category'
		),
		'singular_label' => 'solution_category'
	);
	register_taxonomy(
		'solution_category',
		'solution',
		$solution_category_taxonomy
	);



	// キーワードタクソノミー
	// $solution_keyword_taxonomy = array(
	// 	'hierarchical' => false,
	// 	'label' => 'ソリューションキーワード',
	// 	'show_ui' => true,
	// 	'query_var' => true,
	// 	'rewrite' => array(
	// 		'slug' => 'solution_keywords'
	// 	),
	// 	'singular_label' => 'solution_keywords'
	// );
	// register_taxonomy(
	// 	'solution_keywords',
	// 	'solution',
	// 	$solution_keyword_taxonomy
	// );


}
