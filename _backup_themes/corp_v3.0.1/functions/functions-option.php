<?php

// バージョン自動更新の無効化
add_filter('automatic_updater_disabled', '__return_true');
// END


// jQuery 読み込み
// http://www.webdesignleaves.com/wp/wordpress/952/
function add_my_scripts() {
  if(is_admin()) return;
  wp_deregister_script( 'jquery');
  wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', false);
}
add_action('wp_enqueue_scripts', 'add_my_scripts');
// END


// アップロードした画像のwidth height 削除
function remove_width_attribute( $html ) {
  $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
  return $html;
}
add_filter( "post_thumbnail_html", "remove_width_attribute", 10 );
add_filter( "image_send_to_editor", "remove_width_attribute", 10 );
// END


// サムネイルの有効化
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
	add_image_size('interview-thumbs_square_md', 400, 400, true);
  add_image_size('interview-thumbs_md', 1000, 700, true);
  add_image_size('news-thumbs', 984, 608, true);
  add_image_size('client-logo-thumbs', 200, 200, true);
  add_image_size('post-thumbs', 900, 562, false); // 実際のサイズ 600 370
};
// END


// classの付与
function bodyAddClass () { ?>
  <?php if ( is_home() || is_front_page() ) : ?>top<?php else : ?>lower<?php endif; ?> <?php echo esc_attr( $post->post_name ); ?>
<?php }
// END


// 1件目の記事取得
function isFirst(){
  global $wp_query;
  return ($wp_query->current_post === 0);
}
// END


// サイトタイトル
function siteTitle() {
global $page, $paged;
wp_title( '|', true, 'right' );
bloginfo( 'name' );
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
	echo " | $site_description";
if ( $paged >= 2 || $page >= 2 )
	echo ' | ' . sprintf( __( 'Page %s', 'enigma' ), max( $paged, $page ) );
}
// END


// いらないメタタグ削除
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );	//絵文字
remove_action( 'wp_print_styles', 'print_emoji_styles' );		//絵文字
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');
// END


// Gutenberg（ブロックエディタ) css の読み込み無効
add_action( 'wp_enqueue_scripts', 'remove_block_library_style' );
function remove_block_library_style() {
  wp_dequeue_style( 'wp-block-library' );
}
// END


// USERAGENT
function is_mobile(){
	$useragents = array(
	'iPhone', // iPhone
	'iPod', // iPod touch
	'Android.*Mobile', // 1.5+ Android *** Only mobile
	'Windows.*Phone', // *** Windows Phone
	'dream', // Pre 1.5 Android
	'CUPCAKE', // 1.5+ Android
	'blackberry9500', // Storm
	'blackberry9530', // Storm
	'blackberry9520', // Storm v2
	'blackberry9550', // Storm v2
	'blackberry9800', // Torch
	'webOS', // Palm Pre Experimental
	'incognito', // Other iPhone browser
	'webmate' // Other iPhone browser
	);
	$pattern = '/'.implode('|', $useragents).'/i';
	return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}
// END


// 日付にUPDATA 日を表示
function upData() {
	$days			= 7;
	$daysInt 		= ($days - 1) * 86400;
	$today 			= time();
	$entry 			= get_the_time('U');
	$dayago 		= $today - $entry;
	if ( $dayago < $daysInt ) {
		echo '<i class="new">NEW</i>';
	}
}
// END


// ページャー 次へ 前へ
function pagerSingle() { ?>
	<nav class="s_prev_next cf">
	<?php
		$prevpost = get_adjacent_post(false, '', true); //前の記事
		$nextpost = get_adjacent_post(false, '', false); //次の記事
		if( $prevpost or $nextpost ){ //前の記事、次の記事いずれか存在しているとき
	?>
	<?php
		if ( $prevpost ) {
			//前の記事が存在しているとき
			echo '<a href="' . get_permalink($prevpost->ID) . '" class="prev cf">
			<div class="thumbnail">' . get_the_post_thumbnail($prevpost->ID, array(150,150)) . '</div>
			<p>' . mb_strimwidth(get_the_title($prevpost->ID), 0, 58, "…", "UTF-8") . '</p></a>';
		} else {
			//前の記事が存在しないとき
			echo '<div class="no_post -prev"></div>';
		}
		if ( $nextpost ) {
			//次の記事が存在しているとき
			echo '<a href="' . get_permalink($nextpost->ID) . '" class="next cf">
			<div class="thumbnail">' . get_the_post_thumbnail($nextpost->ID, array(150,150)) . '</div>
			<p>'. mb_strimwidth(get_the_title($nextpost->ID), 0, 58, "…", "UTF-8") . '</p></a>';
		} else {
			//次の記事が存在しないとき
			echo '<div class="no_post -next"></div>';
		}
	?>
	<?php } ?>
	</nav>
<?php }
// END


// 詳細ページのURLに、/newsを追加
function add_article_post_permalink( $permalink ) {
	$permalink = '/news' . $permalink;
	return $permalink;
}
add_filter( 'pre_post_link', 'add_article_post_permalink' );

function add_article_post_rewrite_rules( $post_rewrite ) {
	$return_rule = array();
	foreach ( $post_rewrite as $regex => $rewrite ) {
		$return_rule['news/' . $regex] = $rewrite;
	}
	return $return_rule;
	}
add_filter( 'post_rewrite_rules', 'add_article_post_rewrite_rules' );
// END


//概要（抜粋）の文字数調整
function my_excerpt_length($length) {
  return 80;
}
add_filter('excerpt_length', 'my_excerpt_length');
// END


//概要（抜粋）の省略文字
function my_excerpt_more($more) {
  return '…';
}
add_filter('excerpt_more', 'my_excerpt_more');
// END




// 詳細 SNS BTN
function singleSnsBtn() { ?>
	<div class="social__btn cf js-social__pjax">
		<div class="social__btn-items -facebook cf">
      <div class="fb-like"
        data-href="<?php the_permalink(); ?>"
        data-layout="button_count"
        data-action="like"
        data-size="large"
        data-show-faces="false"
        data-share="false">
      </div>
		</div>
    <div class="social__btn-items -facebook_share cf">
      <a href="//www.facebook.com/sharer.php?src=bm&u=<?php echo urlencode(get_permalink()); ?>&t=<?php echo urlencode(the_title("","",0)); ?>" target="_blank">
        <span>シェア</span>
      </a>
		</div>
    <div class="social__btn-items -twitter cf">
			<a class="twitter" href="//twitter.com/intent/tweet?text=<?php echo urlencode(the_title("","",0)); ?>&amp;<?php echo urlencode(get_permalink()); ?>&amp;url=<?php echo urlencode(get_permalink()); ?>" target="_blank" title="Twitterでシェアする">
				<i class="icon-twitter"></i>
				<span>ツイート</span>
			</a>
		</div>
		<div class="social__btn-items -pocket cf">
			<a class="pocket" href="//getpocket.com/edit?url=<?php echo urlencode(get_permalink()); ?>" target="_blank" title="Pocketであとで読む">
				<i class="icon-pocket"></i>
				<span>Pocket</span>
			</a>
		</div>
		<div class="social__btn-items -feedly cf">
			<a class="feedly" href="//cloud.feedly.com/#subscription%2Ffeed%2F<?php echo urlencode(bloginfo('rss2_url')); ?>" target="_blank" title="Feedlyで購読する">
				<i class="icon-feedly"></i>
				<span>Feedly</span>
			</a>
		</div>
		<div class="social__btn-items -hatena cf">
			<a class="hatena" href="//b.hatena.ne.jp/add?mode=confirm&amp;url=<?php echo urlencode(get_permalink()); ?>&amp;title=<?php echo urlencode(the_title("","",0)); ?>" target="_blank" data-hatena-bookmark-title="<?php the_permalink(); ?>" title="このエントリーをはてなブックマークに追加する">
				<i class="icon-hatenabookmark"></i>
				<span>はてブ</span>
			</a>
		</div>
		<div class="social__btn-items -line cf">
			<a class="line" href="//timeline.line.me/social-plugin/share?url=<?php echo urlencode(get_permalink()); ?>" target="_blank" title="LINEでシェアする">
				<i class="icon-line"></i>
				<span>LINE</span>
			</a>
		</div>
	</div>
<?php }
// END




// -------------------------------------------------------------------------------------
// admin 画面
// -------------------------------------------------------------------------------------

// 日本語のスラッグ名を自動変更
function auto_post_slug( $slug, $post_ID, $post_status, $post_type ) {
	if ( preg_match( '/(%[0-9a-f]{2})+/', $slug ) ) {
		$slug = utf8_uri_encode( 'page' ) . '-' . $post_ID;
	}
	return $slug;
}
add_filter( 'wp_unique_post_slug', 'auto_post_slug', 10, 4  );
// END


// 投稿一覧 マーマリンク表示
function add_custom_column( $defaults ) {
  $defaults['permalink'] = 'パーマリンク';
  return $defaults;
}
add_filter('manage_posts_columns', 'add_custom_column');

function add_custom_column_id($column_name, $id) {
  if($column_name == 'permalink'){
    echo get_permalink();
  }
}
add_action('manage_posts_custom_column', 'add_custom_column_id', 10, 2);


function manage_pages_columns($columns) {
    $columns['permalink'] = "パーマリンク";
    return $columns;
}
function add_pages_column($column_name, $post_id) {
    if ( 'permalink' == $column_name) {
        $plink = get_permalink($post_id);
    }
    if ( isset($plink) && $plink ) {
        echo "<a href='$plink' target='_blank'>$plink</a>";
    } else {
        echo __('None');
    }
}
add_filter( 'manage_pages_columns', 'manage_pages_columns' );
add_action( 'manage_pages_custom_column', 'add_pages_column', 10, 2 );
// END


// フィルターをかける
function _what_is_eyecatch($content, $post_id){
   $post = get_post($post_id);
   // もし投稿のアイキャッチにだけ表示したいとかであれば、
   if('interview' == $post->post_type);
   return $content.
    '<p class="description">インタビュー：1980px × 980px</p>' .
    '<p class="description">クライアントロゴ：200px × auto</p>';
}
add_filter('admin_post_thumbnail_html', '_what_is_eyecatch', 10, 2);
// END





// -------------------------------------------------------------------------------------
// ここから下は、プラグイン周りのfunction
// -------------------------------------------------------------------------------------

// パンくず
function wpBreadcrumbs() { ?>
  <?php if(function_exists('bcn_display')) : ?>
    <div class="g_breadcrumbs cf">
    	<div class="g_breadcrumbs__inner cf">
    		 <?php bcn_display(); ?>
    	</div>
    </div>
  <?php endif; ?>
<?php }
// END


// Pagenavi
function wpPagenavi() { ?>
<div class="g_pagenavi">
  <div class="g_pagenavi__inner">
    <?php wp_pagenavi(); ?>
  </div>
</div>
<?php }
// END
