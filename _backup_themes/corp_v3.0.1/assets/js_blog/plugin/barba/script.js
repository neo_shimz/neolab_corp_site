
(function() {

    var init = function () {
        // イベント処理
        events();

        // Views
        Top.init();

        // Barba.js 実行
        Barba.Pjax.start();
    };

    var events = function () {
        //
        // リンククリック後にCSSアニメーション用のクラスを追加
        //
        Barba.Dispatcher.on('linkClicked', function(HTMLElement, MouseEvent) {
            document.body.classList.add('is-transition-start');
            $('html,body').animate({scrollTop:0},'500');
            $('body').addClass('jsStart');
        });

        //
        // ページ読み込み後にCSSアニメーション用のクラスを削除
        //
        Barba.Dispatcher.on('transitionCompleted', function(HTMLElement, MouseEvent) {
            document.body.classList.remove('is-transition-start');
            $('body').removeClass('jsStart');
        });

        //
        // 各ページごとに処理を実行したい場合
        //
        Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container) {

            switch (currentStatus.namespace) {
            case 'home':
                break;
            }
        });

        Barba.Dispatcher.on( 'newPageReady', function( currentStatus, oldStatus, container, newPageRawHTML ) {
          if ( Barba.HistoryManager.history.length === 1 ) { // ファーストビュー
              return; // この時に更新は必要ありません
          }
          // jquery-pjaxから借りた
          var $newPageHead = $( '<head />' ).html(
              $.parseHTML(
                  newPageRawHTML.match( /<head[^>]*>([\s\S.]*)<\/head>/i )[ 0 ],
                  document,
                  true
              )
          );
          var headTags = [ // 更新するタグ
              "meta[name='keywords']",
              "meta[name='description']",
              "meta[property^='og']",
              "meta[name^='twitter']",
              "meta[itemprop]",
              "link[itemprop]",
              "link[rel='prev']",
              "link[rel='next']",
              "link[rel='canonical']",
              "link[rel='alternate']"
          ].join( ',' );
          $( 'head' ).find( headTags ).remove(); // タグを削除する
          $newPageHead.find( headTags ).appendTo( 'head' ); // タグを追加する
        });

        // Googleアナリティクスに情報を送る
        Barba.Dispatcher.on('initStateChange', function() {
          if (typeof ga === 'function') {
            ga('send', 'pageview', window.location.pathname.replace(/^\/?/, '/') + window.location.search);
          }
        });
    };


    var Top = Barba.BaseView.extend({
        namespace: 'single',
        onEnter: function () {
            // 新しいコンテナの準備が完了したとき
            // console.log('準備完了');

            //Facebook
            FB.XFBML.parse();
        },
        onEnterCompleted: function () {
            // 準備されたコンテナへの遷移が完了したとき
            // console.log('遷移完了');
        },
        onLeave: function () {
            // 新しいページへの遷移が開始されたとき
            // console.log('遷移開始');
        },
        onLeaveCompleted: function () {
            // 遷移が完了し、古いコンテナがDOMから削除されたとき
            // console.log('遷移完了');
        }
    });

    init();


})();
