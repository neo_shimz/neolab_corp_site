<aside class="g_sidebar cf">
  <div class="g_sidebar__inner">
    <div class="g_sidebar__inner-items">
      <h3 class="g_title__dot">
        <span>最新記事</span>
      </h3>
      <section class="g_sidebar__inner-conts cf">
        <?php
          $loop = new WP_Query(array(
            'posts_per_page'  => '6',
            'post_type'       => 'blog',
            'order'           => 'DESC',
          ));
          while ($loop->have_posts()) : $loop->the_post();
        ?>
          <?php postBlogArticle(); ?>
        <?php endwhile; ?>
      </section>
    </div>
  </div>
</aside>
