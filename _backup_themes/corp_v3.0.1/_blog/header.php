<?php
  session_start();
  get_template_part('_include/inc-tag');
  get_template_part('_include/inc-post');
  $page = get_post( get_the_ID() );
  $slug = basename(get_permalink());
  $share_url   = get_permalink();
  $share_title = get_the_title();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php wp_enqueue_script('jquery'); ?>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="initial-scale=1.0, width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php siteTitle(); ?></title>
<link rel="shortcut icon" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/favicons/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/favicons/android-chrome-256x256.png">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/font/icomoon/style.css">
<link rel="stylesheet" href="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/css/blog.css">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:500,700" rel="stylesheet">
<?php wp_head(); ?>
<?php tagGTM(); ?>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.2&appId=214224325296092&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<?php tagGTM_Noscript(); ?>

<?php
  if (!isset($_SESSION["visited"])){
    //print('初回の訪問です。セッションを開始します。');
    jsSplash();
    $_SESSION["visited"] = 1;
  }else{
    $visited = $_SESSION["visited"];
    $visited++;
    //print('訪問回数は'.$visited.'です。<br>');
    $_SESSION["visited"] = $visited;
  }
?>
<?php function jsSplash() { ?>
  <div class="splash" id="splash_load">
    <div class="splash__inner">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="414px" height="86.2px" viewBox="0 0 414 86.2" style="enable-background:new 0 0 414 86.2;" xml:space="preserve">
        <path class="st1" d="M10,24.3v54.8H0V0l61.5,61.9V7.1h10v79.1L10,24.3z M190.7,85.2c-10.2,0-20-3.7-27.6-10.5
      	c-7.6-6.8-12.5-16.2-13.8-26.5H89v20.9h59.5v10.1H79v-72h69.5v10.1H89v20.9h60.3c1.3-10.2,6.2-19.7,13.8-26.5
      	c7.6-6.8,17.5-10.6,27.6-10.5c23,0,41.7,18.9,41.7,42S213.7,85.2,190.7,85.2z M159.4,48.2c1.2,7.9,5.4,15.1,11.6,20.1
      	s14.1,7.4,22,6.7c7.9-0.6,15.3-4.3,20.6-10.2c5.4-5.9,8.4-13.6,8.4-21.7c0-8-3-15.8-8.4-21.7c-5.4-5.9-12.7-9.5-20.6-10.2
      	c-7.9-0.6-15.7,1.8-22,6.7s-10.4,12.1-11.6,20.1h19.3c1.1-2.8,3.2-5.1,5.8-6.5c2.6-1.4,5.7-1.9,8.6-1.3c2.9,0.6,5.6,2.2,7.5,4.5
      	c1.9,2.3,2.9,5.3,2.9,8.3c0,3-1,6-2.9,8.3c-1.9,2.3-4.5,4-7.5,4.5c-2.9,0.6-6,0.1-8.6-1.3c-2.6-1.4-4.7-3.7-5.8-6.5H159.4z
      	 M350.7,79.1L323,23.4l-27.7,55.7h-55.1v-72h10v61.9h39L323,0.9l33.9,68.2h32.7c1.9,0,3.8-0.3,5.5-1.1c1.8-0.7,3.3-1.8,4.7-3.1
      	c1.3-1.3,2.4-3,3.1-4.7s1.1-3.7,1.1-5.6c0-1.9-0.4-3.8-1.1-5.6c-0.7-1.8-1.8-3.4-3.1-4.7c-1.3-1.4-2.9-2.4-4.7-3.1
      	c-1.8-0.7-3.6-1.1-5.5-1.1h-2.9V30h2.9c0.8,0,1.6-0.2,2.4-0.5c0.8-0.3,1.5-0.8,2-1.4c0.6-0.6,1-1.3,1.4-2.1c0.3-0.8,0.5-1.6,0.5-2.4
      	s-0.2-1.7-0.5-2.4s-0.8-1.5-1.4-2.1c-0.6-0.6-1.3-1.1-2-1.4c-0.8-0.3-1.6-0.5-2.4-0.5h-21.2V7.1h21.2c3,0,6,0.8,8.6,2.4
      	c2.6,1.6,4.7,4,6,6.7c1.3,2.8,1.9,5.9,1.6,9c-0.3,3.1-1.5,6-3.4,8.4c3.5,2.2,6.4,5.3,8.5,8.9c2,3.7,3.1,7.8,3.1,12
      	c0,6.5-2.6,12.8-7.2,17.4c-4.6,4.6-10.8,7.2-17.2,7.2H350.7z"/>
      </svg>
    </div>
  </div>
  <script>
  $(function() {
    $('#splash_load').fadeIn().queue( function() {
      setTimeout(function() {
        $('#splash_load').dequeue();
      }, 3000);
    });
    $('#splash_load').fadeOut();
  });
  </script>
<?php } ?>


<div class="sp_header">
  <div class="sp_header__inner drawr-nav-main">
    <div class="nav-head cf">
      <a href="<?php echo home_url(); ?>/" class="no-barba sp_logo">
        <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/logo-default.svg" alt="<?php echo bloginfo( 'name' ); ?>" />
      </a>
      <div class="nav-toggle cf">
        <div class="button-container cf">
          <div class="shape-top">
            <span class="svg svg-shape-menu">
              <svg version="1.1" id="svg2-Calque_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="46px" height="46px" xml:space="preserve">
                <path fill="#FFFFFF" stroke="#FFFFFF" stroke-miterlimit="10" d="M0.084,0.5c0,0,182.218,31.113,283.562,78.687c102.534,48.133,171.35,55.421,216.438,55.421s113.904-7.288,216.438-55.421C817.866,31.613,1000.084,0.5,1000.084,0.5H0.084z"></path>
              </svg>
            </span>
          </div>
          <div class="shape has-hover"></div>
          <div class="lines open">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
          </div>
          <div class="lines close">
            <div class="line"></div>
            <div class="line"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="nav-body cf">
      <div class="nav-inner cf">
        <ul class="cf">
          <?php
            $terms = get_terms('blog_category');
            foreach ( $terms as $term ) {
              echo '<li class="items"><a class="no-barba" href="'.get_term_link($term).'">'.$term->name.'</a></li>';
            }
          ?>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="g_page__wrap cf">
  <header role="banner" class="g_header cf <?php if ( !is_home() && !is_front_page() ) : ?>lower<?php endif; ?>" data-scroll-header>
    <div class="g_header__inner cf">
      <div class="g_logo">
        <a href="<?php echo home_url(); ?>/" class="g_logo__inner no-barba op">
          <div class="g_logo__image">
            <img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/common/logo-default.svg" alt="<?php echo bloginfo( 'name' ); ?>" />
          </div>
          <span class="g_logo__text wfont1">NEOLAB BLOG</span>
        </a>
      </div>
      <nav class="g_nav">
        <ul class="g_nav__inner -menu js-menu-current">
          <li class="items">
            <a href="<?php echo home_url(); ?>/blog/">
              <span>ブログトップ</span>
            </a>
          </li>
          <?php
            $terms = get_terms('blog_category');
            foreach ( $terms as $term ) {
              echo '<li class="items"><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
            }
          ?>
        </ul>
        <ul class="g_nav__inner -sub">
          <li>
            <a href="<?php echo home_url(); ?>/recruit-top/" class="no-barba">
              <span>採用情報</span>
            </a>
          </li>
          <li>
            <a href="<?php echo home_url(); ?>/contact/" class="no-barba">
              <span>お問い合わせ</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </header>
  <main id="barba-wrapper" class="g_barba__main cf" role="main">
    <div <?php body_class('barba-container'); ?> data-namespace="<?php if ( is_home() || is_front_page() ) : ?>top<?php endif; ?><?php if ( is_single() ) : ?>single<?php endif; ?>">
      <div class="g_barba__main-inner">
