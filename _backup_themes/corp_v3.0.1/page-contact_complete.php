<?php
/*
	template Name: お問い合わせ - 完了
*/
?>

<?php get_template_part('_include/header'); ?>


<main class="m_main__area" role="main">
	<div class="h_sub__header">
    <div id="particle-canvas" class="js_header__canvas"></div>
    <div class="h_sub__header-inner -short">
      <h1 class="h_title">
        <span class="-jp">お問い合わせ</span>
        <span class="-en">CONTACT</span>
      </h1>
    </div>
  </div>
	<?php wpBreadcrumbs(); ?>
	<div class="m_main__area-inner -contact">
		<div class="m_main__area-body">
			<?php if (have_posts()) while(have_posts()) : the_post(); the_content(); endwhile; ?>
		</div>
		<div class="f_complete__body">
			<h3>お問合せいただき誠に有り難う御座いました</h3>
			<p>
				ご記入を頂きましたご連絡先に担当のものより近日中にご連絡をさせて頂きます。<br>
				大変恐縮で御座いますが少々お待ち頂けますと幸いで御座います。
			</p>
			<div class="btns">
				<a href="<?php echo home_url(); ?>/" class="items -black op">
					<span>TOPに戻る</span>
				</a>
			</div>
		</div>
	</div>
</main>


<?php get_template_part('_include/footer'); ?>
