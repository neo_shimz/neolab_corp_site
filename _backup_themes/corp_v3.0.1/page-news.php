<?php
/*
	template Name: ニュース
*/
?>

<?php get_template_part('_include/header'); ?>


<main class="m_main__area" role="main">
	<div class="h_sub__header">
    <div id="particle-canvas" class="js_header__canvas"></div>
    <div class="h_sub__header-inner -short">
      <h1 class="h_title">
        <span class="-jp">ニュース</span>
        <span class="-en">NEWS</span>
      </h1>
    </div>
  </div>
	<?php wpBreadcrumbs(); ?>
	<div class="m_post__archive -news">
		<div class="m_post__archive-inner">
			<?php query_posts('paged='.$paged); ?>
			<?php if(have_posts()): while ( have_posts() ) : the_post(); ?>
				<?php newsPost(); ?>
			<?php endwhile; endif; ?>
			<?php wpPagenavi(); ?>
		</div>
	</div>
</main>


<?php get_template_part('_include/footer'); ?>
