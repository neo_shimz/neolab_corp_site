<?php

add_action('after_setup_theme', 'load_custom_functions');
function load_custom_functions() {
	$custom_funcsions_path = dirname(__FILE__);


	// オプション
	require_once("{$custom_funcsions_path}/functions/functions-option.php");

	// ブログ
	require_once("{$custom_funcsions_path}/functions/functions-blog.php");
	require_once("{$custom_funcsions_path}/functions/functions-amp_css.php");


	// インタビュー
	require_once("{$custom_funcsions_path}/functions/functions-interview.php");

	// 求人
	require_once("{$custom_funcsions_path}/functions/functions-recruit.php");

	// クライアント
	require_once("{$custom_funcsions_path}/functions/functions-client.php");

	// ソリューション
	require_once("{$custom_funcsions_path}/functions/functions-solution.php");



}
