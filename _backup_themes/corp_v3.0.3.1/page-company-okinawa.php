<?php
/*
template Name: 企業情報 - 沖縄
*/
?>

<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-company'); ?>


<main class="m_main__area" role="main">
	<div class="h_sub__header">
		<div id="particle-canvas" class="js_header__canvas"></div>
		<div class="h_sub__header-inner -short">
			<h1 class="h_title">
				<span class="-jp">企業情報</span>
				<span class="-en">COMPANY</span>
			</h1>
		</div>
	</div>
	<?php wpBreadcrumbs(); ?>
	<div class="m_main__area-inner -company">
		<?php companyNav(); ?>
		<header class="company__head-title">
			<h2>
				<strong class="gf1">JAPAN</strong>
				<span><?php echo the_title(''); ?></span>
			</h2>
		</header>
		<div class="company__contents-area -profile" data-text="OKINAWA">
			<div class="company__contents-inner">
				<h3 class="company__contents-title">
					<span class="-en gf1">CORPORATE PROFILE</span>
					<span class="-jp">会社情報</span>
				</h3>
				<section class="company__contents-info">
					<dl>
						<dt><?php echo the_title(''); ?></dt>
						<dd>
							〒900-0015<br />
							<a href="https://goo.gl/maps/WypM1cC8BXE2" target="_blank">
								那覇市久茂地2丁目22-12 久茂地UFビル4階
							</a>
						</dd>
					</dl>
				</section>
			</div>
		</div>
		<div class="company__contents-google">
			<div id="map" class="-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7158.564071222831!2d127.682622!3d26.220024!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34e56978ed160bd1%3A0x5193f90bdab5c81a!2z44CSOTAwLTAwMTUg5rKW57iE55yM6YKj6KaH5biC5LmF6IyC5Zyw77yS5LiB55uu77yS77yS4oiS77yR77yS!5e0!3m2!1sja!2sjp!4v1554981686458!5m2!1sja!2sjp" width="980" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<a class="-btn" href="https://goo.gl/maps/WypM1cC8BXE2" target="_blank">
				<span>Google Maps</span>
				<i class="icon-filter_none"></i>
			</a>
		</div>
		<p class="company__contents-description">
			沖縄県那覇市にネオラボは開発拠点を構えています。開発エンジニア、デザイナーなど多彩なメンバーが在籍しており、多くのメンバーがリモートワークで働いています。オフィスは那覇の中心部、国際通りから徒歩10分の場所に位置しています。沖縄の良好な気候、数十分で行ける抜群に綺麗な海はもちろん、美栄橋周辺にある那覇一番のグルメ街など、充実している周辺環境はエンジニアの生産性を引き出します。
		</p>
		<div class="company__contents-inner -gallery">
			<h3 class="company__contents-title">
				<span class="-en gf1">CORPORATE GALLERY</span>
				<span class="-jp">会社写真</span>
			</h3>
			<section class="company__contents-gallery">
				<ul class="js-image-gallery">
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-okinawa-1">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/01.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-okinawa-2">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/02.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-okinawa-3">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/03.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-okinawa-4">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/04.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-okinawa-5">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/05.jpg" alt="" />
						</a>
					</li>
				</ul>
			</section>
		</div>
	</div>
</main>


<div id="modal-okinawa-1" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/01.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-okinawa-2" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/02.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-okinawa-3" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/03.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-okinawa-4" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/04.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-okinawa-5" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/okinawa/05.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>

<?php get_template_part('_include/footer'); ?>
