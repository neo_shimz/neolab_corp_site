<?php
/*
template Name: 企業情報 - ベトナム ダナン
*/
?>

<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-company'); ?>


<main class="m_main__area" role="main">
	<div class="h_sub__header">
		<div id="particle-canvas" class="js_header__canvas"></div>
		<div class="h_sub__header-inner -short">
			<h1 class="h_title">
				<span class="-jp">企業情報</span>
				<span class="-en">COMPANY</span>
			</h1>
		</div>
	</div>
	<?php wpBreadcrumbs(); ?>
	<div class="m_main__area-inner -company">
		<?php companyNav(); ?>
		<header class="company__head-title">
			<h2>
				<strong class="gf1">VIETNAM</strong>
				<span><?php echo the_title(''); ?></span>
			</h2>
		</header>
		<div class="company__contents-area -profile" data-text="DANANG">
			<div class="company__contents-inner">
				<h3 class="company__contents-title">
					<span class="-en gf1">CORPORATE PROFILE</span>
					<span class="-jp">会社情報</span>
				</h3>
				<section class="company__contents-info">
					<dl>
						<dt><?php echo the_title(''); ?></dt>
						<dd>
							<a href="https://goo.gl/maps/316CLAUK1VT2" target="_blank">
								1,2,3 Floor, 344, 2-9 St., Hai Chau Dist., Da Nang, Việt Nam
							</a>
						</dd>
					</dl>
				</section>
			</div>
		</div>
		<div class="company__contents-google">
			<div id="map" class="-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7668.902642295656!2d108.222758!3d16.042053!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314219c27ccfa917%3A0x4c4a88edd6fc7ebc!2zMzQ0IDIgVGjDoW5nIDksIEhvw6AgQ8aw4budbmcgQuG6r2MsIEjhuqNpIENow6J1LCDEkMOgIE7hurVuZyA1NTAwMDAg44OZ44OI44OK44Og!5e0!3m2!1sja!2sjp!4v1554981756180!5m2!1sja!2sjp" width="980" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<a class="-btn" href="https://goo.gl/maps/316CLAUK1VT2" target="_blank">
				<span>Google Maps</span>
				<i class="icon-filter_none"></i>
			</a>
		</div>
		<p class="company__contents-description">
			ダナンオフィスにはネオラボで最も多くの開発チームメンバーが在籍しています。
			若く優秀な才能の集まるダナンにおいて、常にエンジニアが最高のパフォーマンスを発揮できるオフィスファシリティが用意されています。成長著しいダナンの街は、エネルギッシュな雰囲気に満ちていて、周辺の自然環境も魅力の一つです。美しいクイニョンの海岸はリフレッシュに最適です。
		</p>
		<div class="company__contents-inner -gallery">
			<h3 class="company__contents-title">
				<span class="-en gf1">CORPORATE GALLERY</span>
				<span class="-jp">会社写真</span>
			</h3>
			<section class="company__contents-gallery">
				<ul class="js-image-gallery">
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-danang-1">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/01.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-danang-2">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/02.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-danang-3">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/03.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-danang-4">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/04.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-danang-5">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/05.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-danang-6">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/06.jpg" alt="" />
						</a>
					</li>
					<li>
						<a class="modal-trigger" href="" data-izimodal-open="#modal-danang-7">
							<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/07.jpg" alt="" />
						</a>
					</li>
				</ul>
			</section>
		</div>
	</div>
</main>


<div id="modal-danang-1" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/01.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-danang-2" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/02.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-danang-3" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/03.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-danang-4" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/04.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-danang-5" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/05.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-danang-6" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/06.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>
<div id="modal-danang-7" class="js-modais" data-izimodal-group="group-shop-images" data-izimodal-loop=true>
	<div class="modal-images">
		<img src="<?php echo esc_url ( get_stylesheet_directory_uri() ); ?>/assets/img/company/vietnam-danang/07.jpg" alt="" />
		<a href="javascript:void(0)" class="iziModal-button iziModal-button-close" data-izimodal-close=""></a>
	</div>
</div>

<?php get_template_part('_include/footer'); ?>
