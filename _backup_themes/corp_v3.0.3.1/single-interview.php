<?php get_template_part('_include/header'); ?>
<?php get_template_part('_include/inc-recruit'); ?>

<main class="g_main" role="main">
  <?php if(have_posts()):while(have_posts()):the_post(); ?>
  <?php RecruitHeader(); ?>
  <?php wpBreadcrumbs(); ?>
  <div class="single_area -interview">
    <div class="single_inner">
      <div class="single_visual__info">
        <p class="single_visual__joining-year">
          <?php echo get_field('interviewJoiningYear'); ?>
        </p>
        <h1 class="single_visual__name f_m">
          <?php the_title(''); ?>
        </h1>
        <div class="single_visual__position">
          <?php echo get_field('interviewPost'); ?>
        </div>
      </div>
      <div class="single_visual__image">
        <?php if (has_post_thumbnail()) : ?>
          <?php the_post_thumbnail(); ?>
        <?php endif ; ?>
        <?php
          if ($terms = get_the_terms($post->ID, 'interview_category')) {
            foreach ( $terms as $term ) {
              echo "<div class='interview_cat__label'>" . esc_html($term->name) . "</div>";
            }
          }
        ?>
      </div>
      <div class="the_content__area -interview">
        <div class="the_content cf">
          <?php the_content(); ?>
        </div>
      </div>
      <section class="single_recommend__area">
        <div class="single_recommend__area-inner">
          <h2 class="single_recommend__area-title">
            その他の「
            <?php
              if ($terms = get_the_terms($post->ID, 'interview_category')) {
                foreach ( $terms as $term ) {
                  echo "<span class=''>" . esc_html($term->name) . "</span>";
                }
              }
            ?>」のインタビュー記事
          </h2>
          <div class="single_recommend__area-list">
            <?php
              global $post;
              $term = array_shift(get_the_terms($post->ID, 'interview_category'));
              $args = array(
                'numberposts' => 3,
                'post_type' => 'interview',
                'taxonomy' => 'interview_category',
                'term' => $term->slug,
                // 'orderby' => 'rand',
                'order'         => 'DESC', // 新着順
                'post__not_in' => array($post->ID) //表示中の記事を除外
              );
            ?>
            <?php $myPosts = get_posts($args); if($myPosts) : ?>
            <?php foreach($myPosts as $post) : setup_postdata($post); ?>
            <article class="single_recommend__area-items">
              <a href="<?php the_permalink(); ?>" class="cf">
                <figure class="thumbs">
                  <?php the_post_thumbnail('interview-thumbs_square_md'); ?>
                </figure>
                <div class="description">
                  <div class="joining"><?php echo get_field('interviewJoiningYear', $post_id); ?></div>
                  <h1 class="title f_m"><?php the_title(); ?></h1>
                  <p class="position"><?php echo get_field('interviewPost', $post_id); ?></p>
                </div>
              </a>
            </article>
            <?php endforeach; ?>
            <?php else : ?>
            <?php // 関連アイテムはまだありません ?>
            <?php endif; wp_reset_postdata(); ?>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>
<?php endwhile;endif; ?>

<?php RecruitFooterBnner(); ?>
<?php get_template_part('_include/footer'); ?>
